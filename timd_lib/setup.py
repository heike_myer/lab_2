import os.path as path
import setuptools


setuptools.setup(
    name='timd_lib',
    version='1.0',

    package_dir={'': 'timd_lib'},
    packages=setuptools.find_packages(where='timd_lib'),
    long_description=open(path.join(path.dirname(__file__), 'README.md')).read()
)







