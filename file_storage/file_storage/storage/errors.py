import enum

import dateutil.rrule as rrule


class LowLevelAPIError(KeyError):
    def __init__(self, obj_id, obj_type, filename):
        self.obj_id = obj_id
        self.obj_type = obj_type
        self.filename = filename


class ObjectNotFoundError(LowLevelAPIError):
    def __init__(self, obj_id=None, obj_type=None, filename=None):
        super().__init__(obj_id, obj_type, filename)

    def __str__(self):
        return ("Object not found error:\nfilename: {}\nobject type: {}\n object_id: {}"
                .format(self.filename), self.obj_type, self.obj_id)


class ObjectRemovedError(LowLevelAPIError):
    def __init__(self, obj_id=None, obj_type=None, filename=None):
        super().__init__(obj_id, obj_type, filename)

    def __str__(self):
        return ("Object removed error:\nfilename: {}\nobject type: {}\n object_id: {}"
                .format(self.filename), self.obj_type, self.obj_id)


class StorageAPIErrorType(enum.Enum):
    OTHER = -1
    ADD_SUBTASK_TO_PERIODICAL_TASK = 0
    SUBTASK_DEADLINE_AFTER_PARENT = 1
    RESET_PERIODICAL_DEADLINE = 2
    NO_DEADLINE_REMINDER = 3
    RESET_EXISTING_DEADLINE = 4
    BOX_ALREADY_EXISTS = 5
    SET_PERIODICITY_TO_SUBTASK = 6
    SET_PERIODICITY_NO_DEADLINE = 7
    ALREADY_PERIODICAL = 8
    NEGATIVE_RELATIVE_DELTA = 9
    ZERO_RELATIVE_DELTA =10
    MAX_SUB_LEVEL = 11
    ALREADY_PERIODICAL_SUBTASK = 12
    RELATION_ALREADY_EXISTS = 13
    TASK_TAG_RELATION_EXISTS = 14
    UNSET_PERIODICAL_SUBTASK = 15
    NONE_DEADLINE = 16
    PERIODICAL_SUBTASK_FOR_A_DEADLINED_PARENT = 17
    UNCOMPLETED_SUBTASK = 18
    BLOCKED_TASK = 19
    REMINDER_EXISTS = 20
    PERMISSION_EXISTS = 21
    CANNOT_WITHDRAW_CREATOR_ACCESS = 22
    COMPLETED_PARENT = 23
    NEED_RESUME_TASK_FIRST = 24
    IN_BOX = 25
    BOX_EXISTS = 26


class StorageAPIError(Exception):
    def __init__(self, message=None, *args, **kwargs):
        self.message = message
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.message


class ObjectExistsError(StorageAPIError):
    pass
    #def __init__(self, message=None, *args, **kwargs):
        #super().__init__(message=message, *args, **kwargs)
        #self.error_type = error_type

    #def __str__(self):
       # return self.message


class PeriodicityError(StorageAPIError):
    def __init__(self, message=None, *args, **kwargs):
        super().__init__(message=message, *args, **kwargs)


class DeadlineUnsetError(StorageAPIError):
    def __init__(self, message=None, *args, **kwargs):
        super().__init__(message=message, *args, **kwargs)


class SubtaskError(StorageAPIError):
    def __init__(self, message=None, *args, **kwargs):
        super().__init__(message=message, *args, **kwargs)


class ArchivedError(StorageAPIError):
    def __init__(self, message=None, *args, **kwargs):
        super().__init__(message=message, *args, **kwargs)
