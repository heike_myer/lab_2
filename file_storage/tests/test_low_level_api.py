import os
import os.path
import shutil

import pytest

import file_storage.storage.errors as errors
import file_storage.storage.low_level_api as low_level_api
import file_storage.tasktracker_logging as tasktracker_logging
from tests.helpers import contains_corresponding_dict_obj, contains_corresponding_object, load
from file_storage.models import TestObject

tasktracker_logging.configure_default_loggers()


filename = 'data_for_tests/current.json'


def setup_function():
    if os.path.exists(os.path.join(os.path.dirname(__file__), filename)):
        os.remove(filename)

    shutil.copyfile('data_for_tests/data_low_level_api_test.json', filename)


def test_default_key():
    obj = TestObject(1, 'my')
    assert low_level_api.default_key(obj) == 1


def test_create_object_default_key():
    low_level_api.create_object(filename, TestObject, name='Name')
    data = load()

    assert contains_corresponding_dict_obj(data, name='Name')


def test_create_object_custom_key():
    low_level_api.create_object(filename, TestObject, name='Name', keyfunc=lambda obj: obj.name)
    data = load()

    assert 'Name' in data


def test_add_new_objects():
    obj_list = [TestObject('1', 'unique1'), TestObject('2', 'unique2'), TestObject('3', 'unique3')]
    low_level_api.add_new_objects(filename, TestObject, obj_list)
    data = load()

    assert contains_corresponding_dict_obj(data, name='unique1')
    assert contains_corresponding_dict_obj(data, name='unique2')
    assert contains_corresponding_dict_obj(data, name='unique3')


def test_get_object_by_id():
    first = low_level_api.get_object_by_id(filename, TestObject, '1')
    second = low_level_api.get_object_by_id(filename, TestObject, '2')

    assert first.instance_id == '1' and first.name == 'first'
    assert second.instance_id == '2' and second.name == 'second'


def test_get_object_by_id_raises_objectnotfound_exception():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.get_object_by_id(filename, TestObject, 'blablabla')


def test_get_objects_by_id_list():
    dict_with_obj = low_level_api.get_objects_by_id_list(filename, TestObject, ['1', '2'])

    assert len(dict_with_obj) == 2
    assert contains_corresponding_object(dict_with_obj, instance_id='1')
    assert contains_corresponding_object(dict_with_obj, instance_id='2')


def test_get_objects_by_id_list_raises_objectnotfound_exception():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.get_objects_by_id_list(filename, TestObject, ['1', '2', 'blablabla'])


def test_find_objects_by_key_field():
    dict_with_obj = low_level_api.find_objects_by_key_field(filename, TestObject, 'name', 'dracula')
    assert len(dict_with_obj) == 2
    assert contains_corresponding_object(dict_with_obj, instance_id='3')
    assert contains_corresponding_object(dict_with_obj, instance_id='4')


def test_find_corresponding_objects_fields_match():
    dict_with_obj = low_level_api.find_corresponding_objects(filename, TestObject, name='dracula')
    assert len(dict_with_obj) == 2
    assert contains_corresponding_object(dict_with_obj, instance_id='3')
    assert contains_corresponding_object(dict_with_obj, instance_id='4')


def test_find_corresponding_objects_custom_predicate():
    name = 'dracula'
    dict_with_obj = low_level_api.find_corresponding_objects(filename, TestObject, name,
                                                             predicate=lambda obj, name: obj.name == name)
    assert len(dict_with_obj) == 2
    assert contains_corresponding_object(dict_with_obj, instance_id='3')
    assert contains_corresponding_object(dict_with_obj, instance_id='4')


def test_find_first_corresponding_object_raises_objectnotfound_exception():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.find_first_corresponding_object(filename, TestObject, name='blablabla')


def test_delete_object_by_id():
    low_level_api.delete_object_by_id(filename, TestObject, '1')
    data = load()

    assert not contains_corresponding_dict_obj(data, name='first', instance_id='1')


def test_delete_object_by_id_raises_objectnotfound_exception():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.delete_object_by_id(filename, TestObject, 'blablabla')


def test_delete_objects_by_id_list():
    low_level_api.delete_objects_by_id_list(filename, TestObject, ['1', '2'])
    data = load()

    assert not contains_corresponding_dict_obj(data, name='first', instance_id='1')
    assert not contains_corresponding_dict_obj(data, name='second', instance_id='2')


def test_delete_objects_by_id_list_raises_objectnotfound_exception():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.delete_objects_by_id_list(filename, TestObject, ['1', '2', 'blablabla'])


def test_delete_objects_by_key_field():
    low_level_api.delete_objects_by_key_field(filename, TestObject, 'name', 'dracula')
    data = load()

    assert not contains_corresponding_dict_obj(data, name='dracula')


def test_delete_corresponding_objects():
    low_level_api.delete_corresponding_objects(filename, TestObject, predicate=lambda obj: obj.name == 'dracula')
    data = load()

    assert not contains_corresponding_dict_obj(data, instance_id='3')
    assert not contains_corresponding_dict_obj(data, instance_id='4')
    assert not contains_corresponding_dict_obj(data, name='dracula')


def test_update_object_by_id():
    low_level_api.update_object_by_id(filename, TestObject, '1', name='SUPER FIRST')
    data = load()

    assert not contains_corresponding_dict_obj(data, instance_id='1', name='first')
    assert contains_corresponding_dict_obj(data, instance_id='1', name='SUPER FIRST')


def test_update_raises_objectnotfound_exception():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.update_object_by_id(filename, TestObject, 'blabla', name='BLA BLA')


def test_update_objects():
    update_list = [('1', {'name': 'SUPER FIRST'}), ('2', {'name': 'GOD SAVE ME'})]
    low_level_api.update_objects(filename, TestObject, update_list)
    data = load()

    assert not contains_corresponding_dict_obj(data, instance_id='1', name='first')
    assert contains_corresponding_dict_obj(data, instance_id='1', name='SUPER FIRST')

    assert not contains_corresponding_dict_obj(data, instance_id='2', name='second')
    assert contains_corresponding_dict_obj(data, instance_id='2', name='GOD SAVE ME')


def test_update_objects_raises_objectnotfound_exception():
    update_list = [('1', {'name': 'SUPER FIRST'}), ('2', {'name': 'GOD SAVE ME'}), ('bla', {})]
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.update_objects(filename, TestObject, update_list)


def test_update_objects_with_same_value():
    low_level_api.update_objects_with_same_value(filename, TestObject, ['1', '2', '3', '4'], 'name', 'Sweety')
    data = load()

    assert not contains_corresponding_dict_obj(data, instance_id='1', name='first')
    assert not contains_corresponding_dict_obj(data, instance_id='2', name='second')
    assert not contains_corresponding_dict_obj(data, instance_id='3', name='dracula')
    assert not contains_corresponding_dict_obj(data, instance_id='4', name='dracula')

    for d_id in data:
        assert data[d_id]['name'] == 'Sweety'


def test_update_objects_with_same_value_raises_objectnotfound_error():
    with pytest.raises(errors.ObjectNotFoundError):
        low_level_api.update_objects_with_same_value(filename, TestObject, ['1', '33', '2', '3', '4'], 'name', 'AAA')
