import pytest

import console.console.parser as console
import file_storage.file_storage.models as models
import file_storage.file_storage.models.str_constants as const
import file_storage.file_storage.storage.errors as errors
import file_storage.file_storage.storage.low_level_api as low_level_api
from file_storage.file_storage.storage.storage_api import StorageAPI
from file_storage.tests.helpers import create_data_files, load_from, contains_dict, remove_data_folder
from lib_api import LibraryAPI


class TestParser:
    def setup_method(self, test_method):
        remove_data_folder()
        self.paths = create_data_files()
        self.storage = LibraryAPI(StorageAPI(self.paths))
        self.parser = console.create_main_parser()

        self.user_1 = self.storage.storage_api.create_object(models.User, 'user_1', None, None)
        self.creator_box_1 = self.storage.storage_api.create_object(models.Box, self.user_1, 'My Box')
        self.shared_box_1 = self.storage.storage_api.create_object(models.Box, self.user_1, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user_1,
                                          creator_box=self.creator_box_1, shared_box=self.shared_box_1)

        self.user_2 = self.storage.storage_api.create_object(models.User, 'user_2', None, None)
        self.creator_box_2 = self.storage.storage_api.create_object(models.Box, self.user_2, 'My Box')
        self.shared_box_2 = self.storage.storage_api.create_object(models.Box, self.user_2, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user_2,
                                               creator_box=self.creator_box_2, shared_box=self.shared_box_2)

        self.custom_box_1 = self.storage.create_box(self.user_1, 'Custom Box')

        self.task = self.storage.create_task(self.user_1, 'task')

    def test_create_box_handler(self):
        namespace = self.parser.parse_args(['create', 'box', 'title_1'])
        namespace.handler(self.storage, self.user_1, namespace)

        data = load_from(self.paths, models.Box)

        assert contains_dict(data, {const.CLASS: const.BOX_CLASS, const.USER_ID: self.user_1, const.TITLE: 'title_1'})

    def test_create_existing_box(self):
        namespace = self.parser.parse_args(['create', 'box', 'My Box'])
        with pytest.raises(errors.ObjectExistsError):
            namespace.handler(self.storage, self.user_1, namespace)

    def test_create_task_default_box(self):
        namespace = self.parser.parse_args(['create', 'task', 'title_1'])
        result = namespace.handler(self.storage, self.user_1, namespace)

        data = load_from(self.paths, models.Task)
        assert contains_dict(data, {const.CLASS: const.TASK_CLASS, const.USER_ID: self.user_1, const.TITLE: 'title_1'})

        data = load_from(self.paths, models.TaskBoxRelation)
        assert contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.USER_ID: self.user_1,
                                    const.TASK_ID: result, const.BOX_ID: self.creator_box_1})

    def test_create_task_custom_box(self):
        namespace = self.parser.parse_args(['create', 'task', 'title_1', '--box', 'Custom Box'])
        result = namespace.handler(self.storage, self.user_1, namespace)

        data = load_from(self.paths, models.Task)
        assert contains_dict(data, {const.CLASS: const.TASK_CLASS, const.USER_ID: self.user_1, const.TITLE: 'title_1'})

        data = load_from(self.paths, models.TaskBoxRelation)
        assert contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.USER_ID: self.user_1,
                                    const.TASK_ID: result, const.BOX_ID: self.custom_box_1})

    def test_set_pasrser(self):
        namespace = self.parser.parse_args(['set', 'deadline', self.task, '10-10-2016'])

        namespace.handler(self.storage, self.user_1, namespace)

        data = load_from(self.paths, models.Task)
        assert contains_dict(data, {const.CLASS: const.TASK_CLASS, const.USER_ID: self.user_1,
                                    const.INSTANCE_ID: self.task, const.DEADLINE: "2016-10-10T00:00:00"})

        data = load_from(self.paths, models.Term)
        assert contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TERM: "2016-10-10T00:00:00",
                                    const.TERM_TYPE: const.DEADLINE_TERM_TYPE})
