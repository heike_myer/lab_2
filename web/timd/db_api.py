from django.contrib.auth.models import User
import timd.models as models

import timd.errors as errors
import django.core.exceptions as exceptions
from datetime import datetime

from django.utils import timezone

import dateutil.rrule as rrule

from django.db.models import Q, F

from django.utils.text import slugify

import django.http as h


def write(message):
    with open('info.txt', mode='a') as file:
        file.write(message + '\n')


def custom_user(func):
    def wrapper(self, username, *args, **kwargs):
        custom_user = models.CustomUser.objects.get(user__username=username)
        return func(self, custom_user, *args, **kwargs)

    return wrapper


def model_object_by_slug(model=models.Task, slug_pos=0):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            slug = args[slug_pos]
            model_obj = model.objects.get(id=slug)
            args_list = list(args)
            args_list[slug_pos] = model_obj
            return func(self, *args_list, **kwargs)

        return wrapper

    return decorator


class DatabaseAPI:
    def __init__(self):
        pass

    def get_user_by_username(self, username):
        return models.CustomUser.objects.get(user__username=username)

    def get_box_by_id(self, box_id):
        return models.Box.objects.get(id=box_id)

    def get_task_by_id(self, task_id):
        return models.Task.objects.get(id=task_id)



    @custom_user
    def get_user_tasks(self, user):
        return user.tasks.all()

    @custom_user
    def create_box(self, user, title):
        box = user.boxes.create(title=title)
        return box

    #@model_object_by_slug(model=models.Box)
    def rename_box(self, box, title):
        box.title = title
        box.save()
        return box

    @model_object_by_slug(model=models.Box)
    def delete_box(self, box):
        models.Task.objects.filter(Q(box=box) | Q(parent__box=box)).delete()
        box.delete()
        #models.Box.objects.filter(slug=box_slug).delete()

    @custom_user
    def get_user_boxes(self, user):
        shared_box = user.shared_box
        created_box = user.created_box
        others = models.Box.objects.filter(user=user).exclude(Q(id=shared_box.id) |
                                                              Q(id=created_box.id)).order_by('title')

        return shared_box, created_box, others

    def get_tasks_from_box(self, box):
        return box.tasks.order_by('last_update_time')

    #@custom_user
    def get_all_subtasks(self, task):
        return models.Task.objects.filter(parent=task).order_by('last_update_time')

    @custom_user
    def create_task(self, user, box_id, title, priority):
        box = models.Box.objects.get(id=box_id)
        task = models.Task(creator=user, title=title, priority=priority)
        task.save()

        box.tasks.add(task)

        #self.slugify(task)

        models.Permission.objects.create(user=user, task=task, access_level=models.AccessLevel.CREATOR.value)

        return task

    @custom_user
    def create_subtask(self, user, parent_slug, title, priority):
        parent = self.get_task_by_id(parent_slug)
        if parent.has_parent:
            raise errors.SubtaskError()

        subtask = models.Task(creator=user, parent=parent, title=title, priority=priority,
                              deadline=parent.deadline, periodicity=parent.periodicity)
        subtask.save()

        #self.slugify(subtask)

        perms = parent.permissions.all()
        for p in perms:
            p.id = None
            p.task = subtask
            p.save()

        return subtask

    def get_task_permissions(self, task):
        return task.permissions.all()

    def get_reminders(self, task):
        return task.reminders.all()

    def delete_task(self, task_id):
        models.Task.objects.filter(Q(id=task_id) | Q(parent__id=task_id)).delete()

    def set_deadline(self, task, deadline):
        self.update_deadline(task, deadline)
        task.subtasks.filter(deadline__isnull=True).update(deadline=deadline, last_update_time=timezone.now())

    def update_deadline(self, task, new_deadline):
        tasks = models.Task.objects.filter(Q(id=task.id) | Q(parent=task) & Q(deadline__gt=new_deadline))

        tasks_ids = tasks.values_list('id')
        reminders = models.Reminder.objects.filter(task__in=tasks_ids)
        now = timezone.now()
        reminders_to_activate = reminders.filter(active=False, timedelta__lte=new_deadline - now)

        reminders_to_activate.update(active=True)
        reminders.update(term=new_deadline - F('timedelta'))
        tasks.update(deadline=new_deadline, last_update_time=now)

    def unset_deadline(self, task):
        task.deadline = None
        task.periodical = False
        task.rule_str = None
        task.save()

    def set_reminder(self, task, timedelta):
        deadline = task.deadline
        reminder, created = task.reminders.get_or_create(timedelta=timedelta,
                                                         defaults={'term': deadline - timedelta})
        if not created:
            raise errors.ObjectExistsError

        return reminder

    def unset_reminder(self, reminder_id):
        models.Reminder.objects.filter(id=reminder_id).delete()

    def unset_all_reminders(self, task):
        task.reminders.all().delete()

    def set_periodicity(self, task, freq, interval=1, count=None, until=None, byweekday=None):
        periodicity = models.Periodicity(frequency=freq, interval=interval, count=count, until=until)
        periodicity.weekday_list = byweekday

        #return periodicity
        periodicity.save()

        #rule_str = make_rule_str(freq, interval=interval, count=count, until=until, byweekday=byweekday)
        models.Task.objects.filter(Q(id=task.id) | Q(parent=task)).update(periodicity=periodicity)

    def unset_periodicity(self, task):
        #tasks = models.Task.objects.filter(Q(id=task.id) | Q(parent=task))#.update(periodical=False)
        #for t in tasks:
            #if t.periodicity is not None:
        task.periodicity.delete()

    @model_object_by_slug()
    def complete_task(self, task):
        if not task.periodicity is not None or (task.parent is not None and task.periodicity is not None):
            task.reminders.all().update(active=False)
            task.status = models.Status.COMPLETED.value
            task.save()

        else:
            next_deadline_term = task.periodicity.next_date(task.deadline)#get_next_date(task.rule_str, task.deadline.term)

            if next_deadline_term is None:
                task.reminders.all().update(active=False)
                task.status = models.Status.COMPLETED.value
                task.save()

            else:
                self._update_deadline_req(task)
                #rule_str = shift_rule(task.rule_str)
                models.Task.objects.filter(Q(id=task.id) | Q(parent=task)).update(status=models.Status.PENDING.value)
                task.periodicity.shift()
                 #                                                                 rule_str=rule_str)

    def _update_deadline_req(self, task):
        tasks = models.Task.objects.filter(Q(id=task.id) | Q(parent=task))
        for task in tasks:
            next_deadline = task.periodicity.next_date(task.deadline)#get_next_date(task.rule_str, task.deadline)
            task.deadline = next_deadline
            task.status = models.Status.PENDING.value
            #task.rule_str = rule_str
            task.save()
            for r in task.reminders.all():
                r.term = next_deadline - r.timedelta
                if r.term >= timezone.now():
                    r.active = True

                r.save()

    @model_object_by_slug()
    def resume_task(self, task):
        if task.status != models.Status.COMPLETED.value:
            raise errors.ObjectExistsError

        task.status = models.Status.PENDING.value
        task.save()

        now = timezone.now()

        if task.deadline is not None:
            task.reminders.filter(task=task, timedelta__lte=task.deadline - now).update(active=True)

        parent = task.parent
        if parent is not None and parent.status == models.Status.COMPLETED.value:
            parent.status = models.Status.PENDING.value
            parent.save()
            if parent.deadline is not None:
                parent.reminders.filter(task=parent, timedelta__lte=parent.deadline - now).update(active=True)

    @custom_user
    def attach_tag(self, user, task, tag_text):
        tag, created = models.Tag.objects.get_or_create(user=user, task=task, tag_text=tag_text)

        if not created:
            raise errors.ObjectExistsError

    @custom_user
    def detach_tag(self, user, task, tag_id):
        task.tags.filter(id=tag_id).delete()

    @custom_user
    def attach_relation(self, user, from_task, to_task, relation_type):
        try:
            models.Relation.objects.get(user=user, from_task=from_task, to_task=to_task)
            models.Relation.objects.get(user=user, from_task=to_task, to_task=from_task)
            raise errors.ObjectExistsError

        except exceptions.ObjectDoesNotExist:
            models.Relation.objects.create(user=user, from_task=to_task, to_task=from_task, relation_type=relation_type)


    def detach_relation(self, relation_id):
        models.Relation.objects.filter(id=relation_id).delete()

    @custom_user
    def move_task_to_box(self, user, task, box):
        if task in box.tasks:
            raise errors.ObjectExistsError()

        task.box_set.remove(user__user=user)
        box.tasks.add(task)

        if task.parent is not None:
            task.parent = None

    @model_object_by_slug()
    def archive_task(self, task):
        models.Task.objects.filter(Q(id=task.id) | Q(parent=task)).update(archived=True, last_update_time=timezone.now())

    @model_object_by_slug()
    def restore_task(self, task):
        models.Task.objects.filter(Q(id=task.id) | Q(parent=task)).update(archived=False, last_update_time=timezone.now())


    def update_fields(self, object, **kwargs):
        object.__dict__.update(kwargs)
        object.save()

    def create_notifications(self, task, date):
        date = date.date()
        write('\nDate:' + str(date))
        deadline_type = models.NotificationType.DEADLINE.value
        for user in task.users.all():
            notifications = user.notifications.filter(task=task, notification_type=deadline_type)
            write('Deadlines '+str(notifications))

            if notifications.filter(viewed=False).exists():
                write('only viewed')
                return
                             #.exclude(viewed=True, viewed_time__date=date))

            #if notifications.filter(viewed=False).exists():

            if notifications.exists():
                copy = notifications.latest('viewed_time')
                if copy.viewed_time.date() == date:
                    return
                #notifications = notifications.exclude(viewed=True, viewed_time__date=date)
                write('Viewed earlier than today: ' + str(notifications))
                #copy = notifications.first()
                write('Copy '+str(copy))

                if copy is not None:
                    self.update_fields(copy, id=None, creation_time=date, viewed=False, viewed_time=None)
                    write('Updated: ' + str(copy))

            else:
                write('else')
                user.notifications.create(user=user, task=task, notification_type=deadline_type, creation_time=date)


                #if notifications.filter


    def create_deadline_notifications(self, task, date):
        write('\nDate:' + str(date))
        deadline_type = models.NotificationType.DEADLINE.value
        #now = timezone.now()
        notifications = task.notification_set.all()#filter(notification_type=deadline_type,
                                                  #   viewed=True).exclude(viewed_time__date=date)
        write('Viewed earlier than today: ' + str(notifications))

        if notifications:
            notifications = notifications.filter(notification_type=deadline_type,
                                                 viewed=True).exclude(viewed_time__date=date)
            by_user = notifications.order_by('user').distinct('user')
            write('After distinct: ' + str(by_user))
            #for user in task.users.all():
               # pass
                #user_notifica
            for n in by_user:
                self.update_fields(n, id=None, creation_time=date, viewed=False, viewed_time=None)
                write('Updated: '+str(n))
                #write(str(n))

        else:
            write('else')
            for user in task.users.all():
                models.Notification.objects.create(user=user, task=task, notification_type=deadline_type, creation_time=date)

            #models.Notififcation.create



        #notifications = task.notification_set.filter(notification_type=notification_type).exclude(Q(viewed=False) |
            #                                                                                      Q(viewed_time__date=date))

        #for


        #now = timezone.now()
        #for user in task.users.all():
            #models.Notification.objects.get_or_create(user=user, task=task,
                                                      #notification_type=notification_type, viewed=False,
                                                      #defaults={'creation_time': now})

   # def create_notification(self, user, task, notification_type):


    def handle_overdue_tasks(self, date):
        overdue_tasks = models.Task.objects.filter(archived=False, deadline__gte=date)
        for t in overdue_tasks:
            t.status = models.Status.OVERDUE.value
            self.create_notifications(t, date)

    def handle_overdue_reminders(self, date):
        reminders = models.Reminder.objects.filter(active=True, term__gte=date)
        reminders.update(active=False)
        for r in reminders:
            task = r.task
            #self.create_notifications(task, models.NotificationType.REMINDER.value)

    def handle_overdue_terms(self, date=None):
        #if date is None:
          #  date = timezone.now()

        self.handle_overdue_tasks(date)
        self.handle_overdue_reminders(date)



    @custom_user
    def get_blocking_tasks(self, user, task):
        return task.relating_tasks.all()
        #filter(relation_type=models.RelationType.BLOCKING.value)
        #return models.Task.objects.filter(id__in=task)

    @custom_user
    def get_blocked(self, user, task):
        return task.related_tasks.all()
        #.filter(relation_type=models.RelationType.BLOCKING.value)

    @custom_user
    def get_simultaneous_tasks(self, user, task):
        return models.Relation.objects.filter((Q(from_task=task) | Q(to_task=task)) &
                                               Q(relation_type=models.RelationType.SIMULTANEOUS.value))

    @custom_user
    def get_user_tags(self, user, task):
        return task.tags.filter(user=user)

    def get_permissions(self, task):
        return models.Permission.objects.filter(task=task)

    def give_permission(self, user, task, access_level):
        perm = models.Permission.objects.create(user=user, task=task, access_level=access_level)
        user.shared_box.tasks.add(task)

    @custom_user
    def withdraw_permission(self, user, task):
        for t in models.Task.objects.filter(Q(id=task.id) | Q(parent=task)):
            user.shared_box.tasks.remove(t)
        models.Permission.objects.filter(Q(task=task) | Q(task__parent=task), user=user).delete()

    @custom_user
    def get_user_completed_tasks(self, user):
        return user.tasks.filter(status=models.Status.COMPLETED.value)

    @custom_user
    def get_shared_tasks(self, user):
        return user.shared_box.tasks.all()

    @custom_user
    def get_permission(self, user, task):
        return models.Permission.objects.get(user=user, task=task)
        #return permission.access_level

    @custom_user
    def get_all_user_notifications(self, user):
        return user.notifications.order_by('viewed', 'creation_time')

    @custom_user
    def view_notifications(self, user, date):
        now = timezone.now()
        user.notifications.filter(viewed=False).update(viewed=True, viewed_time=date)

        #notifications.filter(viewed=False).update(viewed=True)


    @custom_user
    def get_user_box(self, user, task):
        return user.boxes.get(id__in=task.box_set.values_list('id'))

    @custom_user
    def get_all_user_relations(self, user, task):
        #blocked = models.Relation.objects.filter(user=user, from_task=task,
                                            #     relation_type=models.RelationType.BLOCKING.value)
        #blocking = models.Relation.objects.filter(user=user, to_task=task,
                                            #      relation_type=models.RelationType.BLOCKING.value)
        #simultaneous = models.Relation.objects.filter(Q(user=user) & (Q(to_task=task) | Q(from_task=task)) &
                        #                              Q(relation_type=models.RelationType.SIMULTANEOUS.value))

        #return blocking, simultaneous, blocked
        return models.Relation.objects.filter(Q(user=user) & (Q(to_task=task) | Q(from_task=task)))

    @custom_user
    def get_tasks_for_date(self, user, date):
        return user.tasks.filter(deadline__date=date)

    @custom_user
    def get_tasks_in_date_range(self, user, from_date, until_date):
        return user.tasks.filter(deadline__date__gte=from_date, deadline__date__lte=until_date)