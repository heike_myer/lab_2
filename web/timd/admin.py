from django.contrib import admin

import timd.models as models

admin.site.register(models.CustomUser)
admin.site.register(models.Box)
admin.site.register(models.Task)
admin.site.register(models.Permission)
admin.site.register(models.Reminder)
admin.site.register(models.Tag)
admin.site.register(models.Relation)
admin.site.register(models.Notification)
