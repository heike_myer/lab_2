import unittest

import sys
sys.path.append('..')

#import file_storage.tests as tests
import tests.test_storage_api as test_add_create_attach_api
#from . import test_storage_api


import file_storage.storage_logging as storage_logging
storage_logging.configure_default_logger()


storageAPITestSuit = unittest.TestSuite()
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestCreateTaskBox))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestSetDeadlineReminderPeriodictyRelation))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestCreateSubtask))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestTag))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestCopyTask))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestCompletePeriodicalTask))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestCompleteTask))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestUnsetPeriodicity))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestDeadlineUpdateUnset))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestDetachAttribute))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestArchiveRestoreDeleteTask))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestHandleOverdueNonperiodicalTerms))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestHandleOverduePeriodicalTerms))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestBoxes))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestResumeTask))
storageAPITestSuit.addTest(unittest.makeSuite(test_add_create_attach_api.TestGetOverdueTasks))

current = unittest.TestSuite()
current.addTest(unittest.makeSuite(test_add_create_attach_api.TestGetOverdueTasks))
#current.addTest(unittest.makeSuite(test_add_create_attach_api.TestArchiveRestoreDeleteTask))


runner = unittest.TextTestRunner(verbosity=2)

#runner.run(storageAPITestSuit)
#runner.run(current)

