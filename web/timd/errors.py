import enum


class StorageAPIErrorType(enum.Enum):
    OTHER = -1
    ADD_SUBTASK_TO_PERIODICAL_TASK = 0
    SUBTASK_DEADLINE_AFTER_PARENT = 1
    RESET_PERIODICAL_DEADLINE = 2
    NO_DEADLINE_REMINDER = 3
    RESET_EXISTING_DEADLINE = 4
    BOX_ALREADY_EXISTS = 5
    SET_PERIODICITY_TO_SUBTASK = 6
    SET_PERIODICITY_NO_DEADLINE = 7
    ALREADY_PERIODICAL = 8
    NEGATIVE_RELATIVE_DELTA = 9
    ZERO_RELATIVE_DELTA =10
    MAX_SUB_LEVEL = 11
    ALREADY_PERIODICAL_SUBTASK = 12
    RELATION_ALREADY_EXISTS = 13
    TASK_TAG_RELATION_EXISTS = 14
    UNSET_PERIODICAL_SUBTASK = 15
    NONE_DEADLINE = 16
    PERIODICAL_SUBTASK_FOR_A_DEADLINED_PARENT = 17
    UNCOMPLETED_SUBTASK = 18
    BLOCKED_TASK = 19
    REMINDER_EXISTS = 20
    PERMISSION_EXISTS = 21
    CANNOT_WITHDRAW_CREATOR_ACCESS = 22
    COMPLETED_PARENT = 23
    NEED_RESUME_TASK_FIRST = 24
    IN_BOX = 25
    BOX_EXISTS = 26


class StorageAPIError(Exception):
    def __init__(self, error_type=StorageAPIErrorType.OTHER):
        self.error_type = error_type

    def __str__(self):
        return self.error_type.name


class ObjectExistsError(StorageAPIError):
    def __init__(self, error_type=StorageAPIErrorType.OTHER):
        self.error_type = error_type

    def __str__(self):
        return self.error_type.name
