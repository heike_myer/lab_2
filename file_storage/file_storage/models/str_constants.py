"""This module contains string constants with the names of stored models' classes and all their attributes' names."""

CLASS = 'class'

TASK_CLASS = 'Task'
USER_CLASS = 'User'
PERMISSION_CLASS = 'Permission'
TERM_CLASS = 'Term'
BOX_CLASS = 'Box'
RELATION_CLASS = 'Relation'
TAG_CLASS = 'Tag'
TASK_TAG_RELATION_CLASS = 'TaskTagRelation'
TASK_BOX_RELATION_CLASS = 'TaskBoxRelation'

INSTANCE_ID = 'instance_id'
TAG = 'tag'

USER_ID = 'user_id'
TASK_ID = 'task_id'
TASK_FROM = 'task_from'
TASK_TO = 'task_to'
PARENT_ID = 'parent_id'
BOX_ID = 'box_id'

CREATION_TIME = 'creation_time'
LAST_UPDATE_TIME = 'last_update_time'

TITLE = 'title'
USERNAME = 'username'

TERM = 'term'
DEADLINE = 'deadline'
RELATIVE_DELTA = 'relative_delta'
RULE_STR = 'rule_str'

PERIODICAL = 'periodical'
ACTIVE = 'active'
VIEWED = 'viewed'
ARCHIVED = 'archived'

STATUS = 'status'
PRIORITY = 'priority'
ACCESS_LEVEL = 'access_level'
TERM_TYPE = 'term_type'
RELATION_TYPE = 'relation_type'
NOTIFICATION_TYPE = 'notification_type'


YEARS = 'years'
MONTHS = 'months'
DAYS = 'days'
HOURS = 'hours'
MINUTES = 'minutes'

OBJECT_TYPE = 'object_type'

HIGH = 'HIGH'
MEDIUM = 'MEDIUM'
LOW = 'LOW'
NONE = 'NONE'

BLOCKING = 'BLOCKING'
SIMULTANEOUS = 'SIMULTANEOUS'

CREATOR = 'CREATOR'
ADMIN = 'ADMIN'
EXECUTOR = 'EXECUTOR'
WATCHER = 'WATCHER'

DEADLINE_TERM_TYPE = 'DEADLINE'
REMINDER_TERM_TYPE = 'REMINDER'

COMPLETED = 'COMPLETED'
PENDING = 'PENDING'
OVERDUE = 'OVERDUE'

CREATOR_BOX = 'creator_box'
SHARED_BOX = 'shared_box'
