"""This module contains functions implementing the most common operations on stored objects.

These are mostly creating new, searching for existing or removing them from storage files.
"""
import json
import logging
import uuid

import file_storage.storage.errors as errors
import file_storage.storage_logging as storage_logging
from file_storage.storage.json_extensions import CustomJSONEncoder, CustomJSONDecoder


def low_level_api_log(level=logging.DEBUG):
    return storage_logging.file_storage_log(level=level)


@low_level_api_log()
def generate_id():
    return str(uuid.uuid4())


@low_level_api_log()
def dump(filename, objects):
    """Encode `objects` using `CustomJsonEncoder` and dump them to specified file_storage."""
    with open(filename, mode='w') as file:
        json.dump(objects, file, cls=CustomJSONEncoder, indent=4)


@low_level_api_log()
def load(filename, obj_type):
    """Decode objects of type `obj_type` stored in specified file_storage using `CustomJsonDecoder`."""
    with open(filename, mode='r') as file:

        json_data = json.load(file, cls=CustomJSONDecoder, object_type=obj_type)
    return json_data


@low_level_api_log()
def default_key(obj):
    return obj.instance_id


@low_level_api_log()
def object_fields_match(obj, **kwargs):
    """Check that object's fields' values are equal to corresponding values from `**kwargs`."""
    match = True
    for attr in kwargs:

        if obj.__dict__[attr] != kwargs[attr]:
            match = False
            break

    return match


@low_level_api_log()
def create_object(filename, obj_type, *args, keyfunc=default_key, **kwargs):
    """Create new object and save it to specified file_storage.

    Args:
        filename (str): Path to a file_storage where the storage is.
        obj_type (type): Type to send to `load`.
        *args: All positional arguments except `instance_id` required by `obj_type` `__init__` method.
        keyfunc: Function selecting an object's attribute to be its storage key (default is `instance_id`).
        **kwargs: All optional arguments to send to `obj_type` `__init__` method.

    Returns:
        Id of a new object.

    Raises:
        ObjectExistsError: If there is already a key selected by `keyfunc` in a storage.

    Side effects:
        No way to influence the way `instance_id` is generated.
    """
    obj_dict = load(filename, obj_type)

    obj_id = generate_id()
    obj = obj_type(obj_id, *args, **kwargs)
    key = keyfunc(obj)

    if obj_dict.pop(key, None) is not None:
        raise errors.ObjectExistsError()

    obj_dict[key] = obj
    dump(filename, obj_dict)

    return obj_id


@low_level_api_log()
def add_new_objects(filename, obj_type, obj_list, keyfunc=default_key):
    """Save listed objects to a specified file_storage.

    Args:
        filename (str): Path to a file_storage where the storage is.
        obj_type (type): Type to send to `load`.
        obj_list (list of obj_type): Listed objects.
        keyfunc: Function selecting an object's attribute to be its storage key (default is `instance_id`).

    Raises:
       ObjectExistsError: If there is already a key selected by `keyfunc` in a storage.

    Side effects:
       Initial `instance_id` values are changed.
    """
    obj_dict = load(filename, obj_type)
    for obj in obj_list:
        obj.instance_id = generate_id()
        key = keyfunc(obj)

        if obj_dict.pop(key, None) is not None:
            raise errors.ObjectExistsError(key, obj_type, filename)

        obj_dict[key] = obj
    dump(filename, obj_dict)


@low_level_api_log()
def get_object_by_id(filename, obj_type, obj_id):
    """Find object in specified file_storage by its key.

    Raises:
        ObjectNotFoundError: If there is no object with such key.
    """
    obj_dict = load(filename, obj_type)
    obj = obj_dict.get(obj_id)

    if obj is None:
        raise errors.ObjectNotFoundError(obj_id=obj_id, obj_type=obj_type, filename=filename)

    return obj


@low_level_api_log()
def get_objects_by_id_list(filename, obj_type, id_list):
    """Find objects whose keys are listed in a specified file_storage.

    Raises:
        ObjectNotFoundError: If `id_list` contains a key which is not in a storage.
    """
    obj_dict = load(filename, obj_type)
    match = {}
    for obj_id in id_list:
        obj = obj_dict.get(obj_id)

        if obj is None:
            raise errors.ObjectNotFoundError(obj_id=obj_id, obj_type=obj_type, filename=filename)

        match[obj_id] = obj

    return match


@low_level_api_log()
def find_first_corresponding_object(filename, obj_type, *args, predicate=object_fields_match, **kwargs):
    """Find first object matching predicate in specified file_storage.

    Args:
        filename (str): Path to a file_storage where the storage is.
        obj_type (type): Type to send to `load`.
        *args: All positional arguments required by `predicate`.
        predicate: Function returning True when object matches and False otherwise (default is `object_fields_match').
        **kwargs: All keyword arguments required by `predicate`.

    Returns:
        Corresponding object.

    Raises:
        ObjectNotFoundError: If there is no such object in storage.

    Side effects:
        No way to define the check order, so the first one may be any actually.
    """
    obj_dict = load(filename, obj_type)
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]

        if isinstance(obj, dict):
            continue

        if predicate(obj, *args, **kwargs):
            return obj

    raise errors.ObjectNotFoundError(filename=filename, obj_type=obj_type)


@low_level_api_log()
def find_objects_by_key_field(filename, obj_type, field_name, field_value):
    """Find all objects whose `field_name`'s value equals to `field_value` and return them as a dict."""
    obj_dict = load(filename, obj_type)
    match = {}
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]

        if isinstance(obj, dict):
            continue

        if obj.__dict__[field_name] == field_value:
            match[obj_id] = obj

    return match


@low_level_api_log()
def find_corresponding_objects(filename, obj_type, *args, predicate=object_fields_match, **kwargs):
    """Find all objects matching predicate in specified file_storage.

    Args:
        filename (str): Path to a file_storage where the storage is.
        obj_type (type): Type to send to `load`.
        *args: All positional arguments required by `predicate`.
        predicate: Function returning True when object matches and False otherwise (default is `object_fields_match').
        **kwargs: All keyword arguments required by `predicate`.

    Returns:
        Dict with objects where each object's key is its `instance_id` attribute.
    """
    obj_dict = load(filename, obj_type)

    match = {}
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]

        if isinstance(obj, dict):
            continue

        if predicate(obj, *args, **kwargs):
            match[obj_id] = obj

    return match


@low_level_api_log()
def delete_object_by_id(filename, obj_type, obj_id):
    """Delete object from specified file_storage by its key, raising `ObjectNotFoundError` if there is no such key in storage"""
    obj_dict = load(filename, obj_type)
    obj = obj_dict.pop(obj_id, None)
    if obj is None:
        raise errors.ObjectNotFoundError(filename=filename, obj_type=obj_type, obj_id=obj_id)
    dump(filename, obj_dict)


@low_level_api_log()
def delete_objects_by_id_list(filename, obj_type, obj_id_list):
    """Delete all objects whose keys are in `obj_id_list, raising `ObjectNotFoundError` if there is no such key"""
    obj_dict = load(filename, obj_type)
    for obj_id in obj_id_list:
        obj = obj_dict.pop(obj_id, None)
        if obj is None:
            raise errors.ObjectNotFoundError(filename=filename, obj_type=obj_type, obj_id=obj_id)

    dump(filename, obj_dict)


@low_level_api_log()
def delete_objects_by_key_field(filename, obj_type, field_name, field_value):
    """Delete all objects whose `field_name`'s value equals to `field_value`."""
    obj_dict = load(filename, obj_type)
    obj_left = {}
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]

        if isinstance(obj, dict):
            obj_left[obj_id] = obj
            continue

        if obj.__dict__[field_name] != field_value:
            obj_left[obj_id] = obj

    dump(filename, obj_left)


@low_level_api_log()
def delete_first_corresponding_object(filename, obj_type, *args, predicate=object_fields_match, **kwargs):
    """Delete the first object matching predicate from specified file_storage."""
    obj_dict = load(filename, obj_type)
    obj_left = {}
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]

        if isinstance(obj, dict):
            continue

        if not predicate(obj, *args, **kwargs):
            obj_left[obj_id] = obj

    if len(obj_dict) == len(obj_left):
        raise errors.ObjectNotFoundError(filename=filename, obj_type=obj_type)

    dump(filename, obj_left)


@low_level_api_log()
def delete_corresponding_objects(filename, obj_type, *args, predicate=object_fields_match, **kwargs):
    """Delete all objects matching predicate from specified file_storage."""
    obj_dict = load(filename, obj_type)
    obj_left = {}
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]
        if not predicate(obj, *args, **kwargs):
            obj_left[obj_id] = obj

    dump(filename, obj_left)


@low_level_api_log()
def update_object(filename, obj_type, obj_dict, obj_id, **kwargs):
    """Gets object from `obj_dict` by its key updating fields with corresponding values from `**kwargs`.

    Raises:
        ObjectNotFoundError: If there is no such key in `obj_dict`.
    """
    obj = obj_dict.get(obj_id)

    if obj is None:
        raise errors.ObjectNotFoundError(filename=filename, obj_type=obj_type, obj_id=obj_id)

    obj.__dict__.update(kwargs)

@low_level_api_log()
def update_object_by_id(filename, obj_type, obj_id, **kwargs):
    """Update object with specified id changing its fields' values on corresponding values from `**kwargs`"""
    obj_dict = load(filename, obj_type)
    update_object(filename, obj_type, obj_dict, obj_id, **kwargs)
    dump(filename, obj_dict)


@low_level_api_log()
def update_objects(filename, obj_type, id_field_pair_list):
    """Update objects whose ids are listed with corresponding values.

    Args:
        filename (str): Path to a file_storage where the storage is.
        obj_type (type): Type to send to `load`.
        id_field_pair_list (:obj:`list` of :obj:`tuple`): First item in a tuple is object's storage key, second is
                                                          a dict where keys are field names and values are new values to
                                                          be set to corresponding fields of the object whose key is
                                                          tuple's first item.
    """
    obj_dict = load(filename, obj_type)
    for pair in id_field_pair_list:
        update_object(filename, obj_type, obj_dict, pair[0], **pair[1])
    dump(filename, obj_dict)


@low_level_api_log()
def update_single_field(filename, obj_type, obj_dict, obj_id, field_name, field_value):
    """Get object from dict by its id and replace specified field with specified value.

    Raises:
        ObjectNotFoundError: If there is no such key in `obj_dict`.
    """
    obj = obj_dict.get(obj_id)

    if obj is None:
        raise errors.ObjectNotFoundError(filename=filename, obj_type=obj_type, obj_id=obj_id)

    obj_dict[obj_id].__dict__[field_name] = field_value


@low_level_api_log()
def update_objects_with_same_value(filename, obj_type, obj_id_list, field_name, field_value):
    """For each object whose id is in list replace specified field with specified value."""
    obj_dict = load(filename, obj_type)
    for obj_id in obj_id_list:
        update_single_field(filename, obj_type, obj_dict, obj_id, field_name, field_value)
    dump(filename, obj_dict)
