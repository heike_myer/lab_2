##ThisIsMyDesign - Console app

_Console app designed to help you manage your time and daily tasks._

#####Installation how-to:

  - clone project from repo
  - move to `console` directory:
  
        cd console
        
  - install package to your environment:
        
        python setup.py install
        
  - after you've installed app, you need to specify a directory where json files with
  data about your tasks will be located:
  
    - open `config.ini` file, which is in the same folder as this `README.md`
    
    - change `directory` option
    
          [storage]
          directory = <full path to your folder>

    - save changes
    
  - now you're able to launch the program from your terminal:
  
        thisismydesign ...
        
#####Usage

  - first of all, you have to sign up - that will create new user (login is supposed to
  be unique, otherwise you'll see error message):
  
        thisismydesign signup <your login>
        
  - now you can log in (if you don't, other commands won't work):
   
        thisismydesign login <your login>
        
  - once you've logged in, you are free to use all the commands, use `-h` or `--help` 
  for further details:
  
        thisismydesign --help
        
  - to log out, use corresponding command (watch out, you won't be logged out 
  automatically):
  
        thisismydesign logout
        
Enjoy yourself!
