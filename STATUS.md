# __Аналоги__

## Списки дел

- ### Wunderlist

  для личного пользования (чтение, фильмы, список покупок, просто какие-то
  текущие дела) - супер, для командной работы не очень (во всяком случае,
  я не настолько добросовестный человек)

  - #### _плюсы_

    - хорошая иерархия: папки, списки, задачи, подзадачи
    - у свернутого списка видно, сколько в нем всего задач, и сколько просрочено
    - у просроченных задач дедлайн красный, у нормальных синий
    - поиск задачи по названию
    - теги в названии и поиск по ним
    - можно просмотреть завершенные задачи и вернуть из завершенных
    - сортировка по дедлайну: сегодня, неделя (как отдельные папки)
    - сортировки внутри списка (по алфавиту, по дедлайну, по дате создания,
    по исполнителю и по важности)
    - отдельно можно посмотреть отмеченные задачи
    - можно задавать или не задавать для задачи срок выполнения
    - настраиваются напоминания (часы со стрелками, забавно)
    - повторение для задач по дням/неделям/месяцам/годам
    - когда подзадачи выполняются, задача закрашивается (индикатор выполнения),
    но не завершается автоматически после выполнения всех
    - заметки для задач
    - комментарии к задачам (хотя смысл, если это не общий список?)
    - прикрепляются файлы
    - можно создавать общие списки
    - люди, которых уже добавляли, сохраняются
    - в задачах из общего списка можно устраивать беседки (в комментариях)
    - можно назначать исполнителя (только одного) для задачи в общем списке
    - входящие - приглашения от других пользователей
    - при добавлении человека в общий список видно, принял он приглашение или
    еще нет (ожидание/добавлен)
    - отдельно можно смотреть порученные мне задачи
    - чужие комментарии и уведомления можно смотреть отдельно, и оттуда перейти
    на задачу или список
    - все можно редактировать (переименовывать, менять сроки, напоминания,
     переносить задачи из одного списка в другой, менять папку для списка,
     добавлять и переназначать пользователей и т.д.)

  - #### _минусы_

    - одноразовые напоминания о задаче (в смысле я не могу добавить несколько,
    чтобы и за неделю, и за день, и за 5 минут, или чтобы было каждые 2 часа,
    пока не выполню, хотя их можно как бы отложить, но только на 5 минут)
    - папки создаются странно - нельзя создать папку саму по себе, надо сделать
    список и из его меню создать папку
    - завершенные задачи свернуты (это плюс), но внизу списка, долго прокручивать
    - нельзя отдельно просмотреть все завершенные задачи
    - подзадачи совсем не равноценны задачам, нельзя установить сроки,
    назначить исполнителя
    - нельзя просмотреть задачи не списком, а в таблице, как в календаре, и
    чтобы было видно как бы расписание на неделю, месяц, год
    - нельзя разграничить права участников общего списка (слишком демократично:
    назначать на задачу в общем списке может любой участник, и каждый может
    снять себя с задачу или вообще уйти из списка, переименовать задачу или
    список, добавить задачу, убрать задачу, в общем, что хочу, то и ворочу)
    - нет групп (команд, не суть, как называть), чтобы не добавлять пользователей
    в список по одному, а сразу группу

Остальные списки дел похожи на урезанный Wunderlist, только многое убрали в Премиум,
а то, что осталось, местами переименовали или переместили

- ### Фичи в других списках дел

  - #### Todoist

    - карма - добавляются очки за вовремя выполненные задачи, отнимаются за
        просроченные

  - #### Any.do

    - автодополнение
    - можно добавлять задачи и события в календаре
    - встроенные тэги-цвета (но в Премиум)
    - есть напоминания по геолокации (тоже в Премиум), напр., купить лекарства,
     если аптека рядом, но без сети это, очевидно, работать не будет

## Календари Google и Яндекс

Легче управлять временем, следовать плану дня, организовывать встречи и мероприятия.
С командным проектом или списком книг для саморазвития сложно, но календари в
чистом виде для этого и не предназначены.

  - #### _плюсы_

    - создание мероприятий, можно указывать место проведения, приглашать других
    участников, задавать, могут ли они изменять мероприятие, приглашать других
    или видеть список приглашенных
    - четко планируется время (напр., с 12:00 до 14:00 прогулка в лесу)
    - повторяющиеся мероприятия с разными интервалами, можно бесконечные, до
  конкретной даты, определенное число повторов
    - можно задавать несколько уведомлений с разными сроками
    - просмотр календаря на день, неделю, месяц, год
    - просмотр в виде расписания в Google
    - можно импортировать другие календари (в т.ч. задачи со сроками из Wunderlist)
    - в Яндекс можно создавать списки дел и для каждого дела устанавливать
    дату, смотреть отдельно выполненные и просроченные

## Трекеры задач

- ### Яндекс Трекер

Для больших проектов, где много участников, наверное, полезная вещь, но так сразу
освоить нелегко

  - #### _плюсы_
    - к задаче можно добавить подзадачу, и каждая подзадача тоже полноценная
    задача, и наоборот, можно добавить родительскую
    - статус задачи (открыта/в работе/решена/закрыта и др.)
    - можно указать причину, по которой задача закрылась
    - приоритет задачи (блокер/критичный/средний/низкий/незначительный)
    - у задачи могут быть наблюдатели (получают уведомления о любых изменениях
      этой задачи)
    - история изменений задачи (автор изменения, измененные параметры и время изменения)
    - подписка но очередь (на все изменения или только на создание новых
      задач)
    - разные режимы доступа к задаче

# __Сценарии использования__

Все события и персонажи почти вымышлены, любые совпадения почти случайны

### День рождения Алеси

  Крайний срок задачи - 6 апреля 20:00, добавляем всех друзей участниками с равными
  правами. Делаем уведомления о приближении за 2 недели, за неделю, за день.
  Добавляем подзадачи и назначаем исполнителей. Удобно, если у подзадач по
  дефолту выставлен крайний срок, совпадающий с родительским

  - купить торт (ставим время 6 апреля с утра до восьми, потому что торты долго
    не хранятся), займется этим делом Ангелина
  - затариться вином (срок по умолчанию) - Полина
  - подарок (срок по умолчанию), сначала можем никого не назначать, здорово,
    если есть комментарии, сразу там обсуждаем, что дарить, когда определились,
    исполняет Саша

### Следить за здоровьем (бессрочная)

  - подзадача: проехаться по всему Минску на велосипеде, пригласить в задачу друга,
    сделать ее совместной (то есть она будет считаться выполненной, только если и я
    и он поставим птички, что сделали)
  - повторяющаяся задача: не объедаться, повторять каждый день

### Эскиз татуировки на заказ

  Добавляю себя исполнителем, заказчика наблюдателем (будет получать уведомления
  обо всех изменениях). Ставим дедлайн. Добавляю подзадачи: эскиз, прорисовка, в
  цвете. Круто,если можно прикреплять файлы, чтоб заказчик там не перенервничал.
  Внутри эскиза можно сделать еще подзадач для себя (эскиз 1, эскиз 2), но заказчику
  доступ к ним не давать (хотя по умолчанию можно сделать, чтобы подзадачи наследовали
  родительский доступ, но чтобы можно было при создании поменять).

### Шью плюшевого зайца
  - купить ткань
  - сшить шкурку (будет блокироваться покупкой ткани, потому что шить без ткани
    сложновато)
  - купить холлофайбер
  - набить зайца (пока не куплю наполнитель, не смогу, тоже будет блокироваться)

### Лето

  Добавляю задачи на лето: прочитать "Унесенных ветром", досмотреть сериал, научиться
  плавать, прыгнуть с парашютом, научиться готовить лазанью, пройти курс по фотографии
  и обработке фото, нарисовать лошадь на а3, разобраться с нейросетями и машинным
  обучением, скинуть 15 кг и т.д.

  И было бы здорово добавить сюда задачи из других папок: проехаться по городу
  на велосипеде (из бессрочной задачи "Следить за здоровьем").

  Дедлайн, логично, 31 августа. В этот день мы понимаем, что более чем довольны
  нашим летом, и с хотим поставить галочку напротив "Лето". При
  этом было бы неплохо увидеть предупреждение, что с парашютом мы так и не
  прыгнули. Возможно, мы и прыгнули, но от избытка эмоций забыли отметить
  эту задачу. Программа предлагает пометить подзадачи, как решенные. Если мы не
  соглашаемся, есть несколько вариантов:

  0. Не ставим никаких галочек, прыгаем с парашютом, ставим галочку напротив прыжка,
   с чистой совестью отмечаем "Лето"
  0. Просто удаляем прыжок с парашютом из списка задач, и опять же отмечаем "Лето"
  0. Переносим прыжок с парашютом из "Лета" в "Безумные идеи"



# __Планируемые возможности__

  * Создание учетной записи пользователя (логин, пароль, имя, аватарка)

### О задачах

  * У каждого пользователя есть список папок, в самом начале должна быть одна
  по умолчанию (допустим, "Текущие дела"), пользователь может создавать,
  переименовывать и удалять папки. При удалении нужно спросить, точно ли пользователь
  хочет удалить папку. Папка удаляется со всем содержимым. Папка - это список
  задач

  * Внутри каждой задачи есть список подзадач. Но подзадача - полноценная задача,
  и внутри нее тоже может быть список задач, и т.д. Но в принципе эту рекурсию стоит
  ограничить, допустим, максимум 10 уровней. Все-таки это не корпоративное
  какое-нибудь приложение, а для личного пользования, нормальным людям выше крыши
  должно быть

  * Задачи можно удалять (вместе со всем содержимым), переименовывать, перемещать
  из одной папки или родительской задачи в другую, редактировать все сведения о
  ней

  * Задачи могут быть 3 видов: задача, мероприятие, напоминание (пока хз, как это
  лучше оформить для пользователя)

### У задачи будет:

  0. #### _Срок: от, до, бессрочно_

    - если указано бессрочно, то "от" и "до" становятся серенькими и на нажатие
    не реагируют, и эта задача считается просто задачей
    - если указано только "до", то "от" считается от сейчас, это тоже просто задача
    - если указано "от" и "до", то это мероприятие

  0. #### _Повторяемость_

    В идеале будет как в Гугл Календаре напоминания, только еще можно не просто время, а "от"-"до"

    - для бессрочных задач тоже серенькая и не реагирует
    - можно повторять ежедневно, еженедельно (с выбором дня недели), ежемесячно
    (указав число), ежегодно, каждые 3 дня, каждые 7 лет - интервал, короче
    - можно задать число повторений (всегда, каждый интервал до какого-то дня,
    просто столько-то раз)

  0. #### _Список уведомлений о ее приближении_

    - для бессрочных опять же не работает
    - то есть их несколько может быть, за год (1, 2 года, и т.д.), за неделю, да день,
    за час, за эн минут
    - для обычных задач, логично, о приближении дедлайна ("до"), для мероприятия
    его начало ("от")
    - когда вылазит уведомление, есть галочка "решено", тогда задача считается
    решенной (если все остальное в порядке, но об этом позже), и... ну, "спасибо",
    например, и еще просто крестик, чтобы закрыть уведомление

  0. #### _Приоритет_

    Срочно, средний, низкий, без приоритета

    - можно сделать типа кружочки или флажки разных цветов, чтобы при отображении
    задачи были видны
    - в идеале чтобы пользователь мог добавить еще свои приоритеты и цвета сам
    для них выбрать
    - и еще в идеале, если поставить в соответствие приоритету число (1 - максимальный,
    например, 10 - минимальный), и потом сделать прогрессбар для родительской
    задачи (в смысле срочная задача ценится больше, и отрезок в прогрессбаре
    на нее больше,потому что если только на количество выполненных дочерних смотреть,
    не очень получается, можно выполнить много маленьких незначительных подзадачек, а
    выглядеть будет, как будто 90 % запилил), но тогда пользовательские
    приоритеты должны в эту шкалу укладываться

  0. #### _Связанные задачи_

    - можно задать связанные задачи при создании и редактировании
    - если для задачи указать связанную, то срок, повторения и уведомления
    автоматически считаются такими же, как у той, с которой связали
    - 2 связанные между собой задачи считаются решенными, если обе решены

  0. #### _Блокирующие задачи_

    - можно задать при создании и редактировании
    - пока не решен блокер, саму задачу тоже нельзя решить

  0. #### _Блокируемые задачи_

    - когда добавляют задачу как блокирующую, в ее список блокируемых вносится та,
    куда ее добавили
    - можно добавить, тогда у добавленных эта добавится в список блокирующих

  0. #### _Статус_

    Открыта, в процессе, блокируется, связана, решена, провалена

    - сначала, сразу после создания, статус = открыта
    - в процессе - будет какая-нибудь кнопка, чтобы взять задачу в работу (если
      есть блокеры, то будет неактивна, пока блокеры не решатся)
    - блокируется - какой-нибудь значок, и при этом нельзя поставить галочку
    в решена
    - связана - тоже значок, можно отметить, как решенную, но будет висеть,
    пока не будет решена связанная, но не будет считаться просроченной
    - решена - пользователь ставит галочку, задача исчезает из списка (если
    не блокируется и не связана)
    - провалено - если срок "до" прошел, а галочку на решено не поставили, ну и
    не бессрочная задача была, и еще красным или розовым цветом ее выделять
    - в идеале еще, чтобы пользователь мог добавить свои какие-нибудь статусы

  0. ####  _Список групп_

    Скорее всего, задаются в виде тегов

    - можно несколько тегов (ясное дело) к одной задаче
    - будут несколько изначально (дом, семья, работа, саморазвитие и т.д.)
    - можно добавлять свои сколько угодно  

  0. #### _Владелец_

  0. #### _Участники_

    Список пользователей, которым открыт доступ к задаче (его определяет владелец)  

  0. #### _Исполнитель_

    Только один - тот, кто может менять статус задачи (если это не групповая
    задача, то это сам владелец), если не указан, то это могут делать все участники
    Если указан, остальные участники получают уведомления об изменении статуса,
    но могут только смотреть

  0. #### _Прикрепленные файлы_

  0. #### _Комментарии (чат)_

### Права доступа

  * Должен быть список контактов, куда можно напрямую добавить другого
  зарегистрированного пользователя по логину. Доступ можно предоставлять всем,
  кто есть в контактах

  * Можно дать доступ любому зарегистрированному пользователю, при этом он
  автоматически заносится в список контактов

  * Доступ можно предоставить к папке - тогда по умолчанию предоставится доступ
  ко всему ее содержимому, которое уже создано. Для будущих при создании будет
  вариант доступа "как к папке"

  * Доступ можно предоставить к задаче - по умолчанию получим доступ ко всем
  подзадачам (аналогично папкам, "как в родительской")

  В принципе, и "как к папке", и "как в родительской" можно назвать "всем",
  это подразумевает, всем, кто добавлен в список участников на уровень выше

  * Права доступа к задаче

  Во-первых, доступ имеют только пользователи, которых владелец укажет в
  списке участников. Но у них тоже могут быть разные права, так что перед тем,
  как что-то делать с задачей (даже отобразить) по просьбе участника, будем
  проверять, что вообще ему разрешено:

  Права на редактирование:

  - админ - может редактировать сведения о задаче, давать или забирать
    доступ, назначать исполнителей/наблюдателей и т.д, изначально владелец является
    админом, но можно назначить вообще всех участников в списке (и когда админ
    видит задачу, будет иконка какая-нибудь, что он админ)
  - обычные - не админы

  И админ/обычный будет задаваться прямо в списке участников, будет отдельно
  такое поле напротив имени участника.

  Права на изменение статуса:

  - исполнитель - только один, может менять статус задачи (админ не может тогда)
  - остальные получают уведомления и оповещения об изменении, если указан исполнитель,
   статус менять не могут
  - если не задан исполнитель, то изменить статус может любой участник

  Исполнитель будет задаваться отдельно, не в списке участников, но выбирать
  можно только из списка.

### Возможности

У задачи есть 2 варианта отображения:

  - развернутый (вообще вся информация о задаче, появляется, если "нажать" на задачу)
  - свернутый: срок, приоритет, кому назначена, сколько нерешенных подзадач,
    сколько из них просрочены, и т.д.

Еще будут отдельно что-то вроде папок:

  - задачи на сегодня
  - задачи на завтра
  - задачи на неделю
  - задачи на месяц
  - задачи, где я исполнитель
  - задачи, где я наблюдатель
  - завершенные задачи
  - просроченные задачи
  - в процессе

Поиск задач

  - по названию
  - по группам-тегам

Просмотр в виде календаря или чтобы можно было потом календарь из моего приложения
импортировать в Google или Outlook (в идеале)

Темы для приложения (в идеале)

# __Логическая архитектура__

## __Сущности__

### _Пользователь_
  - id
  - логин
  - пароль
  - имя
  - почта
  - список задач
  - список контактов
  - входящие

Логин должен быть уникальным, поэтому вместо него можно взять почту, сразу 2 зайцев
убиваем - и уникальная, и запоминается.

### _Список пользователей_

Пользователи хранятся в отдельном словаре с ключами-айдишниками. По сути, это
большая БД на сервере, где будет храниться вообще вся информация. Будет список
всех пользователей с их айди, логинами, паролями, именами и почтой. Контакты -
это "многие ко многим", нужна отдельная таблица для этих связей.

### _Список задач пользователя_

Задачи будут все вместе хранится в большой БД на сервере. Связь пользователи-задачи
хранится отдельно. Это "многие ко многим", т.к. у нас есть групповые задачи.

По-хорошему, у каждого пользователя должна быть локальная копия части этой БД, с
принадлежащими ему задачами. И периодически изменения должны пушиться на сервер
и забираться с него (потому что есть групповые задачи).
Но это намного сложнее, чем хотелось бы.

У каждой задачи будет id. Все пользовательские задачи будут хранится в словаре.
Ключ - это id, значение - сама задача.

### _Задача_
  - id
  - родительская задача
  - список подзадач
  - список блокирующих задач
  - список связанных задач
  - заголовок
  - описание (возможно)
  - промежуток времени
  - повторение
  - приоритет
  - список тегов
  - список уведомлений
  - статус
  - прикрепленные файлы (возможно)


  - владелец (один к одному)
  - исполнитель (один к одному)
  - список админов (один ко многим)
  - список участников
  - список ожидающих участников


### _Как хранятся все эти списки_

Мы храним список ключей наших подзадач, блокирующих и связанных задач, админов,
наблюдателей, уведомлений, тегов.
Родительская задача тоже хранится как ключ словаря. Для БД это отдельные таблицы
связей.

### _Почему "возможно"_
Описание и файлы занимают много места, и держать их в памяти постоянно
глупо. И я пока не придумала, что мне с этим делать.

### _Папка_

Фактически, это тоже задача, только на самом верхнем уровне (родительская
задача = null). От задачи у нее останется id, список подзадач (уже обычных),
заголовок, описание, список участников.

### _Промежуток времени_
  - начало
  - конец

Если не указан конец, считается бессрочным. Если не указано начало, то началом считается момент создания. Поэтому если вообще не задан (null), считаем
бессрочным с момента создания.

### _Повторение_
  - единицы измерения (часы, дни, недели, месяцы, годы)
  - интервал повторения (в единицах измерения)
  - дата окончания (если не задано, то считается, что никогда)
  - число повторений (логично, что одновременно с датой окончания задавать
    нельзя, если оба не заданы - считаем, что нет окончания)

### _Приоритет_

Может принимать какое-либо значение от 0 до 10, 0 - наиважнейший.

### _Теги_

Теги у каждого пользователя свои, как и список задач. Изначально будут какие-то
общие, по умолчанию (дом, семья, работа и т.д.). Пользователь может добавлять
свои теги. Они будут храниться в отдельном словаре, сам тег и будет ключом
(потому что смысла нет создавать идентичные теги), или однозначно приводиться к
ключу. Значением будет список из id задач, к которым он применен. Внутри
задачи хранится список из ключей тегов. Получается "многие-ко-многим".

### _Уведомление_
  - единицы измерения
  - интервал до задачи (если указано начало, то до него, если только конец, то
    до конца)
  - (возможно) флаг: отправлять уведомление на почту или нет

### _Участник_

Когда пользователь предоставляет доступ к своей задаче другим, они становятся
участниками. Нужно внести эту задачу в их список задач. Для этого добавим
соответствующие связи в таблицу "пользователи-задачи".
Чтобы собрать список участников из БД, нужно выудить из таблицы "пользователи-задачи"
всех пользователей с нужным айди задачи. Чтобы проверить права доступа, нужно
посмотреть, содержится ли участник в списке админов или исполнителей этой задачи.

### _Статус_
Принимает одно из значений: открыта, в процессе, блокируется, связана, решена,
провалена

## __Взаимодействие между сущностями__

### _Менеджер задач_

Я не уверена, но, по-моему, мне понадобится что-то такое.
Это сущность, которая будет следить за сроками. Что я от нее хочу:

  - чтобы она работала постоянно (в фоне)
  - она каким-то образом хранит сроки (моменты времени), они, ясное дело, не
  могут повторяться
  - каждому сроку соответствует список айдишников задач, заинтересованных в нем
  - когда я задаю срок для задачи, она просит менеджера добавить ее к нужному
  сроку, если такого срока еще не было, создать его
  - если по каким-то причинам у задачи меняется срок или ее удаляют (просто
  удаляют или решают), ее убирают из заинтересованных в старом сроке
  - самое важное: менеджер отслеживает, не наступил ли какой-то из сроков,
  которые он хранит, если наступил, то оповещает всех подписчиков, и удаляет
  срок (потому что время, увы, вспять не повернуть)
  - если задача циклическая, то она либо сразу регистрируется во всех своих сроках
  (если не бесконечная), либо каждый раз, когда ее оповещают о наступившем сроке,
  регистрируется на следующий
  - в принципе, с уведомлениями такая же схема, и они точно сразу регистрируются
  на все свои сроки, и еще может быть вариант отложить (допустим, по умолчанию
  на 5 минут), и тогда она еще и этот срок добавит после того, как ее оповестили

Проблема в том, чтобы найти кого-то, кто умеет собственно контролировать время,
но такие штуки наверняка уже были написаны

И возможно отдельно будет менеджер задач, и отдельно - менеджер уведомлений,
чтобы им не надо было думать, в хранилище чего нужно лезть

### _Что делает задача, когда наступил ее срок_

Итак, задача узнала, что вот он, срок (начало для мероприятия или дедлайн, не
суть важно). Во-первых, пользователь получает оповещение. Если он не говорит,
что решил ее, статус меняется на провалено.

P.S. У проваленных задач тоже можно менять дедлайн, тогда она перестает считаться
проваленной, и все по новой. Может быть, при оповещении о дедлайне кроме вариантов
"решить" и "закрыть" будет сразу "перенести".

### _Задачи на сегодня/неделю/месяц, порученные мне и т.п_
Видимо, это будет запрос к БД, чтобы получить все подходящие задачи.

Но вообще, можно поступить как с тегами. Например, для порученных мне я буду
отдельно хранить список айдишников всех таких задач. И когда мне нужно показать
их все, не буду искать в общем хранилище, а сразу достану их из него по ключу.

Со временем примерно то же, но их хорошо бы еще хранить в отсортированном виде,
чтобы каждый день/неделю/месяц не пересматривать все общее хранилище, а забирать
из как бы бывшего следующего. В смысле мы задачи на сегодня перетягиваем из задач
на завтра, при этом старые задачи на сегодня мы убираем из всех (их уже нет ни в
неделе, ни в месяце). Значит, задачами на завтра станут первые сколько-то задач
из новых недельных. И в неделю тоже добавятся задачи, но нужно в месяце найти
место, с которого начинается последний день недели.

### _Как оповещать пользователей_

Входящие у пользователя - это список всех оповещений. У входящих должен быть
флаг "просмотрено", устанавливается, когда мы их отображаем. Если поступили
свежие оповещения, что-нибудь моргает/высвечивается/выползает и т.п. В идеале
должно быть пушап-уведомление.

#### Какие бывают оповещения

  - предварительное уведомление о задаче (тип "осталось 7 дней" и т.п.)
  - дедлайн
  - начало/конец мероприятия
  - повторяющаяся задаче
  - пользователю предложили доступ к задаче
  - пользователь согласен вступить в список участников
  - пользователь покинул список участников
  - пользователь назначен/снят исполнителем
  - статус задачи изменен

Оповещения получают все участники (может быть, будет вариант отказаться от них)

#### _Менеджер оповещений_

  - получает от задачи ее айди и нужное сообщение
  - создает оповещение
  - заносит оповещение во входящие для каждого пользователя

  Каждый раз, когда информация о задаче редактируется (ну, или в первые заполняется),
  и пользователь нажимает "сохранить", задача также посылает свой айди и сообщение
  менеджеру оповещений. Он:
  - сравнивает старую информацию о задаче в БД с новой (если редактировалась)
  - если что-то изменилось, создает оповещение, заносит во входящие

### _Как создавать и редактировать задачи_

#### Создание

Создаем новый объект, заполняем все поля, добавляем в БД.

#### Редактирование (наверное, зависит от БД)

Копируем объект из БД, отображаем эту копию, вносим все изменения в копию. Если
пользователь хочет отменить изменения, просто выходим, не сохраняя копию. Если
пользователь хочет сохранить, то

  1. отправляем менеджеру оповещений айди старой задачи и новый объект
  2. или аккуратненько смотрим, где что изменилось, и меняем то, что хранится в
  БД в нужных местах, или просто удаляем оригинал и сохраняем копию.

#### Перемещение

Переносим задачи вместе со всем содержимым. Задача знает только о своем родителе
и своих дочерних.
Мы должны поменять родителя и дочернюю, по сути, удалить старую связь в таблице
связей задача-подзадача и добавить новую.

## Внешний вид

  - вертикальное меню слева (примерно 1/6 всего места), можно свернуть при желании
  - справа от меню и до конца будет выводится информация о задачах:
    - в самом верху - строка поиска, чтобы вбить тег или название
    - дальше панелька, где, когда надо, будет название задачи (или пункта меню)
    и кнопка "сортировать"
    - ниже - список задач в свернутом виде (подзадач или ответов на поисковый запрос)

### Пункты меню:
  - краткая информация о текущем пользователе (имя, аватарка), ЛЩМ -
    контекстное меню (как минимум выход, параметры учетной записи)
  - оповещения (колокольчик) - на той же строчке, что и имя, ЛЩМ - появится
    окошко-список с оповещениями (чем раньше получено, тем, очевидно, ниже в
    списке), если есть непрочитанные, колокольчик, допустим, становится красным
    и моргает
  - контакты (книжечка) - правее колокольчика, ЛЩМ - окошко-список с именами и
    логинами людей из списка контактов
  - создать папку

ЛЩМ - вылазит окошко, нужно ввести заголовок, можно описание, задать участников,
  кнопки "отмена" и "готово" - появляется в списке пользовательских папок

  - день
  - неделя
  - месяц
  - возможно год, но слишком много информации придется выводить

  ЛЩМ по дню...месяцу - выводится список задач,подходящих по дате и
  отсортированных по дате, независимо от их уровня вложенности. На панельке
  с названием выводится соответственно "День хх.хх.хххх", "Неделя хх.хх.хххх -
  хх.хх.хххх", "Месяц хх.хххх". Изначально день - сегодня, неделя - ближайшие
  7 дней, месяц - ближайшие 28 дней. Рядышком должны быть кнопки "следующий"
  и "предыдущий", чтобы листать. Их можно подгружать в фоне. Причем в неделе
  и месяце задачи за один день группируются вместе.
  - выполненные, ЛЩМ - справа в обратном порядке все решенные. Возможно сгруппированы
    по родительской задаче или по папке, но лучше по дню выполнения
  - мероприятия
  - папки (изначально "текущие дела", потом добавятся пользовательские),
    отсортированы по алфавиту, ЛЩМ по папке - выводится список задач,
    непосредственно дочерних для папки, в панели с названием - имя папки

Справа скраю возле имени папки или пунктов день...месяц:
  - сколько всего задач (для папок - именно дочерних)
  - сколько из них просрочено

  Папок может быть много, нужна прокрутка (но отдельно от других пунктов меню).

### Отображение задач и подзадач
  - в панельке с названием - имя задачи, если есть, то сколько дочерних
  и из них - просроченных
  - рядышком кнопка добавить подзадачу (если в этот момент не открыта задача, т.е.
    название на панельке не написано, добавляем в "Текущие дела"), ЛЩМ - окошко
    с полной информацией о задаче (для записи)
  - если подзадача также  имеет подзадачи, должен быть значок (треугольник слева, напр.),
   ЛЩМ по задаче - отобразится уже она (панелька с названием - ее название,
  ее подзадачи и т.д.)
  - двойной ЛЩМ по задаче - появится окошко с полной информацией о задаче,
  есть кнопка редактировать (если есть права)
  - ПЩМ по задаче - контекстное меню (удалить, переместить и др.)

Полная информация о задаче - это все ее поля, есть кнопки редактировать и готово.
Пока не нажато редактировать, ничего изменять нельзя (не реагирует на нажатия).

### Добавление участников
  - в окошке с полной инфой о задаче есть пункт "Участники"
  - это список участников
  - напротив участника написано, владелец он или исполнитель (если другое, ничего не написано)
  - когда нажато редактировать, над списком появляется поле, где пишем имя или
  логин, и кнопка добавить
  - сначала ищем такого пользователя в своем списке контактов, если его нет,
  ищем в БД, если вообще нет - сообщение об ошибке
  - участнику отправляется оповещение
  - пока новый участник не ответил принять, напротив него "ожидание"
  - нам приходит оповещение, принял или нет (если да, то заносится в список участников)
  - в идеале автодополнение
  - кнопка удалить напротив участника (только у админа или владельца, причем владелец
    других админов удалить может, а его никто)
  - если участник удален, ему посылается оповещение, его убирают из списка участников

# __Этапы разработки__

0. ## Консоль
  Будут написаны базовые классы и прописана основная логика: создание пользователя,
  добавление/удаление/в идеале редактирование(хотя бы без вывода, просто чтобы
  было) задач, установка сроков, атрибутов, и чтобы работал менеджер задач.
  Возможно предоставление доступа. Более-менее удобоваримое меню.
  Все данные будут храниться в файлах (json). Будет отдельный файл, хранящий
  всех пользователей (ну, штук 5, для примера). Для каждого пользователя будет
  свой файл со списком задач и файл с тегами. Все это дело при запуске будет
  читаться и заноситься в словари.

0. ## UI
  Все окна, панели и т.д.

0. ## Добавление БД
  Переносим все из файлов в БД, можно добавить прикрепленные файлы и описание.

0. ## Веб
  Добавляем календарь, возможность настроить уведомления и оповещения на почту

# __Сроки__
***
Консоль до 15.04.2018

UI до 01.05.2018

БД до 20.05.2018

Веб до 10.06.2018

# __Статус__

0. Рассмотрены похожие приложения
0. Примерно определены планируемые возможности
0. Частично разработана логическая архитектура
0. В целом определены этапы разработки
0. Определены сроки для каждого этапа
0. Отчет корректируется
