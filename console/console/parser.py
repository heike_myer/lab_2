import argparse

import dateutil.parser

import console.handlers as handlers
import file_storage.models.str_constants as str_constants


import re


from dateutil.relativedelta import relativedelta
import dateutil.rrule as rrule

from console.errors import ConsoleError

from datetime import datetime
import dateutil.parser

frequency_map = {
    'M': rrule.MINUTELY,
    'H': rrule.HOURLY,
    'd': rrule.DAILY,
    'w': rrule.WEEKLY,
    'm': rrule.MONTHLY,
    'Y': rrule.YEARLY
}


weekday_map = {
    'MO': rrule.MO,
    'TU': rrule.TU,
    'WE': rrule.WE,
    'TH': rrule.TH,
    'FR': rrule.FR,
    'SA': rrule.SA,
    'SU': rrule.SU
}


def to_frequency(frequency_str):
    frequency = frequency_map.get(frequency_str)
    if frequency is None:
        raise ConsoleError('No such frequency option.')

    return frequency


def to_weekday_list(weekday_str_list):
    weekday_list = []
    for weekday_str in weekday_str_list:
        weekday = weekday_map.get(weekday_str)
        if weekday is None:
            raise ValueError('No such weekday.')
        weekday_list.append(weekday)

    return weekday_list


def to_periodicity_rule(rule_str):
    #'freq=`Y|m|w|d|H|M` interval=<int> until=<datetime string>|count=<int> weekday=[]'
    rule_kwargs = {}

    freq_expr = r'(?<=\bfreq=)[YmwdHM]{1}(?=(\s|$))'
    freq_match = re.search(freq_expr, rule_str)

    if freq_match is None:
        raise ValueError('Frequency is not specified or is in the wrong format.')

    freq = to_frequency(freq_match.group())
    rule_kwargs['freq'] = freq

    interval_expr = r'(?<=\binterval=)[\d]+(?=(\s|$))'
    interval_match = re.search(interval_expr, rule_str)
    if interval_match is None:
        raise ValueError('Interval is not specified or is in the wrong format.')

    interval = int(interval_match.group())
    rule_kwargs['interval'] = interval

    count_expr = r'(?<=\bcount=)[\d]+(?=(\s|$))'
    count_match = re.search(count_expr, rule_str)

    until_expr = r'(?<=\buntil=)[\d]{1,4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}(?=(\s|$))'
    until_match = re.search(until_expr, rule_str)

    if count_match is not None and until_match is not None:
        raise ConsoleError('Cannot specify both count and until options.')

    elif count_match is None and until_match is None:
        raise ValueError('Either count or until required, check that one of them is specified and properly formatted.')

    if count_match is not None:
        count = int(count_match.group())
        rule_kwargs['count'] = count

    else:
        until = dateutil.parser.parse(until_match.group())
        rule_kwargs['until'] = until

    if freq == frequency_map['w']:
        byweekday_expr = r'(?<=\bbyweekday=\[)[A-Z\s]+(?=(\]|\s|$))'
        weekday_match = re.search(byweekday_expr, rule_str)

        weekday_str_list = weekday_match.group().split()
        weekday_list = to_weekday_list(weekday_str_list)
        rule_kwargs['byweekday'] = weekday_list

    return rule_kwargs


relative_delta_fields = {
    'Y': 'years',
    'm': 'months',
    'd': 'days',
    'H': 'hours',
    'M': 'minutes'
}


def to_relative_delta(relative_delta_str):
    #'10Y 5H 10H 18M 40m 14d'
    expr = '([\d]+)([YmdHM])'
    match_list = re.findall(expr, relative_delta_str)

    if match_list:
        relative_delta = relativedelta()
        for match in match_list:
            amount = int(match[0])
            field = relative_delta_fields[match[1]]
            relative_delta.__dict__[field] += amount

        return relative_delta

    else:
        raise ValueError('Unsupported relative delta format.')


def to_datetime(datetime_str):
    datetime = None
    if datetime_str is not None:
        datetime = dateutil.parser.parse(datetime_str)

    return datetime


def create_parent_task_id_parser():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('task_id', help='id of the task you are interested in')

    return parser


def create_parent_task_parser():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-d', '--deadline', type=to_datetime,
                        help='quoted string containing a date in almost any datetime format specifying the deadline')
    parser.add_argument('-p', '--priority', dest='priority_str',
                        choices=[str_constants.HIGH, str_constants.MEDIUM, str_constants.LOW, str_constants.NONE],
                        default=str_constants.NONE,
                        help='order of importance or urgency of the task (default is NONE)')

    return parser


def create_parent_relative_delta_parser():
    relative_delta_parser = argparse.ArgumentParser(add_help=False)
    group = relative_delta_parser.add_argument_group(title='relative time delta',
                                                     description='specifies how far in advance reminder is expected '
                                                                 '(timespan is calculated as the sum of all arguments)')
    group.add_argument('--years', type=int, default=0)
    group.add_argument('--months', type=int, default=0)
    group.add_argument('--days', type=int, default=0)
    group.add_argument('--hours', type=int, default=0)
    group.add_argument('--minutes', type=int, default=0)

    return relative_delta_parser


def create_parent_relation_parser():
    relation_parser = argparse.ArgumentParser(add_help=False)
    relation_parser.add_argument('task_from', metavar='from', help='id of a task relation is going from')
    relation_parser.add_argument('task_to', metavar='to', help='id of a task that is being related')

    return relation_parser


def create_parent_tag_parser():
    tag_parser = argparse.ArgumentParser(add_help=False)
    tag_parser.add_argument('tag', help='quoted string containing tag')

    return tag_parser


def create_parent_add_remove_parser():
    add_remove_parser = argparse.ArgumentParser(add_help=False)
    group = add_remove_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-a', '--add', action='store_true', help='set -a flag to add new')
    group.add_argument('-r', '--remove', action='store_true', help='set -r flag to remove existing')

    return add_remove_parser


def init_create_parser(create_parser, task_id_parser, task_parser):
    create_subparsers = create_parser.add_subparsers(title='create subcommands')

    create_box_parser = create_subparsers.add_parser('box', help='create new box')
    create_box_parser.add_argument('title', help='quoted string containing the title of a new box')
    create_box_parser.set_defaults(handler=handlers.create_box_handler)

    create_task_parser = create_subparsers.add_parser('task', parents=[task_parser], help='create new task')
    create_task_parser.add_argument('title', help='quoted string containing the title of a new task')
    create_task_parser.add_argument('--box', default='My Box', help='title of a box where to put a new task')
    create_task_parser.set_defaults(handler=handlers.create_task_handler)

    create_subtask_parser = create_subparsers.add_parser('subtask', parents=[task_id_parser, task_parser],
                                                         help='create subtask for a task with specified id')
    create_subtask_parser.add_argument('title', help='quoted string containing the title of a new task')
    create_subtask_parser.set_defaults(handler=handlers.create_subtask_handler)


def init_set_parser(set_parser, task_id_parser):
    set_subparsers = set_parser.add_subparsers(title='set subcommands')

    set_deadline_parser = set_subparsers.add_parser('deadline', parents=[task_id_parser], help='set deadline')
    set_deadline_parser.add_argument('term', type=to_datetime, help='quoted string containing a date in almost any '
                                                                    'datetime format specifying the deadline ')
    set_deadline_parser.set_defaults(handler=handlers.set_deadline_handler)

    set_reminder_parser = set_subparsers.add_parser('reminder', parents=[task_id_parser],
                                                    help='use to set reminder')
    set_reminder_parser.add_argument('timedelta', type=to_relative_delta,
                                     help='quoted string containing duration in following format: '
                                          '`<amount>Y <amount>m <amount>d <amount>H <amount>M`, '
                                          'options may go in any order, may also be omitted or repeated, '
                                          'result is the sum of all amounts.')
    set_reminder_parser.set_defaults(handler=handlers.set_reminder_handler)

    set_periodicity_parser = set_subparsers.add_parser('periodicity', parents=[task_id_parser],
                                                       help='use to set periodicity')
    set_periodicity_parser.add_argument('rule', type=to_periodicity_rule,
                                        help='quoted string containing periodicity rule in following format: '
                                             '`freq=`Y|m|w|d|H|M` interval=<int> until=<datetime string>|count=<int> '
                                             'byweekday=[MO TU WE TH FR SA SU]`, '
                                             'options may go in any order, '
                                             'weekday constants should be separated with spaces.')

    set_periodicity_parser.set_defaults(handler=handlers.set_periodicity_handler)


def init_unset_parser(unset_parser, task_id_parser):
    unset_subparsers = unset_parser.add_subparsers(title='unset subcommands')

    unset_deadline_parser = unset_subparsers.add_parser('deadline', parents=[task_id_parser],
                                                        help='use to unset deadline')
    unset_deadline_parser.set_defaults(handler=handlers.unset_deadline_handler)

    unset_reminder_parser = unset_subparsers.add_parser('reminder', parents=[task_id_parser],
                                                    help='use to unset reminder')
    group = unset_reminder_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--timedelta', type=to_relative_delta, help='duration')
    group.add_argument('--all', action='store_true',
                                       help='use this flag to get rid of all task reminders')

    unset_reminder_parser.set_defaults(handler=handlers.unset_reminder_handler)

    unset_periodicity_parser = unset_subparsers.add_parser('periodicity', parents=[task_id_parser],
                                                           help='use to unset periodicity')
    unset_periodicity_parser.set_defaults(handler=handlers.unset_periodicity_handler)


def init_relate_parser(relate_parser):
    relate_parser.add_argument('--type', dest='relation_type', choices=[str_constants.BLOCKING, str_constants.SIMULTANEOUS],
                               help='relation type (blocking means that to-task cannot be completed before the '
                                    'completion of from-task)')

    relate_parser.set_defaults(handler=handlers.relate_handler)


def init_update_parser(update_parser):
    group = update_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-d', '--deadline', type=to_datetime,
                       help='new deadline')
    group.add_argument('-p', '--priority', dest='priority_str',
                       choices=[str_constants.HIGH, str_constants.MEDIUM, str_constants.LOW, str_constants.NONE],
                       help='new priority')
    group.add_argument('-t', '--title', help='new title')
    update_parser.set_defaults(handler=handlers.update_handler)


def init_permit_parser(permit_parser):
    permit_parser.add_argument('username',
                               help='name of the user you want to permit or allow access')
    permit_parser.add_argument('--access_level', help='level of access you mean',
                               choices=[str_constants.ADMIN, str_constants.EXECUTOR, str_constants.WATCHER])

    permit_parser.set_defaults(handler=handlers.permit_handler)


def init_get_parser(get_parser):
    get_parser.add_argument('--notifications', action='store_true', help='see new notifications')
    get_parser.add_argument('--overdue', action='store_true', help='see overdue tasks')
    get_parser.add_argument('--today', action='store_true', help='see tasks for today')
    get_parser.set_defaults(handler=handlers.get_handler)


def init_view_parser(view_parser):
    view_parser.add_argument('-d', '--depth', action='count', default=0, help='increase output depth')
    view_parser.set_defaults(handler=handlers.view_handler)


def init_move_parser(move_parser):
    group = move_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--box', help='title of a box where you want to put your task')
    group.add_argument('--parent', help='id of a future parent task')

    move_parser.set_defaults(handler=handlers.move_handler)


def init_archive_parser(archive_parser):
    group = archive_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--task', help='id of a task to archive', dest='task_id')
    group.add_argument('--box', help='id of a box to archive')

    archive_parser.set_defaults(handler=handlers.archive_handler)


def init_restore_parser(restore_parser):
    group = restore_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--task', help='id of a task to restore', dest='task_id')
    group.add_argument('--box', help='id of a box to restore')

    restore_parser.set_defaults(handler=handlers.restore_handler)


def init_delete_parser(delete_parser):
    sa = delete_parser.add_mutually_exclusive_group(required=True)
    sa.add_argument('--task', help='id of a task to delete')
    sa.add_argument('--box', help='id of a box to delete')
    delete_parser.set_defaults(handler=handlers.delete_handler)


def init_info_parser(info_parser):
    group = info_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--task', dest='task_id', help='id of a task to show info about')
    info_parser.set_defaults(handler=handlers.info_handler)


def create_subparsers(subparsers):
    task_id_parser = create_parent_task_id_parser()
    task_parser = create_parent_task_parser()
    relation_parser = create_parent_relation_parser()
    tag_parser = create_parent_tag_parser()
    add_remove_parser = create_parent_add_remove_parser()

    create_parser = subparsers.add_parser('create', help='create box, task or subtask')
    init_create_parser(create_parser, task_id_parser, task_parser)

    permit_parser = subparsers.add_parser('permit', parents=[task_id_parser, add_remove_parser],
                                          help='permit or deny access')
    init_permit_parser(permit_parser)

    set_parser = subparsers.add_parser('set', help='set deadline, periodicity or reminders')
    init_set_parser(set_parser, task_id_parser)

    unset_parser = subparsers.add_parser('unset', help='unset deadline, reminder or periodicity')
    init_unset_parser(unset_parser, task_id_parser)

    tag_parser = subparsers.add_parser('tag', parents=[task_id_parser, tag_parser, add_remove_parser], help='tag task')
    tag_parser.set_defaults(handler=handlers.tag_handler)

    relate_parser = subparsers.add_parser('relate', parents=[relation_parser, add_remove_parser],
                                          help='attach or detach relation to another task')
    init_relate_parser(relate_parser)

    update_parser = subparsers.add_parser('update', parents=[task_id_parser],
                                          help='update title, priority or deadline of a task')
    init_update_parser(update_parser)

    get_parser = subparsers.add_parser('get', help='get some specific tasks')
    init_get_parser(get_parser)

    view_parser = subparsers.add_parser('view', help='view hierarchy')
    init_view_parser(view_parser)

    move_parser = subparsers.add_parser('move', parents=[task_id_parser], help='move task to box or to parent')
    init_move_parser(move_parser)

    archive_parser = subparsers.add_parser('archive', help='archive task or box')
    init_archive_parser(archive_parser)

    complete_parser = subparsers.add_parser('complete', help='complete task', parents=[task_id_parser])
    complete_parser.set_defaults(handler=handlers.complete_handler)

    resume_parser = subparsers.add_parser('resume', help='resume task', parents=[task_id_parser])
    resume_parser.set_defaults(handler=handlers.resume_handler)

    restore_parser = subparsers.add_parser('restore', help='restore task or box')
    init_restore_parser(restore_parser)

    delete_parser = subparsers.add_parser('delete', help='delete task or box')
    init_delete_parser(delete_parser)

    info_parser = subparsers.add_parser('info', help='get info about...')
    init_info_parser(info_parser)


def create_main_parser():
    parser = argparse.ArgumentParser(prog='file_storage')

    parser.add_argument('--me', action='store_true', help='get current user')
    parser.set_defaults(handler=handlers.me_handler)

    subparsers = parser.add_subparsers(title='subcommands')
    create_subparsers(subparsers)

    signup_parser = subparsers.add_parser('signup', help='create new user')
    signup_parser.add_argument('username', help='quoted string containing new username')
    signup_parser.set_defaults(handler=handlers.signup_handler)

    login_parser = subparsers.add_parser('login', help='log in')
    login_parser.add_argument('username', help='your username')
    login_parser.set_defaults(handler=handlers.login_handler)

    logout_parser = subparsers.add_parser('logout', help='leave app')
    logout_parser.set_defaults(handler=handlers.logout_handler)

    return parser
