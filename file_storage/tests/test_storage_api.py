import os
import os.path
import shutil

import json


from datetime import datetime
from dateutil.relativedelta import relativedelta
import dateutil.rrule as rrule






import unittest

import sys
sys.path.append('..')

import tests.helpers as settings

import file_storage.models as models
import file_storage.storage.errors as errors
import file_storage.models.str_constants as const
from file_storage.storage.storage_api import StorageAPI
import file_storage.storage.low_level_api as low_level_api


import file_storage.tasktracker_logging as tasktracker_logging


from tests.helpers import (filename, load, load_from, form_storage_paths, contains_dict, contains_items,
                                                 contains_corresponding_dict_obj, contains_corresponding_object)


tasktracker_logging.configure_default_loggers()


class TestCreateTaskBox(unittest.TestCase):
    def setUp(self):
        self.storage = StorageAPI(form_storage_paths())
        if os.path.exists(os.path.join(os.path.dirname(__file__), filename)):
            os.remove(filename)

        shutil.copyfile(os.path.join(os.path.dirname(__file__), 'data_for_tests/data_TestCreateTaskBox.json'), filename)

    def test_create_task_without_deadline(self):
        task_id = self.storage.create_task('user', 'task', priority=models.Priority.HIGH)
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.TITLE: 'task',
                                             const.PRIORITY: const.HIGH, const.INSTANCE_ID: task_id,
                                             const.USER_ID: 'user'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: 'user', const.ACCESS_LEVEL: const.CREATOR}))
        #self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: task_id,
        #                                     const.USER_ID: 'user', const.BOX_ID: 'user_box'}))

    def test_create_deadlined_task(self):
        task_id = self.storage.create_task('user', 'task', deadline=datetime(2000, 1, 1))
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TERM: '2000-01-01T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.TASK_ID: task_id}))
    @unittest.skip('test file needs fixing')
    def test_create_box(self):
        box_id = self.storage.create_box('user', 'mybox')
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.BOX_CLASS, const.TITLE: 'mybox', const.USER_ID: 'user',
                                             const.INSTANCE_ID: box_id}))

    #@unittest.skip('test file needs fixing')
    def test_create_box_with_existing_name(self):
        with self.assertRaises(errors.ObjectExistsError):
            self.storage.create_box('user', 'cool existing box')


class TestSetDeadlineReminderPeriodictyRelation(unittest.TestCase):
    def setUp(self):
        self.storage = StorageAPI(form_storage_paths())
        #full_path = os.path.join(os.path.dirname(__file__), filename)
        if os.path.exists(filename):
            os.remove(filename)

        shutil.copyfile(os.path.join(os.path.dirname(__file__),
                                     'data_for_tests/data_TestSetDeadlineReminderPeriodicity.json'), filename)

    def test_set_deadline(self):
        self.storage.set_deadline('task1', datetime(2005, 4, 27))
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: 'task1',
                                             const.DEADLINE: '2005-04-27T00:00:00'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: 'task1',
                                             const.TERM: '2005-04-27T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))

    def test_set_term_raises(self):
        with self.assertRaises(errors.ObjectExistsError):
            self.storage.set_deadline('task2', datetime(2001, 1, 1))

        with self.assertRaises(errors.ObjectExistsError):
            self.storage.set_reminder('task2', relative_delta=relativedelta(days=1))

        with self.assertRaises(errors.DeadlineUnsetError):
            self.storage.set_reminder('task1', datetime(2001, 1, 1))

        with self.assertRaises(errors.DeadlineUnsetError):
            self.storage.set_periodicity('task1', rrule.DAILY, count=1)

        with self.assertRaises(errors.SubtaskError):
            self.storage.set_periodicity('task3', rrule.DAILY, count=1)


    def test_set_reminder(self):
        self.storage.set_reminder('task3', relative_delta=relativedelta(days=2))
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: 'task3',
                                             const.TERM: '1999-12-29T00:00:00', const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

    def test_set_second_reminder(self):
        self.storage.set_reminder('task2', relative_delta=relativedelta(days=2))
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: 'task2',
                                             const.TERM: '1999-12-31T00:00:00', const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: 'task2',
                                             const.TERM: '1999-12-30T00:00:00', const.TERM_TYPE: const.REMINDER_TERM_TYPE}))


    def test_set_periodicity_for_parent(self):
        self.storage.set_periodicity('task2', rrule.DAILY, count=3)
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: 'task2',
                                             const.PERIODICAL: True,
                                             const.RULE_STR: "DTSTART:20000101T000000\nRRULE:FREQ=DAILY;COUNT=3"}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: 'task3',
                                             const.PERIODICAL: True,
                                             const.RULE_STR: "DTSTART:19991231T000000\nRRULE:FREQ=DAILY;COUNT=3"}))

    def test_set_periodicty_for_subtask_with_nonedeadlined_parent(self):
        self.storage.set_periodicity('task5', rrule.DAILY, count=3)
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: 'task5',
                                             const.PERIODICAL: True,
                                             const.RULE_STR: "DTSTART:20000101T000000\nRRULE:FREQ=DAILY;COUNT=3"}))

    def test_attach_relation(self):
        self.storage.attach_relation('user', 'task2', 'task4', models.RelationType.BLOCKING)

        with self.assertRaises(errors.StorageAPIError):
            self.storage.attach_relation('user', 'task2', 'task4', models.RelationType.BLOCKING)

        self.storage.attach_relation('user', 'task1', 'task4', models.RelationType.SIMULTANEOUS)
        self.storage.attach_relation('user', 'task3', 'task1', models.RelationType.BLOCKING)

        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: 'task2',
                                             const.TASK_TO: 'task4', const.USER_ID: 'user',
                                             const.RELATION_TYPE: const.BLOCKING}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: 'task1',
                                             const.TASK_TO: 'task4', const.USER_ID: 'user',
                                             const.RELATION_TYPE: const.SIMULTANEOUS}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: 'task3',
                                             const.TASK_TO: 'task1', const.USER_ID: 'user',
                                             const.RELATION_TYPE: const.BLOCKING}))

    def test_relation_exists(self):
        self.assertFalse(self.storage.relation_exists('user', 'task1', 'task2'))
        self.assertTrue(self.storage.relation_exists('user', 'task1', 'task5'))


class TestCreateSubtask(unittest.TestCase):
    def setUp(self):
        self.storage = StorageAPI(form_storage_paths())
        if os.path.exists(os.path.join(os.path.dirname(__file__), filename)):
            os.remove(filename)

        shutil.copyfile(os.path.join(os.path.dirname(__file__), 'data_for_tests/data_TestCreateSubtask.json'), filename)

    def test_has_subtasks(self):
        self.assertFalse(self.storage.has_subtasks('task1'))
        self.assertFalse(self.storage.has_subtasks('task3'))
        self.assertFalse(self.storage.has_subtasks('task6'))
        self.assertTrue(self.storage.has_subtasks('task2'))
        self.assertTrue(self.storage.has_subtasks('task4'))


    def test_create_subtask_no_deadline_single_permission(self):
        task_id = self.storage.create_subtask('user', 'task1', 'task1 subtask')
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.TITLE: 'task1 subtask',
                                             const.PARENT_ID: 'task1', const.INSTANCE_ID: task_id,
                                             const.USER_ID: 'user'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: 'user', const.ACCESS_LEVEL: const.CREATOR}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: task_id,
                                              const.USER_ID: 'user', const.BOX_ID: 'user_box'}))

    def test_create_subtask_multiple_permissions(self):
        task_id = self.storage.create_subtask('user', 'task7', 'task7 subtask')
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.TITLE: 'task7 subtask',
                                             const.PARENT_ID: 'task7', const.INSTANCE_ID: task_id,
                                             const.USER_ID: 'user'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: 'user', const.ACCESS_LEVEL: const.CREATOR}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: task_id,
                                              const.USER_ID: 'user', const.BOX_ID: 'user_box'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: 'user1', const.ACCESS_LEVEL: const.WATCHER}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: task_id,
                                              const.USER_ID: 'user1', const.BOX_ID: 'user1_box'}))

    def test_create_multiple_subtasks(self):
        task_id = 'task1'
        subtask_ids = []
        for i in range(10):
            subtask_ids.append(self.storage.create_subtask('user', task_id, 'title'))

        self.assertTrue(self.storage.has_subtasks(task_id))

        data = load()

        for subtask_id in subtask_ids:
            subtask_dict = data[subtask_id]
            self.assertTrue(contains_items(subtask_dict, instance_id=subtask_id, parent_id=task_id))
            self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: subtask_id,
                                                 const.USER_ID: 'user', const.ACCESS_LEVEL: const.CREATOR}))
            self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: subtask_id,
                                                  const.USER_ID: 'user', const.BOX_ID: 'user_box'}))

    def test_create_subtask_to_periodical_task_with_deadline(self):
        subtask_id = self.storage.create_subtask('user', 'task6', 'subtask')
        data = load()

        #print(data[subtask_id])

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '2000-01-01T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PARENT_ID: 'task6',
                                             const.PERIODICAL: True, const.DEADLINE: '2000-01-01T00:00:00',
                                             const.RULE_STR: "DTSTART:20000101T000000\nRRULE:FREQ=DAILY;COUNT=3"}))

    def test_create_deadlined_subtask_to_periodical_task_with_deadline(self):
        subtask_id = self.storage.create_subtask('user', 'task6', 'subtask', deadline=datetime(1999, 1, 1))
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1999-01-01T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PARENT_ID: 'task6',
                                             const.PERIODICAL: True, const.DEADLINE: '1999-01-01T00:00:00',
                                             const.RULE_STR: "DTSTART:19990101T000000\nRRULE:FREQ=DAILY;COUNT=3"}))

    def test_create_subtask_later_deadline(self):
        subtask_id = self.storage.create_subtask('user', 'task2', 'subtask', deadline=datetime(2010, 12, 31))
        data = load()
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '2000-01-01T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PERIODICAL: False, const.DEADLINE: '2000-01-01T00:00:00',
                                             const.PARENT_ID: 'task2'}))

    def test_create_subtask_valid_deadline(self):
        subtask_id = self.storage.create_subtask('user', 'task2', 'subtask', deadline=datetime(1998, 12, 31))
        data = load()

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1998-12-31T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PERIODICAL: False, const.DEADLINE: '1998-12-31T00:00:00',
                                             const.PARENT_ID: 'task2'}))


class TestTag(unittest.TestCase):
    def setUp(self):
        self.storage = StorageAPI(form_storage_paths())
        if os.path.exists(os.path.join(os.path.dirname(__file__), filename)):
            os.remove(filename)

        shutil.copyfile(os.path.join(os.path.dirname(__file__), 'data_for_tests/data_TestCreateSubtask.json'), filename)

    def test_create_tag(self):
        for i in range(1, 10):
            tag = 'tag' + str(i)
            self.storage.create_tag_object(tag)

            data = load()
            self.assertTrue(data[tag]['tag'], tag)

            with self.assertRaises(errors.ObjectExistsError):
                self.storage.create_tag_object(tag)

        data = load()

        self.assertTrue(data['tag1']['tag'], 'tag1')
        self.assertTrue(data['tag2']['tag'], 'tag2')
        self.assertTrue(data['tag9']['tag'], 'tag9')

    def test_attach_tag(self):
        tag = 'tag'

        self.storage.attach_tag('user', 'task1', tag)
        with self.assertRaises(errors.StorageAPIError):
            self.storage.attach_tag('user', 'task1', tag)

        data = load()

        self.assertTrue(data[tag]['tag'], tag)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: 'task1',
                                             const.TAG: 'tag', const.USER_ID: 'user'}))


class TestDeadlineUpdateUnset(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.simple_task_id = self.storage.create_task(self.user, 'task1', deadline=datetime(2000, 1, 1))

        self.simple_task_with_reminders_id = self.storage.create_task(self.user, 'task2', deadline=datetime(2000, 1, 1))
        self.storage.set_reminder(self.simple_task_with_reminders_id, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.simple_task_with_reminders_id, relative_delta=relativedelta(years=1))

        self.periodical_task_id = self.storage.create_task(self.user, 'task3', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.periodical_task_id, rrule.DAILY, count=3)

        self.task_with_subtask_id = self.storage.create_task(self.user, 'task4', deadline=datetime(2000, 1, 1))
        self.subtask_id = self.storage.create_subtask(self.user, self.task_with_subtask_id, 'task5',
                                                      deadline=datetime(1999, 1, 1))

        self.hardcore_task = self.storage.create_task(self.user, 'task6', deadline=datetime(2000, 1, 1))
        self.storage.set_reminder(self.hardcore_task, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.hardcore_task, relative_delta=relativedelta(years=1))
        self.storage.set_periodicity(self.hardcore_task, rrule.DAILY, count=3)

        self.hardcore_subtask = self.storage.create_subtask(self.user, self.hardcore_task, 'task7',
                                                            deadline=datetime(1999, 1, 1))
        self.storage.set_reminder(self.hardcore_subtask, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.hardcore_subtask, relative_delta=relativedelta(years=1))

        self.task_no_deadline = self.storage.create_task(self.user, 'task8')
        self.no_deadline_parent_subtask_id = self.storage.create_subtask(self.user, self.task_no_deadline, 'task9',
                                                                         deadline=datetime(1999, 1, 1))

    def test_update_deadline_simple(self):
        task_id = self.simple_task_id
        self.storage.update_deadline(task_id, datetime(2100, 1, 1))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2100-01-01T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))

    def test_update_deadline_simple_with_reminders(self):
        task_id = self.simple_task_with_reminders_id
        self.storage.update_deadline(task_id, datetime(2100, 1, 1))
        data = load_from(self.paths, models.Term)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2100-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2099-12-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2099-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

    def test_update_deadline_periodical_task(self):
        task_id = self.periodical_task_id
        self.storage.update_deadline(task_id, datetime(2100, 1, 1))

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: True, const.DEADLINE: '2100-01-01T00:00:00',
                                             const.RULE_STR: 'DTSTART:21000101T000000\nRRULE:FREQ=DAILY;COUNT=3'}))

        data = load_from(self.paths, models.Term)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2100-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))

    def test_update_deadline_task_with_subtask_subterm_preserved(self):
        task_id = self.task_with_subtask_id
        subtask_id = self.subtask_id
        self.storage.update_deadline(task_id, datetime(1999, 6, 1))

        data = load_from(self.paths, models.Term)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))

    def test_update_deadline_task_with_subtask_subterm_changed(self):
        task_id = self.task_with_subtask_id
        subtask_id = self.subtask_id
        self.storage.update_deadline(task_id, datetime(1000, 1, 1))

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.DEADLINE: '1000-01-01T00:00:00'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PARENT_ID: task_id,
                                             const.DEADLINE: '1000-01-01T00:00:00'}))

        data = load_from(self.paths, models.Term)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1000-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1000-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))

    def test_update_deadline_subtask(self):
        subtask_id = self.subtask_id

        with self.assertRaises(errors.StorageAPIError):
            self.storage.update_deadline(subtask_id, datetime(2005, 1, 1))

        self.storage.update_deadline(subtask_id, datetime(1000, 1, 1))

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PARENT_ID: self.task_with_subtask_id,
                                             const.DEADLINE: '1000-01-01T00:00:00'}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1000-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))

    def test_hardcore_task_subtask_preserved(self):
        task_id = self.hardcore_task
        subtask_id = self.hardcore_subtask
        self.storage.update_deadline(task_id, datetime(1999, 6, 1))

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: True, const.DEADLINE: '1999-06-01T00:00:00',
                                             const.RULE_STR: 'DTSTART:19990601T000000\nRRULE:FREQ=DAILY;COUNT=3'}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PARENT_ID: task_id,
                                             const.PERIODICAL: True, const.DEADLINE: '1999-01-01T00:00:00',
                                             const.RULE_STR: 'DTSTART:19990101T000000\nRRULE:FREQ=DAILY;COUNT=3'}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1998-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-05-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1998-12-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1998-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

    def test_hardcore_task_subtask_changed(self):
        task_id = self.hardcore_task
        subtask_id = self.hardcore_subtask
        self.storage.update_deadline(task_id, datetime(1000, 6, 1))

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: True, const.DEADLINE: '1000-06-01T00:00:00',
                                             const.RULE_STR: 'DTSTART:10000601T000000\nRRULE:FREQ=DAILY;COUNT=3'}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PARENT_ID: task_id,
                                             const.PERIODICAL: True, const.DEADLINE: '1000-06-01T00:00:00',
                                             const.RULE_STR: 'DTSTART:10000601T000000\nRRULE:FREQ=DAILY;COUNT=3'}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1000-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '0999-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1000-05-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1000-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '0999-06-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1000-05-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

    def test_unset_deadline_periodical(self):
        with self.assertRaises(errors.StorageAPIError):
            self.storage.unset_deadline(self.periodical_task_id)

    def test_unset_deadline_subtask(self):
        with self.assertRaises(errors.StorageAPIError):
            self.storage.unset_deadline(self.subtask_id)

    def test_unset_deadline_subtask_undeadlined_parent(self):
        task_id = self.no_deadline_parent_subtask_id
        self.storage.unset_deadline(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.DEADLINE: None}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id}))

    def test_unset_deadline_task_with_reminders(self):
        task_id = self.simple_task_with_reminders_id
        self.storage.unset_deadline(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.DEADLINE: None}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id}))


class TestUnsetPeriodicity(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.periodical_task_id = self.storage.create_task(self.user, 'task1', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.periodical_task_id, rrule.DAILY, count=3)

        self.periodical_task_with_reminders_id = self.storage.create_task(self.user, 'task3', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.periodical_task_with_reminders_id, rrule.DAILY, count=3)
        self.storage.set_reminder(self.periodical_task_with_reminders_id, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.periodical_task_with_reminders_id, relative_delta=relativedelta(years=1))

        self.hardcore_task = self.storage.create_task(self.user, 'task6', deadline=datetime(2000, 1, 1))
        self.hardcore_subtask = self.storage.create_subtask(self.user, self.hardcore_task, 'task7',
                                                            deadline=datetime(1999, 1, 1))

        self.storage.set_reminder(self.hardcore_task, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.hardcore_task, relative_delta=relativedelta(years=1))

        self.storage.set_reminder(self.hardcore_subtask, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.hardcore_subtask, relative_delta=relativedelta(years=1))

        self.storage.set_periodicity(self.hardcore_task, rrule.DAILY, count=3)

        self.task_with_subtask_id = self.storage.create_task(self.user, 'task4')
        self.subtask_id = self.storage.create_subtask(self.user, self.task_with_subtask_id, 'task5',
                                                      deadline=datetime(1999, 1, 1))
        self.storage.set_periodicity(self.subtask_id, rrule.DAILY, count=3)

    def test_unset_periodicity(self):
        task_id = self.periodical_task_id
        self.storage.unset_periodicity(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: False, const.RULE_STR: None}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True}))

    def test_unset_periodicity_task_with_reminders(self):
        task_id = self.periodical_task_with_reminders_id
        self.storage.unset_periodicity(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: False, const.RULE_STR: None}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-12-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

    def test_unset_periodicity_periodical_parent(self):
        task_id = self.hardcore_subtask

        with self.assertRaises(errors.StorageAPIError):
            self.storage.unset_periodicity(task_id)

    def test_unset_periodicity_req_for_subtasks(self):
        task_id = self.hardcore_task
        subtask_id = self.hardcore_subtask

        self.storage.unset_periodicity(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: False, const.RULE_STR: None}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.PERIODICAL: False, const.RULE_STR: None}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-12-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1998-12-31T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1998-01-01T00:00:00', const.ACTIVE: True,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE}))

    def test_unset_periodicity_subtask(self):
        task_id = self.subtask_id
        self.storage.unset_periodicity(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.PERIODICAL: False, const.RULE_STR: None}))


class TestDetachAttribute(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.user1 = self.storage.create_object(models.User, 'user1', None, None)
        self.creator_box1 = self.storage.create_object(models.Box, self.user1, 'MyBox')
        self.shared_box1 = self.storage.create_object(models.Box, self.user1, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user1, creator_box=self.creator_box1,
                                          shared_box=self.shared_box1)

        self.user2 = self.storage.create_object(models.User, 'user2', None, None)
        self.creator_box2 = self.storage.create_object(models.Box, self.user2, 'MyBox')
        self.shared_box2 = self.storage.create_object(models.Box, self.user2, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user2,
                                          creator_box=self.creator_box2,
                                          shared_box=self.shared_box2)

        self.task_id = self.storage.create_task(self.user, 'task')
        self.second_task_id = self.storage.create_task(self.user, 'task2')
        self.third_task_id = self.storage.create_task(self.user, 'task3')

        self.storage.give_permission(self.user1, self.task_id, models.AccessLevel.ADMIN)
        self.storage.give_permission(self.user1, self.second_task_id, models.AccessLevel.ADMIN)
        self.storage.give_permission(self.user1, self.third_task_id, models.AccessLevel.ADMIN)
        self.storage.give_permission(self.user2, self.task_id, models.AccessLevel.ADMIN)

        self.storage.attach_tag(self.user, self.task_id, 'tag1')
        self.storage.attach_tag(self.user1, self.task_id, 'tag1')
        self.storage.attach_tag(self.user1, self.task_id, 'tag2')

        self.storage.attach_relation(self.user, self.task_id, self.second_task_id, models.RelationType.BLOCKING)
        self.storage.attach_relation(self.user1, self.task_id, self.third_task_id, models.RelationType.BLOCKING)
        self.storage.attach_relation(self.user1, self.second_task_id, self.task_id, models.RelationType.SIMULTANEOUS)


    def test_withdraw_permission(self):
        task_id = self.task_id
        self.storage.withdraw_permission(self.user1, task_id)

        data = load_from(self.paths, models.Permission)
        self.assertFalse(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: self.user1}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                              const.USER_ID: self.user}))

        data = load_from(self.paths, models.TaskTagRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id,
                                              const.USER_ID: self.user1}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: self.user}))

        data = load_from(self.paths, models.Relation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: task_id,
                                              const.USER_ID: self.user1}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: task_id,
                                             const.USER_ID: self.user}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: task_id,
                                              const.USER_ID: self.user1}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: task_id,
                                              const.USER_ID: self.user}))

    def test_detach_tag(self):
        task_id = self.task_id

        self.storage.detach_tag(self.user, task_id, 'tag1')

        data = load_from(self.paths, models.TaskTagRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id,
                                             const.TAG: 'tag1', const.USER_ID: self.user}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id,
                                             const.TAG: 'tag1', const.USER_ID: self.user1}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id,
                                             const.TAG: 'tag2', const.USER_ID: self.user1}))

        self.storage.detach_tag(self.user1, task_id, 'tag1')
        self.storage.detach_tag(self.user1, task_id, 'tag2')

        data = load_from(self.paths, models.TaskTagRelation)
        self.assertDictEqual(data, {})

    def test_detach_relation(self):
        task_id = self.task_id
        second_id = self.second_task_id
        third_id = self.third_task_id

        self.storage.detach_relation(self.user, task_id, second_id)

        data = load_from(self.paths, models.Relation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.USER_ID: self.user,
                                              const.TASK_FROM: task_id, const.TASK_TO: second_id}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.USER_ID: self.user1,
                                              const.TASK_FROM: task_id, const.TASK_TO: third_id}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.USER_ID: self.user1,
                                             const.TASK_FROM: second_id, const.TASK_TO: task_id}))

        self.storage.detach_relation(self.user1, task_id, third_id)
        self.storage.detach_relation(self.user1, second_id, task_id)

        data = load_from(self.paths, models.Relation)
        self.assertDictEqual(data, {})



class TestCompleteTask(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.first = self.storage.create_task(self.user, 'task1')
        self.second = self.storage.create_task(self.user, 'task2', deadline=datetime(2000, 1, 1))

        self.third = self.storage.create_task(self.user, 'task3', deadline=datetime(2000, 1, 1))
        self.storage.set_reminder(self.third, relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.third, relative_delta=relativedelta(years=1))

        self.fourth = self.storage.create_task(self.user, 'task4', deadline=datetime(2000, 1, 1))
        self.fourth_subtask = self.storage.create_subtask('user', self.fourth, 'task4.1', deadline=datetime(2000, 1, 1))

        self.fifth = self.storage.create_task(self.user, 'task5', deadline=datetime(2000, 1, 1))
        self.fifth_subtask = low_level_api.create_object(self.paths[models.Task], models.Task,
                                                         datetime.now(), datetime.now(),
                                                         'user', 'task5.1', deadline=datetime(1999, 1, 1),
                                                         parent_id=self.fifth,
                                                         status=models.Status.COMPLETED)
        low_level_api.create_object(self.paths[models.Term], models.Term, self.fifth_subtask, datetime(1999, 1, 1),
                                    term_type=models.TermType.DEADLINE, active=False)

        self.sixth = self.storage.create_task(self.user, 'task6', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.sixth, rrule.DAILY, count=3)

        self.seventh = self.storage.create_task(self.user, 'task7', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.seventh, rrule.DAILY, count=1)

        self.eigth = self.storage.create_task(self.user, 'task8', deadline=datetime(2000, 1, 1))
        self.eight_subtask = self.storage.create_subtask(self.user, self.eigth, 'task8.1', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.eigth, rrule.DAILY, count=1)

        self.nine = self.storage.create_task(self.user, 'task9', deadline=datetime(2000, 1, 1))
        self.nine_subtask = self.storage.create_subtask(self.user, self.nine, 'task9.1', deadline=datetime(2000, 1, 1))
        self.storage.set_periodicity(self.nine, rrule.DAILY, count=3)

        self.ten = self.storage.create_task(self.user, 'task10')
        self.blocking_task = self.storage.create_task(self.user, 'blocking task 10')
        self.storage.attach_relation(self.user, self.blocking_task, self.ten, models.RelationType.BLOCKING)

    def test_complete_task_no_deadline(self):
        task_id = self.first
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))
        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id}))

    def test_complete_task_with_deadline(self):
        task_id = self.second
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: False}))

    def test_complete_task_with_reminders(self):
        task_id = self.third
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-12-31T00:00:00', const.ACTIVE: False}))

    def test_complete_parent_with_uncompleted_subtask(self):
        task_id = self.fourth
        with self.assertRaises(errors.StorageAPIError):
            self.storage.complete_task(task_id)

    def test_complete_blocked_task(self):
        task_id = self.ten
        with self.assertRaises(errors.StorageAPIError):
            self.storage.complete_task(task_id)

    def test_complete_subtask(self):
        task_id = self.second
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: False}))

    def test_complete_parent_with_completed_subtasks(self):
        task_id = self.fifth
        subtask_id = self.fifth_subtask
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '1999-01-01T00:00:00', const.ACTIVE: False}))

    def test_complete_periodical_task(self):
        task_id = self.sixth
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.PENDING,
                                             const.DEADLINE: '2000-01-02T00:00:00'}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-02T00:00:00', const.ACTIVE: True}))

    def test_complete_periodical_task_last_repeat(self):
        task_id = self.seventh
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))

    def test_complete_periodical_subtask_last_repeat(self):
        parent_id = self.eigth
        task_id = self.eight_subtask
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: parent_id,
                                             const.STATUS: const.PENDING}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: parent_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))

    def test_complete_periodical_subtask(self):
        task_id = self.nine_subtask
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))


class TestCompletePeriodicalTask(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.ten =low_level_api.create_object(self.paths[models.Task], models.Task, datetime.now(), datetime.now(),
                                              self.user, 'task10', deadline=datetime(2000, 1, 1), periodical=True,
                                               rule_str=str(rrule.rrule(rrule.DAILY, dtstart=datetime(2000, 1, 1),
                                                                        count=1)))
        self.ten_subtask = low_level_api.create_object(self.paths[models.Task], models.Task, datetime.now(),
                                                       datetime.now(), self.user, 'task10.1', deadline=datetime(1999, 1, 1),
                                                       parent_id=self.ten, periodical=True,
                                                       rule_str=str(rrule.rrule(rrule.DAILY,
                                                                                dtstart=datetime(1999, 1, 1),
                                                                                count=1)),
                                                       status=models.Status.COMPLETED)
        low_level_api.create_object(self.paths[models.Term], models.Term, self.ten_subtask, datetime(1999, 1, 1),
                                    term_type=models.TermType.DEADLINE, active=False)
        low_level_api.create_object(self.paths[models.Term], models.Term, self.ten, datetime(2000, 1, 1),
                                    term_type=models.TermType.DEADLINE, active=False)

        self.eleven = low_level_api.create_object(self.paths[models.Task], models.Task, datetime.now(),
                                                  datetime.now(), self.user, 'task11', deadline=datetime(2000, 1, 1),
                                                  periodical=True, rule_str=str(rrule.rrule(rrule.DAILY,
                                                                                            dtstart=datetime(2000, 1, 1),
                                                                                            count=3)))
        self.eleven_subtask = low_level_api.create_object(self.paths[models.Task], models.Task, datetime.now(),
                                                          datetime.now(), self.user, 'task11.1',
                                                          deadline=datetime(1999, 1, 1), parent_id=self.eleven,
                                                          periodical=True,
                                                          rule_str=str(rrule.rrule(rrule.DAILY,
                                                                                   dtstart=datetime(1999, 1, 1),
                                                                                   count=3)),
                                                          status=models.Status.COMPLETED)
        low_level_api.create_object(self.paths[models.Term], models.Term, self.eleven_subtask, datetime(1999, 1, 1),
                                    term_type=models.TermType.DEADLINE, active=False)
        low_level_api.create_object(self.paths[models.Term], models.Term, self.eleven, datetime(2000, 1, 1),
                                    term_type=models.TermType.DEADLINE, active=True)


    def test_periodical_parent_with_completed_subtask_last_repeat(self):
        task_id = self.ten
        subtask_id = self.ten_subtask
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.COMPLETED, const.PERIODICAL: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.STATUS: const.COMPLETED, const.PERIODICAL: True}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                              const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))

    def test_periodical_parent_with_completed_subtask(self):
        task_id = self.eleven
        subtask_id = self.eleven_subtask
        self.storage.complete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.PENDING,
                                             const.DEADLINE: '2000-01-02T00:00:00',
                                             const.PERIODICAL: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.STATUS: const.PENDING,
                                             const.DEADLINE: '1999-01-02T00:00:00',
                                             const.PERIODICAL: True}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM: '2000-01-02T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM: '1999-01-02T00:00:00',
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))

class TestCopyTask(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.first = self.storage.create_task(self.user, 'task1')

        self.second = self.storage.create_task(self.user, 'task2', deadline=datetime(2000, 1, 1))

        self.third = self.storage.create_task(self.user, 'task3', deadline=datetime(2000, 1, 1))
        self.storage.set_reminder(self.third, relative_delta=relativedelta(days=1))

        self.four = self.storage.create_task(self.user, 'task4')
        self.four_subtask_a = self.storage.create_subtask(self.user, self.four, 'task4.a', deadline=datetime(666, 1, 1))
        self.four_subtask_b = self.storage.create_subtask(self.user, self.four, 'task4.b')

        self.five = self.storage.create_task(self.user, 'task5')
        self.five_related = self.storage.create_task(self.user, 'task5related')
        self.storage.attach_relation(self.user, self.five, self.five_related, models.RelationType.BLOCKING)

        self.storage.attach_tag(self.user, self.first, 'tag')


    def test_copy_task_without_deadline(self):
        task_id = self.first
        copy_id = self.storage.copy_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.USER_ID: self.user, const.TITLE: 'task1', const.DEADLINE: None}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: copy_id,
                                             const.USER_ID: self.user, const.TITLE: 'task1', const.DEADLINE: None}))

        data = load_from(self.paths, models.Permission)
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: self.user, const.ACCESS_LEVEL: const.CREATOR}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: copy_id,
                                             const.USER_ID: self.user, const.ACCESS_LEVEL: const.CREATOR}))

    def test_copy_task_with_deadline(self):
        task_id = self.second
        copy_id = self.storage.copy_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.USER_ID: self.user, const.TITLE: 'task2',
                                             const.DEADLINE: '2000-01-01T00:00:00'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: copy_id,
                                             const.USER_ID: self.user, const.TITLE: 'task2',
                                             const.DEADLINE: '2000-01-01T00:00:00'}))

        data = load_from(self.paths, models.Permission)
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: self.user, const.ACCESS_LEVEL: const.CREATOR}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: copy_id,
                                             const.USER_ID: self.user, const.ACCESS_LEVEL: const.CREATOR}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: copy_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True}))

    def test_copy_task_with_reminders(self):
        task_id = self.third
        copy_id = self.storage.copy_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.USER_ID: self.user, const.TITLE: 'task3',
                                             const.DEADLINE: '2000-01-01T00:00:00'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: copy_id,
                                             const.USER_ID: self.user, const.TITLE: 'task3',
                                             const.DEADLINE: '2000-01-01T00:00:00'}))

        data = load_from(self.paths, models.Permission)
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: task_id,
                                             const.USER_ID: self.user, const.ACCESS_LEVEL: const.CREATOR}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.TASK_ID: copy_id,
                                             const.USER_ID: self.user, const.ACCESS_LEVEL: const.CREATOR}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: copy_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-12-31T00:00:00', const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: copy_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-12-31T00:00:00', const.ACTIVE: True}))

    def test_copy_task_with_subtasks(self):
        task_id = self.four
        copy_id = self.storage.copy_task(task_id)

        data = load_from(self.paths, models.Task)

        data.pop(task_id)
        data.pop(self.four_subtask_a)
        data.pop(self.four_subtask_b)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: copy_id,
                                             const.USER_ID: self.user, const.TITLE: 'task4'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.PARENT_ID: copy_id,
                                             const.USER_ID: self.user, const.TITLE: 'task4.a'}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.PARENT_ID: copy_id,
                                             const.USER_ID: self.user, const.TITLE: 'task4.b'}))

        data = load_from(self.paths, models.Term)

        deadlined = None
        for d in data:
            d_dict = data[d]
            if d_dict['task_id'] == self.four_subtask_a:
                deadlined = d
        data.pop(deadlined)

        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '0666-01-01T00:00:00'}))

    def test_copy_task_with_relation(self):
        task_id = self.five
        copy_id = self.storage.copy_task(task_id)

        data = load_from(self.paths, models.Relation)
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: task_id,
                                             const.TASK_TO: self.five_related, const.USER_ID: self.user}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: copy_id,
                                             const.TASK_TO: self.five_related, const.USER_ID: self.user}))

    def test_copy_task_with_tag(self):
        task_id = self.first
        copy_id = self.storage.copy_task(task_id)

        data = load_from(self.paths, models.TaskTagRelation)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id,
                                             const.TAG: 'tag', const.USER_ID: self.user}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: copy_id,
                                             const.TAG: 'tag', const.USER_ID: self.user}))


class TestArchiveRestoreDeleteTask(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.task_id = self.storage.create_task(self.user, 'task', deadline=datetime(2000, 1, 1))
        self.subtask_a = self.storage.create_subtask(self.user, self.task_id, 'subtask_a')
        self.subtask_b = self.storage.create_subtask(self.user, self.task_id, 'subtask_b',
                                                     deadline=datetime(1999, 12, 31))
        self.related_task = self.storage.create_task(self.user, 'related_task')

        self.storage.set_reminder(self.task_id, relativedelta(days=1))
        self.storage.set_reminder(self.task_id, relativedelta(months=1))
        self.storage.set_reminder(self.subtask_b, relativedelta(days=1))

        self.storage.attach_tag(self.user, self.task_id, 'tag1')
        self.storage.attach_tag(self.user, self.subtask_a, 'tag2')
        self.storage.attach_tag(self.user, self.related_task, 'tag1')

        self.storage.attach_relation(self.user, self.subtask_a, self.related_task, models.RelationType.BLOCKING)
        self.storage.attach_relation(self.user, self.subtask_b, self.subtask_a, models.RelationType.SIMULTANEOUS)

    def test_archive_parent_task(self):
        task_id = self.task_id
        a_id = self.subtask_a
        b_id = self.subtask_b

        self.storage.archive_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.ARCHIVED: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: a_id,
                                             const.ARCHIVED: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: b_id,
                                             const.ARCHIVED: True}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM, const.TASK_ID: task_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM, const.TASK_ID: a_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM, const.TASK_ID: b_id,
                                              const.ARCHIVED: False}))

        data = load_from(self.paths, models.Relation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: task_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: a_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: b_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: task_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: a_id,
                                              const.ARCHIVED: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: b_id,
                                              const.ARCHIVED: False}))

    def test_restore_task(self):
        task_id = self.task_id
        a_id = self.subtask_a
        b_id = self.subtask_b

        self.storage.archive_task(task_id)
        self.storage.restore_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: a_id,
                                             const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: b_id,
                                             const.ARCHIVED: False}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '2000-01-01T00:00:00', const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-12-31T00:00:00', const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-12-01T00:00:00', const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: b_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE,
                                             const.TERM: '1999-12-31T00:00:00', const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: b_id,
                                             const.TERM_TYPE: const.REMINDER_TERM_TYPE,
                                             const.TERM: '1999-12-30T00:00:00', const.ARCHIVED: False}))

        data = load_from(self.paths, models.Relation)
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: a_id,
                                             const.TASK_TO: self.related_task, const.USER_ID: self.user,
                                             const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: b_id,
                                             const.TASK_TO: a_id, const.USER_ID: self.user, const.ARCHIVED: False}))

    def test_delete_parent_task(self):
        task_id = self.task_id
        a_id = self.subtask_a
        b_id = self.subtask_b

        self.storage.delete_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: a_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: b_id}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM, const.TASK_ID: task_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM, const.TASK_ID: a_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM, const.TASK_ID: b_id}))

        data = load_from(self.paths, models.TaskTagRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: task_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: a_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS, const.TASK_ID: b_id}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_TAG_RELATION_CLASS,
                                             const.TASK_ID: self.related_task,
                                             const.TAG: 'tag1', const.USER_ID: self.user}))

        data = load_from(self.paths, models.Relation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: task_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: a_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_FROM: b_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: task_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: a_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.RELATION_CLASS, const.TASK_TO: b_id}))

        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: task_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: a_id}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.TASK_ID: b_id}))


class TestHandleOverdueNonperiodicalTerms(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.user2 = self.storage.create_object(models.User, 'user2', None, None)
        self.creator_box2 = self.storage.create_object(models.Box, self.user2, 'MyBox')
        self.shared_box2 = self.storage.create_object(models.Box, self.user2, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user2, creator_box=self.creator_box2,
                                          shared_box=self.shared_box2)

        self.user3 = self.storage.create_object(models.User, 'user3', None, None)
        self.creator_box3 = self.storage.create_object(models.Box, self.user3, 'MyBox')
        self.shared_box3 = self.storage.create_object(models.Box, self.user3, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user3, creator_box=self.creator_box3,
                                          shared_box=self.shared_box3)

        self.user_login = 'beastie'
        self.date = datetime(2018, 1, 1)
        self.tasks = {
            #'task1': self.storage.create_task("user1", "task1", deadline=datetime(2015, 12, 14)),
            'task2': self.storage.create_task(self.user, "task2", deadline=datetime(2017, 10, 5)),
            'task3': self.storage.create_task(self.user, "task3", deadline=datetime(2019, 10, 5)),
            'task4': self.storage.create_task(self.user2, "task4", deadline=datetime(2015, 10, 14)),
            'task5': self.storage.create_task(self.user2, "task5", deadline=datetime(2019, 10, 14)),
            'task6': self.storage.create_task(self.user3, "task6", deadline=datetime(2010, 1, 14)),
            'task8': self.storage.create_task(self.user3, "task8", deadline=datetime(2019, 3, 14))
        }

        self.tasks['task7'] = self.storage.create_subtask(self.user3, self.tasks['task3'], "task7", deadline=datetime(2019, 2, 14))
        self.tasks['task1'] = self.storage.create_subtask(self.user, self.tasks['task8'], "task1", deadline=datetime(2015, 12, 14))

        self.storage.set_reminder(self.tasks['task2'], relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.tasks['task2'], relative_delta=relativedelta(years=1))
        self.storage.set_reminder(self.tasks['task7'], relative_delta=relativedelta(days=2))
        self.storage.set_reminder(self.tasks['task8'], relative_delta=relativedelta(years=2))

    def test_peek_overdue_terms(self):
        overdue_terms = self.storage.peek_overdue_terms(self.date)

        self.assertEqual(len(overdue_terms), 7)

        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task2'],
                                                      term_type=models.TermType.REMINDER,
                                                      relative_delta=relativedelta(days=1)))
        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task2'],
                                                      term_type=models.TermType.REMINDER,
                                                      relative_delta=relativedelta(years=1)))
        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task8'],
                                                      term_type=models.TermType.REMINDER,
                                                      relative_delta=relativedelta(years=2)))
        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task1'],
                                                      term_type=models.TermType.DEADLINE))
        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task2'],
                                                      term_type=models.TermType.DEADLINE))
        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task4'],
                                                      term_type=models.TermType.DEADLINE))
        self.assertTrue(contains_corresponding_object(overdue_terms, task_id=self.tasks['task6'],
                                                      term_type=models.TermType.DEADLINE))

    def test_task1(self):
        task_id = self.tasks['task1']
        self.storage.handle_overdue_terms(self.date)

        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.OVERDUE)

        deadline = self.storage.get_deadline_term_by_task_id(task_id)
        self.assertTrue(deadline.active)

        active_reminders = self.storage.get_active_reminders(task_id)
        self.assertDictEqual(active_reminders, {})

        terms = self.storage.get_terms_by_task_id(task_id)
        terms.pop(deadline.instance_id)
        self.assertFalse(terms)

    def test_task2(self):
        task_id = self.tasks['task2']
        self.storage.handle_overdue_terms(self.date)

        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.OVERDUE)

        deadline = self.storage.get_deadline_term_by_task_id(task_id)
        self.assertTrue(deadline.active)

        active_reminders = self.storage.get_active_reminders(task_id)
        self.assertDictEqual(active_reminders, {})

        terms = self.storage.get_terms_by_task_id(task_id)
        terms.pop(deadline.instance_id)
        self.assertEqual(len(terms), 2)

        for term_id in terms:
            self.assertEqual(terms[term_id].term_type, models.TermType.REMINDER)
            self.assertFalse(terms[term_id].active)

    def test_task5(self):
        task_id = self.tasks['task5']
        self.storage.handle_overdue_terms(self.date)

        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.PENDING)

        deadline = self.storage.get_deadline_term_by_task_id(task_id)
        self.assertTrue(deadline.active)

        active_reminders = self.storage.get_active_reminders(task_id)
        self.assertDictEqual(active_reminders, {})

        terms = self.storage.get_terms_by_task_id(task_id)
        terms.pop(deadline.instance_id)
        self.assertFalse(terms)

    def test_task7(self):
        task_id = self.tasks['task7']
        self.storage.handle_overdue_terms(self.date)

        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.PENDING)

        deadline = self.storage.get_deadline_term_by_task_id(task_id)
        self.assertTrue(deadline.active)

        active_reminders = self.storage.get_active_reminders(task_id)
        self.assertEqual(len(active_reminders), 1)
        self.assertTrue(contains_corresponding_object(active_reminders, task_id=self.tasks['task7'],
                                                      term_type=models.TermType.REMINDER, active=True,
                                                      relative_delta=relativedelta(days=2)))

    def test_task8(self):
        task_id = self.tasks['task8']
        self.storage.handle_overdue_terms(self.date)

        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.PENDING)

        deadline = self.storage.get_deadline_term_by_task_id(task_id)
        self.assertTrue(deadline.active)

        active_reminders = self.storage.get_active_reminders(task_id)
        self.assertDictEqual(active_reminders, {})

        terms = self.storage.get_terms_by_task_id(task_id)
        terms.pop(deadline.instance_id)
        self.assertEqual(len(terms), 1)

        for term_id in terms:
            self.assertEqual(terms[term_id].term_type, models.TermType.REMINDER)
            self.assertFalse(terms[term_id].active)


class TestHandleOverduePeriodicalTerms(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)
        self.user_login = 'beastie'

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.user2 = self.storage.create_object(models.User, 'user2', None, None)
        self.creator_box2 = self.storage.create_object(models.Box, self.user2, 'MyBox')
        self.shared_box2 = self.storage.create_object(models.Box, self.user2, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user2,
                                          creator_box=self.creator_box2,
                                          shared_box=self.shared_box2)

        self.user3 = self.storage.create_object(models.User, 'user3', None, None)
        self.creator_box3 = self.storage.create_object(models.Box, self.user3, 'MyBox')
        self.shared_box3 = self.storage.create_object(models.Box, self.user3, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user3,
                                          creator_box=self.creator_box3,
                                          shared_box=self.shared_box3)

        self.date = datetime(2018, 1, 1)
        self.tasks = {
            'task1': self.storage.create_task(self.user, "task1", deadline=datetime(2018, 9, 14)),
            'task2': self.storage.create_task(self.user, "task2", deadline=datetime(2017, 10, 5)),
            'task3': self.storage.create_task(self.user, "task3", deadline=datetime(2019, 10, 5)),
            'task4': self.storage.create_task(self.user2, "task4", deadline=datetime(2020, 10, 14)),
            #'task5': self.storage.add_task("user2", "task5", deadline=datetime(2019, 10, 14)),
            #'task6': self.storage.add_task("user3", "task6", deadline=datetime(2010, 1, 14)),
            #'task7': self.storage.add_task("user3", "task7", deadline=datetime(2019, 2, 14)),
            #'task8': self.storage.add_task("user3", "task8", deadline=datetime(2019, 3, 14))
        }

        self.storage.set_reminder(self.tasks['task3'], relative_delta=relativedelta(days=1))
        self.storage.set_reminder(self.tasks['task4'], relative_delta=relativedelta(years=1))
        self.storage.set_reminder(self.tasks['task4'], relative_delta=relativedelta(days=2))

        self.storage.set_periodicity(self.tasks['task1'], rrule.DAILY, count=5)
        self.storage.set_periodicity(self.tasks['task3'], rrule.YEARLY, count=4)
        self.storage.set_periodicity(self.tasks['task4'], rrule.MONTHLY, count=4)

    def test_task1(self):
        task_id = self.tasks['task1']

        rule = rrule.rrule(rrule.DAILY, count=5, dtstart=datetime(2018, 9, 14))
        following_deadlines = list(rule)

        count = 5

        while True:

            task = self.storage.get_task_by_id(task_id)
            self.assertEqual(task.deadline, following_deadlines[0])

            self.assertEqual(task.status, models.Status.PENDING)



            self.storage.handle_overdue_terms(task.deadline + relativedelta(hours=1))

            task = self.storage.get_task_by_id(task_id)
            self.assertEqual(task.status, models.Status.OVERDUE)

            self.storage.complete_task(task_id)

            count -= 1
            following_deadlines.pop(0)

            if len(following_deadlines) == 0:
                break

            task = self.storage.get_task_by_id(task_id)
            self.assertEqual(task.status, models.Status.PENDING)


        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.COMPLETED)

        terms = self.storage.get_terms_by_task_id(task_id)
        self.assertEqual(len(terms), 1)

    def test_task3(self):
        task_id = self.tasks['task3']

        rule = rrule.rrule(rrule.YEARLY, count=4, dtstart=datetime(2019, 10, 5))
        following_deadlines = list(rule)

        reminder_rule = rrule.rrule(rrule.YEARLY, count=4, dtstart=datetime(2019, 10, 4))
        following_reminders = list(reminder_rule)

        count = 4

        while True:
            print()
            task = self.storage.get_task_by_id(task_id)
            self.assertEqual(task.deadline, following_deadlines[0])

            self.assertEqual(task.status, models.Status.PENDING)

            self.storage.handle_overdue_terms(task.deadline + relativedelta(hours=1))

            task = self.storage.get_task_by_id(task_id)
            self.assertEqual(task.status, models.Status.OVERDUE)

            with open(self.paths[models.Term], mode='r') as file:
                data = json.load(file)

           # for d in data:
           #     if data[d]['task_id'] == task_id:

             #          print(data[d])

            self.assertTrue(contains_corresponding_dict_obj(data, task_id=task_id, term=datetime.isoformat(following_deadlines[0]),
                                                            term_type='DEADLINE', active=True))
            self.assertTrue(contains_corresponding_dict_obj(data, task_id=task_id, term=datetime.isoformat(following_reminders[0]),
                                                            term_type='REMINDER', active=False))


            self.storage.complete_task(task_id)

            count -= 1
            following_deadlines.pop(0)
            following_reminders.pop(0)

            if len(following_deadlines) == 0:
                break

            task = self.storage.get_task_by_id(task_id)
            self.assertEqual(task.status, models.Status.PENDING)

        task = self.storage.get_task_by_id(task_id)
        self.assertEqual(task.status, models.Status.COMPLETED)

        terms = self.storage.get_terms_by_task_id(task_id)
        self.assertEqual(len(terms), 2)


class TestBoxes(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)


        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.first = self.storage.create_task(self.user, 'task')
        self.first_subtask = self.storage.create_subtask(self.user, self.first, 'task')

        self.custom_box = self.storage.create_box(self.user, "Custom")
        self.second = self.storage.create_task(self.user, 'gggg', deadline=datetime(2001, 1, 1), box_id=self.custom_box)
        #print(self.storage.get_task_by_id(self.second))
        self.second_a = self.storage.create_subtask(self.user, self.second, 'taskA', deadline=datetime(2000, 1, 1))
        self.second_b = self.storage.create_subtask(self.user, self.second, 'taskB', deadline=datetime(1999, 1, 1))

        self.third = self.storage.create_task(self.user, 'task1')
        #self.first = self.storage.create_task()

    def test_create_box(self):
        box_id = self.storage.create_box(self.user, 'AAAAAAAAAA')

        data = load_from(self.paths, models.Box)
        self.assertTrue(contains_dict(data, {const.CLASS: const.BOX_CLASS, const.USER_ID: self.user,
                                             const.TITLE: 'AAAAAAAAAA', const.INSTANCE_ID: box_id}))

    def test_create_task_custom_box(self):
        task_id = self.storage.create_task(self.user, 'gggg', box_id=self.custom_box)
        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.USER_ID: self.user,
                                             const.TITLE: 'gggg', const.INSTANCE_ID: task_id}))
        data = load_from(self.paths, models.Permission)
        self.assertTrue(contains_dict(data, {const.CLASS: const.PERMISSION_CLASS, const.USER_ID: self.user,
                                             const.TASK_ID: task_id, const.ACCESS_LEVEL: const.CREATOR}))
        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.USER_ID: self.user,
                                             const.TASK_ID: task_id, const.BOX_ID: self.custom_box}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.USER_ID: self.user,
                                             const.TASK_ID: task_id, const.BOX_ID: self.creator_box}))

    def test_create_box_with_the_same_name(self):
        with self.assertRaises(errors.ObjectExistsError):
            self.storage.create_box(self.user, 'MyBox')

    def test_archive_box(self):
        self.storage.archive_box(self.custom_box)
        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second,
                                             const.ARCHIVED: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second_a,
                                             const.ARCHIVED: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second_b,
                                             const.ARCHIVED: True}))

        data = load_from(self.paths, models.Box)
        self.assertTrue(contains_dict(data, {const.CLASS: const.BOX_CLASS, const.INSTANCE_ID: self.custom_box,
                                             const.ARCHIVED: True}))

        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.custom_box,
                                              const.ARCHIVED: False}))

        self.storage.restore_box(self.custom_box)
        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second,
                                             const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second_a,
                                             const.ARCHIVED: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second_b,
                                             const.ARCHIVED: False}))

        data = load_from(self.paths, models.Box)
        self.assertTrue(contains_dict(data, {const.CLASS: const.BOX_CLASS, const.INSTANCE_ID: self.custom_box,
                                             const.ARCHIVED: False}))

        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.custom_box,
                                              const.ARCHIVED: True}))

    def test_move_task_to_box(self):
        self.storage.move_to_box(self.first, self.custom_box)
        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.creator_box,
                                              const.TASK_ID: self.first, const.USER_ID: self.user}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.custom_box,
                                             const.TASK_ID: self.first, const.USER_ID: self.user}))

    def test_move_subtask_to_box(self):
        self.storage.move_to_box(self.first_subtask, self.custom_box)
        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.custom_box,
                                             const.TASK_ID: self.first_subtask, const.USER_ID: self.user}))
        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.first_subtask,
                                             const.PARENT_ID: None}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.first_subtask,
                                             const.PARENT_ID: self.first}))

    def test_delete_box(self):
        self.storage.delete_box(self.custom_box)
        data = load_from(self.paths, models.Task)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second_a}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.second_b}))

        data = load_from(self.paths, models.Term)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: self.second}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: self.second_a}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: self.second_b}))

        data = load_from(self.paths, models.Box)
        self.assertFalse(contains_dict(data, {const.CLASS: const.BOX_CLASS, const.INSTANCE_ID: self.custom_box}))

        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.custom_box}))


    def test_move_to_another_parent(self):
        self.storage.move_to_parent(self.first_subtask, self.second)
        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.first_subtask,
                                             const.PARENT_ID: self.second}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.first_subtask,
                                              const.PARENT_ID: self.first}))

        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS, const.BOX_ID: self.custom_box,
                                              const.TASK_ID: self.first_subtask}))

    def test_move_to_parent(self):
        self.storage.move_to_parent(self.third, self.second)
        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.third,
                                             const.PARENT_ID: self.second}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: self.third,
                                              const.PARENT_ID: None}))

        data = load_from(self.paths, models.TaskBoxRelation)
        self.assertFalse(contains_dict(data, {const.CLASS: const.TASK_BOX_RELATION_CLASS,
                                              const.TASK_ID: self.third, const.USER_ID: self.user}))


class TestResumeTask(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.simple = self.storage.create_task(self.user, 'simple')
        self.storage.complete_task(self.simple)

        self.task = self.storage.create_task(self.user, 'task', deadline=datetime(2000, 1, 1))
        self.storage.set_reminder(self.task, relativedelta(days=1))
        self.storage.complete_task(self.task)

        self.big_task = self.storage.create_task(self.user, 'big', deadline=(datetime(2000, 1, 1)))
        self.storage.set_reminder(self.big_task, relativedelta(days=1))
        self.subtask = self.storage.create_subtask(self.user, self.big_task, 'subtask')

        self.storage.complete_task(self.subtask)
        self.storage.complete_task(self.big_task)


    def test_resume_simple(self):
        task_id = self.simple
        self.storage.resume_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.PENDING}))

    def test_resume_no_subtask(self):
        task_id = self.task
        self.storage.resume_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.PENDING}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.ACTIVE: True, const.TERM_TYPE: const.REMINDER_TERM_TYPE}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.ACTIVE: False}))

    def test_resume_parent(self):
        task_id = self.big_task
        subtask_id = self.subtask
        self.storage.resume_task(task_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.PENDING}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.STATUS: const.COMPLETED}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.ACTIVE: True, const.TERM_TYPE: const.REMINDER_TERM_TYPE}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.ACTIVE: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: False}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                              const.ACTIVE: True}))

    def test_resume_subtask(self):
        task_id = self.big_task
        subtask_id = self.subtask
        self.storage.resume_task(subtask_id)

        data = load_from(self.paths, models.Task)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: task_id,
                                             const.STATUS: const.PENDING}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TASK_CLASS, const.INSTANCE_ID: subtask_id,
                                             const.STATUS: const.PENDING}))

        data = load_from(self.paths, models.Term)
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                             const.ACTIVE: True, const.TERM_TYPE: const.REMINDER_TERM_TYPE}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: task_id,
                                              const.ACTIVE: False}))
        self.assertTrue(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                             const.TERM_TYPE: const.DEADLINE_TERM_TYPE, const.ACTIVE: True}))
        self.assertFalse(contains_dict(data, {const.CLASS: const.TERM_CLASS, const.TASK_ID: subtask_id,
                                              const.ACTIVE: False}))


class TestGetOverdueTasks(unittest.TestCase):
    def setUp(self):
        settings.remove_data_folder()
        self.paths = settings.create_data_files()
        self.storage = StorageAPI(self.paths)

        self.user = self.storage.create_object(models.User, 'user', None, None)
        self.creator_box = self.storage.create_object(models.Box, self.user, 'MyBox')
        self.shared_box = self.storage.create_object(models.Box, self.user, 'Shared with me')
        low_level_api.update_object_by_id(self.paths[models.User], models.User, self.user, creator_box=self.creator_box,
                                          shared_box=self.shared_box)

        self.first = self.storage.create_task(self.user, '1', deadline=datetime(2000, 1, 1))
        self.storage.set_reminder(self.first, relative_delta=relativedelta(days=1))

        self.second = self.storage.create_task(self.user, '2', deadline=datetime(2005, 1, 1))
        self.second_subtask = self.storage.create_subtask(self.user, self.second, '22', deadline=datetime(2005, 1, 1))

        self.third = self.storage.create_task(self.user, '3', deadline=datetime(2019, 1, 1))
        self.storage.set_reminder(self.third, relative_delta=relativedelta(years=10))

    def test_get_overdue_tasks(self):
        self.storage.handle_overdue_terms(datetime(2018, 1, 1))
        tasks = self.storage.get_user_overdue_tasks(self.user)

        self.assertEqual(len(tasks), 3)
        self.assertIn(self.first, tasks)
        self.assertIn(self.second, tasks)
        self.assertIn(self.second_subtask, tasks)

    def test_get_notifications(self):
        self.storage.handle_overdue_terms(datetime(2018, 1, 1))

        notifications = self.storage.get_fresh_user_notifications(self.user)

        self.assertEqual(len(notifications), 5)

        self.assertTrue(contains_corresponding_object(notifications, task_id=self.first, user_id=self.user,
                                                      notification_type=models.NotificationType.DEADLINE))
        self.assertTrue(contains_corresponding_object(notifications, task_id=self.first, user_id=self.user,
                                                      notification_type=models.NotificationType.REMINDER))
        self.assertTrue(contains_corresponding_object(notifications, task_id=self.second,user_id=self.user,
                                                      notification_type=models.NotificationType.DEADLINE))
        self.assertTrue(contains_corresponding_object(notifications, task_id=self.second_subtask, user_id=self.user,
                                                      notification_type=models.NotificationType.DEADLINE))
        self.assertTrue(contains_corresponding_object(notifications, task_id=self.third, user_id=self.user,
                                                      notification_type=models.NotificationType.REMINDER))
