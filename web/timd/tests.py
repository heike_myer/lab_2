from django.test import TestCase

import django.core.exceptions as exceptions
import timd.errors as errors
from django.contrib.auth.models import User
import timd.models as models
import timd.lib_api as lib_api
import timd.db_api as db_api
# Create your tests here.

from django.utils.text import slugify


from datetime import datetime, timedelta
import dateutil.rrule as rrule

import pytz


def get_lib_api():
    storage_api = db_api.DatabaseAPI()
    return lib_api.LibraryAPI(storage_api)


def get_custom_user(username='test_user'):
    #user = User.objects.get_by_natural_key(username)
    return models.CustomUser.objects.get(user__username=username)


def default_slug(model):
    return slugify(model.title + '-' + str(model.id))


class TestStorageAPI(TestCase):
    def setUp(self):
        self.api = get_lib_api()

        user = User.objects.create_user('test_user', 'test_user@gmail.com', 'test')
        custom_user = models.CustomUser.objects.create(user=user)
        custom_user.created_box = self.api.create_box(custom_user, 'My Box')
        custom_user.shared_box = self.api.create_box(custom_user, 'Shared with me')
        custom_user.save()

        user = User.objects.create_user('test_user_2', 'test_user_2@gmail.com', 'test')
        custom_user = models.CustomUser.objects.create(user=user)
        custom_user.created_box = self.api.create_box(custom_user, 'My Box')
        custom_user.shared_box = self.api.create_box(custom_user, 'Shared with me')
        custom_user.save()

    @property
    def user(self):
        return models.CustomUser.objects.get(user__username='test_user')

    @property
    def second_user(self):
        return models.CustomUser.objects.get(user__username='test_user_2')

    @property
    def created_box(self):
        return models.Box.objects.get(user=self.user, title='My Box')

    @property
    def shared_box(self):
        return models.Box.objects.get(user=self.user, title='Shared with me')

    def test_create_box(self):
        box = self.api.create_box(self.user, 'title')
        self.assertTrue(models.Box.objects.filter(id=box.id, user=self.user, title='title').exists())
        self.assertTrue(self.user.boxes.filter(id=box.id).exists())

    def test_create_task(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        self.assertTrue(self.created_box.tasks.filter(id=task.id).exists())
        self.assertTrue(task.box_set.filter(id=self.created_box.id).exists())
        self.assertTrue(models.Permission.objects.filter(task=task, user=self.user,
                                                         access_level=models.AccessLevel.CREATOR.value).exists())
        self.assertTrue(task.users.filter(id=self.user.id).exists())
        self.assertTrue(self.user.tasks.filter(id=task.id).exists())

    def test_create_subtask(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        subtask = self.api.create_subtask(self.user, task.slug, 'subtask')
        task.refresh_from_db()
        self.assertFalse(self.created_box.tasks.filter(id=subtask.id).exists())
        self.assertTrue(models.Permission.objects.filter(task=subtask, user=self.user,
                                                         access_level=models.AccessLevel.CREATOR.value).exists())
        self.assertTrue(subtask.users.filter(id=self.user.id).exists())
        self.assertTrue(self.user.tasks.filter(id=subtask.id).exists())
        self.assertEqual(subtask.parent, task)
        self.assertIn(subtask, task.subtasks.all())

    def test_set_deadline(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')

        self.api.set_deadline(task, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        self.assertEqual(task.deadline, datetime(2000, 1, 1, tzinfo=pytz.UTC))

        subtask = self.api.create_subtask(self.user, task.slug, 'subtask')
        self.assertEqual(subtask.deadline, datetime(2000, 1, 1, tzinfo=pytz.UTC))

        task = self.api.create_task(self.user, self.created_box.slug, 'task2')
        subtask = self.api.create_subtask(self.user, task.slug, 'subtask2')

        self.api.set_deadline(task, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        subtask.refresh_from_db()
        self.assertEqual(task.deadline, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        self.assertEqual(subtask.deadline, datetime(2000, 1, 1, tzinfo=pytz.UTC))

        task = self.api.create_task(self.user, self.created_box.slug, 'task3')
        subtask = self.api.create_subtask(self.user, task.slug, 'subtask3')
        subtask_3a = self.api.create_subtask(self.user, task.slug, 'subtask3a')

        self.api.set_deadline(subtask, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        self.api.set_deadline(subtask_3a, datetime(1998, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        subtask.refresh_from_db()
        subtask_3a.refresh_from_db()
        self.assertIsNone(task.deadline)
        self.assertEqual(subtask.deadline, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        self.assertEqual(subtask_3a.deadline, datetime(1998, 1, 1, tzinfo=pytz.UTC))

        self.api.set_deadline(task, datetime(1999, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        subtask.refresh_from_db()
        subtask_3a.refresh_from_db()
        self.assertEqual(task.deadline, datetime(1999, 1, 1, tzinfo=pytz.UTC))
        self.assertEqual(subtask.deadline, datetime(1999, 1, 1, tzinfo=pytz.UTC))
        self.assertEqual(subtask_3a.deadline, datetime(1998, 1, 1, tzinfo=pytz.UTC))

    def test_set_reminder(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        self.api.set_deadline(task, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        reminder = self.api.set_reminder(task, timedelta(days=2))
        task.refresh_from_db()

        self.assertIn(reminder, task.reminders.all())
        self.assertEqual(reminder.term, datetime(1999, 12, 30, tzinfo=pytz.UTC))
        self.assertEqual(reminder.timedelta, timedelta(days=2))
        self.assertEqual(reminder.task, task)

        with self.assertRaises(errors.ObjectExistsError):
            self.api.set_reminder(task, timedelta(days=2))

        reminder_2 = self.api.set_reminder(task, timedelta(hours=2))
        task.refresh_from_db()
        self.assertIn(reminder, task.reminders.all())
        self.assertIn(reminder_2, task.reminders.all())
        self.assertEqual(reminder_2.task, task)

        reminder_3 = self.api.set_reminder(task, timedelta(hours=10))
        task.refresh_from_db()

        self.api.unset_reminder(task, timedelta(days=2))
        task.refresh_from_db()
        self.assertIn(reminder_2, task.reminders.all())
        self.assertIn(reminder_3, task.reminders.all())
        self.assertFalse(task.reminders.filter(timedelta=timedelta(days=2)).exists())

        self.api.unset_all_reminders(task)
        task.refresh_from_db()
        self.assertEqual(len(task.reminders.all()), 0)
        self.assertFalse(models.Reminder.objects.filter(task=task).exists())

    def test_update_deadline_simple(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        self.api.set_deadline(task, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        self.api.update_deadline(task, datetime(2024, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        self.assertEqual(task.deadline, datetime(2024, 1, 1, tzinfo=pytz.UTC))

    def test_update_deadline_with_reminders(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        self.api.set_deadline(task, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        reminder_one = self.api.set_reminder(task, timedelta(hours=2))
        reminder_two = self.api.set_reminder(task, timedelta(days=2))

        task.refresh_from_db()

        self.api.update_deadline(task, datetime(2018, 10, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()
        reminder_one.refresh_from_db()
        reminder_two.refresh_from_db()

        self.assertEqual(task.deadline, datetime(2018, 10, 1, tzinfo=pytz.UTC))
        self.assertEqual(reminder_one.term, datetime(2018, 9, 30, 22, tzinfo=pytz.UTC))
        self.assertEqual(reminder_two.term, datetime(2018, 9, 29, tzinfo=pytz.UTC))

    def test_set_periodicicity(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')

        self.api.set_deadline(task, datetime(2000, 1, 1, tzinfo=pytz.UTC))
        task.refresh_from_db()

        subtask = self.api.create_subtask(self.user, task.slug, 'subtask')


        self.api.set_periodicity(task, rrule.DAILY, count=5)
        task.refresh_from_db()
        self.assertTrue(task.periodical)
        self.assertEqual('RRULE:FREQ=DAILY;COUNT=5', task.rule_str)

        subtask.refresh_from_db()
        self.assertTrue(subtask.periodical)
        self.assertEqual('RRULE:FREQ=DAILY;COUNT=5', subtask.rule_str)

        self.api.unset_periodicity(task)

    def test_complete_task(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        self.api.complete_task(task)
        task.refresh_from_db()

        self.assertEqual(task.status, models.Status.COMPLETED.value)
        self.assertFalse(task.reminders.filter(active=True).exists())


    def test_attach_tag(self):
        task = self.api.create_task(self.user, self.created_box.slug, 'task')
        self.api.attach_tag(self.user, task, 'mytag')
        task.refresh_from_db()

        self.assertTrue(self.user.tagged_tasks.filter(id=task.id).exists())
        self.assertTrue(task.tags.filter(task=task, user=self.user, tag_text='mytag').exists())

        self.api.attach_tag(self.user, task, 'mytag2')
        task.refresh_from_db()
        self.assertTrue(task.tags.filter(user=self.user, tag_text='mytag').exists())
        self.assertTrue(task.tags.filter(user=self.user, tag_text='mytag2').exists())

        self.api.detach_tag(self.user, task, 'mytag')
        task.refresh_from_db()
        self.assertFalse(models.Tag.objects.filter(task=task, user=self.user, tag_text='mytag').exists())
        self.assertFalse(task.tags.filter(user=self.user, tag_text='mytag').exists())
        self.assertTrue(task.tags.filter(user=self.user, tag_text='mytag2').exists())

    def test_relation(self):
        one = self.api.create_task(self.user, self.created_box.slug, 'one')
        two = self.api.create_task(self.user, self.created_box.slug, 'two')

        self.api.attach_relation(self.user, one, two, models.RelationType.BLOCKING.value)

        one.refresh_from_db()
        two.refresh_from_db()

        self.assertIn(two, one.relating_tasks.all())
        self.assertIn(one, two.related_tasks.all())

        self.assertTrue(models.Relation.objects.filter(from_task=two, user=self.user, to_task=one).exists())

        self.api.detach_relation(self.user, one, two)
        self.assertFalse(models.Relation.objects.filter(from_task=two, user=self.user, to_task=one).exists())




