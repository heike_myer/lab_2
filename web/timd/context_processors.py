import timd.models as models


def constants(request):
    return {
        'PENDING': models.Status.PENDING.value,
        'OVERDUE': models.Status.OVERDUE.value,
        'COMPLETED': models.Status.COMPLETED.value,
        'CREATOR': models.AccessLevel.CREATOR.value,
        'ADMIN': models.AccessLevel.ADMIN.value,
        'EXECUTOR': models.AccessLevel.EXECUTOR.value,
    }
