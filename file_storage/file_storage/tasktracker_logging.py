import functools
import logging
import logging.config


def log_decorator_creator(logger_name, level=logging.DEBUG):
    def log_decorator(func):

        @functools.wraps(func)
        def logging_wrapper(*args, **kwargs):
            logger = logging.getLogger(logger_name)
            extra = {'func': func.__name__, 'action': 'ENTER'}

            logger.log(level, 'Args: {}\tKwargs: {}'.format(args, kwargs), extra=extra)

            try:
                result = func(*args, **kwargs)
                extra['action'] = 'EXIT'
                logger.log(level, 'Result: {}'.format(result), extra=extra)
                return result

            except Exception as e:
                extra['action'] = 'EXCEPTION RAISED'
                logger.log(logging.WARNING, 'Exception: {}'.format(type(e)), extra=extra)
                raise

        return logging_wrapper

    return log_decorator


def configure_default_loggers():
    config_dict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '%(asctime)s - %(levelname)s - %(name)s\n%(action)s\t%(func)s\t%(message)s\n',
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'handlers': {
            'console': {
                'level': 'ERROR',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
                'formatter': 'default'
            }
        },
        'loggers': {
            'storage': {
                'level': 'ERROR',
                'handlers': ['console']
            },
            'storage.storage_api': {
                'propagate': True
            },
            'storage.low_level_api': {
                'propagate': True
            },
            'storage.json_extensions': {
                'propagate': True
            }
        }
    }

    logging.config.dictConfig(config_dict)
