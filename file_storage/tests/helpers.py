import os
import json
import shutil, errno


import file_storage.models as models


filename = os.path.join(os.path.dirname(__file__), 'current.json')


def form_storage_paths():
    paths = {
        models.Task: filename,
        models.Term: filename,
        models.Permission: filename,
        models.User: filename,
        models.Relation: filename,
        models.Tag: filename,
        models.TaskTagRelation: filename,
        models.Box: filename,
        models.TaskBoxRelation: filename,
        models.Notification: filename
    }

    return paths



def load():
    with open(filename, mode='r') as file:
        data = json.load(file)

    return data


def object_fields_match(obj, **values):
    match = True
    for attr in values:
        if obj.__dict__[attr] != values[attr]:
            match = False
            break

    return match


def contains_corresponding_object(obj_dict, **kwargs):
    contains = False

    for obj_id in obj_dict:
        obj = obj_dict[obj_id]
        if object_fields_match(obj, **kwargs):
            contains = True
            break

    return contains


def dict_match(dict_obj, **values):
    match = True
    for attr in values:
        if dict_obj[attr] != values[attr]:
            match = False
            break

    return match


def contains_corresponding_dict_obj(obj_dict, **kwargs):
    contains = False

    for obj_id in obj_dict:
        obj = obj_dict[obj_id]
        if dict_match(obj, **kwargs):
            contains = True
            break

    return contains


def contains_dict(obj_dict, dict_obj):
    contains = False

    for obj_id in obj_dict:
        obj = obj_dict[obj_id]

        if obj['class'] == 'Term' and obj['term_type'] == 'REMINDER':
            #print(obj)
            obj.pop('relative_delta', None)
       # print(obj, '               ', dict_obj)
        if set(dict_obj.items()).issubset(set(obj.items())):
            contains = True
            break

    return contains


def contains_items(obj_dict, **kwargs):
    contains = True
    for attr in kwargs:
        if obj_dict[attr] != kwargs[attr]:
            contains = False
            break

    return contains


def get(obj_dict, **kwargs):
    for obj_id in obj_dict:
        obj = obj_dict[obj_id]
        if object_fields_match(obj, **kwargs):
            return obj


def form_data_folder_path(test_name='current_test_data'):
    return os.path.join(os.path.dirname(__file__), test_name)


def create_data_folder():
    data_folder = form_data_folder_path()
    os.mkdir(data_folder)
    return data_folder


def form_storage_paths_a(data_folder):
    paths = {
        models.Task: os.path.join(data_folder, "tasks.json"),
        models.Term: os.path.join(data_folder, "terms.json"),
        models.Permission: os.path.join(data_folder, "permissions.json"),
        models.User: os.path.join(data_folder, "users.json"),
        models.Relation: os.path.join(data_folder, "relations.json"),
        models.Tag: os.path.join(data_folder, "tags.json"),
        models.TaskTagRelation: os.path.join(data_folder, "task_tag_relations.json"),
        models.Box: os.path.join(data_folder, "boxes.json"),
        models.Notification: os.path.join(data_folder, "notifications.json"),
        models.TaskBoxRelation: os.path.join(data_folder, "task_box_relations.json"),
    }

    return paths


def load_from(paths, obj_type):
    with open(paths[obj_type], mode='r') as file:
        data = json.load(file)

    return data


def create_data_files():
    data_folder = create_data_folder()

    paths = form_storage_paths_a(data_folder)

    for path in paths.values():
        with open(path, mode="w") as file:
            file.write("{}")

    return paths


def remove_data_folder(path=None):
    if path is None:
        path = form_data_folder_path()
    if os.path.exists(path):
        shutil.rmtree(path)


def copy_data_folder(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            raise



