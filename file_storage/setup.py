import os.path as path
import setuptools


setuptools.setup(
    name='file_storage',
    version='1.0',
    package_dir={'': 'file_storage'},
    packages=setuptools.find_packages(where='file_storage'),
    #packages=['file_storage', 'file_storage.file_storage.models',
    #          'file_storage.file_storage.storage', 'file_storage.tests'],#setuptools.find_packages(),
    long_description=open(path.join(path.dirname(__file__), 'README.md')).read(),
    test_suite='tests',
    include_package_data=True,
    install_requires=[
        'python-dateutil',
        'pytest'
    ]
)
