"""This module defines all object types the library is dealing with."""
import enum


def copy(obj_type, source_obj, **kwargs):
    """Copy object, edit specified fields and return a copy.

    Args:
        obj_type: Type of the `source_obj` (obviously, type of a copy, too).
        source_obj: Object to be copied.
        **kwargs: Fields of the `source_obj` to be updated with specified values.

    Returns:
        Copy of th `source_obj` with some attributes replaced with new values.
    """
    dest_fields = source_obj.__dict__
    dest_fields.update(kwargs)
    dest_obj = obj_type(**dest_fields)
    return dest_obj


class User:
    def __init__(self, instance_id, username, creator_box, shared_box):
        self.instance_id = instance_id
        self.username = username
        self.creator_box = creator_box
        self.shared_box = shared_box

    def __str__(self):
        return self.instance_id + "\t" + self.username


class Status(enum.Enum):
    """Enum (extends enum.Enum) indicating the status of a task.

    Options:
       OVERDUE (0): Deadline came but task hasn't been completed yet.
       PENDING (1): Deadline hasn't come yet and task hasn't been completed.
       COMPLETED (2): Task was completed.
    """
    OVERDUE = 0
    PENDING = 1
    COMPLETED = 2


class Priority(enum.Enum):
    """Enum (extends enum.Enum) indicating the order of importance or urgency of a task.

    Options:
        HIGH (0): Extremely important.
        MEDIUM (1): Of medium importance.
        LOW (2): Not really important.
        NONE (3): Doesn't matter.
    """
    HIGH = 0
    MEDIUM = 1
    LOW = 2
    NONE = 3


class Task:
    """Task is an object created by a user in the hope to complete it one day.

    Attributes:
        instance_id (str): Object's unique identifier.
        creation_time (datetime.datetime): Time when a task was created.
        last_update_time (datetime.datetime): Last time one or more attributes where changed.
        user_id (str): Id of the user who created a task.
        title (str): Title of a task.
        status (models.Status): Whether task is `PENDING`, `COMPLETED` or `OVERDUE`.
        deadline (datetime.datetime): Time task is supposed to be completed by.
        priority (models.Priority): How important or urgent a task is.
        parent_id (str): Id of a parent task (if there is any).
        periodical (bool): Should task be repeated.
        rule_str (str): String containing a periodicity rule which can be parsed by dateutil.rrule.rrulestr
        archived (bool): Is task archived (means its terms are not being tracked)
    """

    def __init__(self, instance_id, creation_time, last_update_time, user_id, title,
                 status=Status.PENDING, deadline=None, priority=Priority.NONE, parent_id=None,
                 periodical=False, rule_str=None,
                 archived=False):

        self.instance_id = instance_id
        self.creation_time = creation_time
        self.last_update_time = last_update_time
        self.user_id = user_id
        self.title = title
        self.status = status
        self.deadline = deadline
        self.priority = priority
        self.parent_id = parent_id
        self.periodical = periodical
        self.rule_str = rule_str
        self.archived = archived

    def __str__(self):
        return "Task: id= {}\ttitle={}\prior={}\tstatus={}\tdeadline={}".format(self.instance_id, self.title, self.priority.name, self.status.name, self.deadline)


class TermType(enum.Enum):
    """Enum (extends enum.Enum) specifying the type of a Term object.

    Options:
        REMINDER (0): Consider Term object to be a reminder.
        DEADLINE (1): Consider Term object to be a deadline.
    """
    REMINDER = 0
    DEADLINE = 1


class Term:
    """Term is an object representing one of task's terms.

    Term can embody deadline or reminder. It stores the time, so these objects are tracked to see if term has come.

    Attributes:
        instance_id (str):  Object's unique identifier.
        task_id (str): Id of a task term belongs to.
        term (datetime.datetime): Time of a term.
        relative_delta (dateutil.relativedelta.relativedelta): How far in advance should user be reminded about a task
                                                               (for reminders only).
        term_type (models.TermType): Is term a deadline or just a reminder.
        active (bool): If term isn't active, it means either a task is completed (for deadlines) or it has already
                       been handled (for reminders), so term is not being tracked anymore.
        archived (bool): Is task term belongs to archived. If True, term is not being tracked.
    """

    def __init__(self, instance_id, task_id, term,
                 term_type=TermType.DEADLINE, active=True, relative_delta=None,
                 archived=False):

        self.instance_id = instance_id
        self.task_id = task_id

        self.term = term
        self.relative_delta = relative_delta
        self.term_type = term_type
        self.active = active

        self.archived = archived

    def __str__(self):
        return ("Term: id={} task_id={} term={} term_type={} is_active={} relative_delta={}"
                .format(self.instance_id, self.task_id, self.term, self.term_type, self.active, self.relative_delta))


class AccessLevel(enum.Enum):
    """Enum (extends enum.Enum) indicating an access level user has to a task.

    Options:
        CREATOR (0): User created the task and is free to edit task's title, terms and periodicity rule.
        ADMIN (1): User can give or withdraw permissions to a task from others.
        EXECUTOR (2): User can complete a task.
        WATCHER (3): User can see a task and gets notifications (all the above can do it, too).
    """
    CREATOR = 0
    ADMIN = 1
    EXECUTOR = 2
    WATCHER = 3


class Permission:
    """Class representing what rights a user has to a task (if has any).

    Attributes:
        instance_id (str):  Object's unique identifier.
        task_id (str): Id of a task permission is for.
        user_id (str): Id of a user permission belongs to.
        access_level (models.AccessLevel): Defines what operations user can do with a task. Level can be `CREATOR`,
                                           `ADMIN`, `EXECUTOR` and `WATCHER`.
    """

    def __init__(self, instance_id, user_id, task_id, access_level=AccessLevel.ADMIN):
        self.instance_id = instance_id
        self.user_id = user_id
        self.task_id = task_id
        self.access_level = access_level


class RelationType(enum.Enum):
    """Enum (extends enum.Enum) indicating a relation type.

    Options:
        BLOCKING (0): One of the tasks cannot be completed before another.
        SIMULTANEOUS (1): Hell knows what it means. Just for you to know that tasks are related somehow.
    """
    BLOCKING = 0
    SIMULTANEOUS = 1


class Relation:
    """Class representing how two tasks are connected to each other.

    Attributes:
        instance_id (str):  Object's unique identifier.
        user_id (str): Id of the user who connected these tasks.
        task_from (str): Id of a task from which relation goes.
        task_to (str): Id of a task which is related to.
        relation_type (models.RelationType): Type of the relation - `BLOCKING` or `SIMULTANEOUS`.
        archived (bool): True if one of the connected tasks is archived.
    """

    def __init__(self, instance_id, task_from, task_to, relation_type, user_id, archived=False):
        self.user_id = user_id
        self.instance_id = instance_id
        self.task_from = task_from
        self.task_to = task_to
        self.relation_type = relation_type

        self.archived = archived


class Tag:
    """Class representing a tag instance.

    Attributes:
        instance_id (str):  Object's unique identifier (the same string as `tag` at the moment)
        tag (str): String containing a tag.
    """

    def __init__(self, instance_id, tag):
        self.instance_id = instance_id
        self.tag = tag

    def __str__(self):
        return self.tag


class TaskTagRelation:
    """Class that embodies user tagging a task.

    Attributes:
        instance_id (str):  Object's unique identifier.
        task_id (str): Id of a task relation belongs to.
        user_id (str): Id of a user who established this relation.
        tag (str): Id of a tag (now actually tag's text) attached to a task.
    """

    def __init__(self, instance_id, task_id, tag, user_id):
        self.instance_id = instance_id
        self.task_id = task_id
        self.user_id = user_id
        self.tag = tag

    def __str__(self):
        return "TaskTagRelation: id={} task_id={} tag={}".format(self.instance_id, self.task_id, self.tag)


class Box:
    """Class representing the highest level of user's tasks' hierarchy - place where tasks live.

    Each user is supposed to have 4 boxes from the very beginning: Creator, Admin, Execute and Watch. Their ids are
    user's id + corresponding name.

    Attributes:
        instance_id (str):  Object's unique identifier.
        user_id (str): Id of a user box belongs to.
        title (str): Name of a box.
        archived (bool): Is box archived. If True, so are all its dwellers.
    """

    def __init__(self, instance_id, user_id, title, archived=False):
        self.instance_id = instance_id
        self.user_id = user_id
        self.title = title
        self.archived = archived

    def __str__(self):
        return self.title


class TaskBoxRelation:
    """Class representing task being put in a box by a user.

    Attributes:
        Attributes:
        instance_id (str):  Object's unique identifier.
        user_id (str): Id of a user box belongs to and who put a task there.
        task_id (str): Id of a task which was put in a box.
        box_id (str): Id of a box where task is being put.
        archived (bool): If true, either a task or the whole box is archived.
    """

    def __init__(self, instance_id, user_id, task_id, box_id):
        self.instance_id = instance_id
        self.user_id = user_id
        self.box_id = box_id
        self.task_id = task_id


class NotificationType(enum.Enum):
    """Enum (extends enum.Enum) specifying the event that notification was caused by.

    Options:
        MESSAGE (0):
        DEADLINE (1): Task's deadline term has come and task is OVERDUE now.
        REMINDER (2): Task's reminder term worked.
    """
    MESSAGE = 0
    DEADLINE = 1
    REMINDER = 2


class Notification:
    """Class for objects that are created to notify user about an event with a task he has permission(-s) for.

    Attributes:
        instance_id (str):  Object's unique identifier.
        creation_time (datetime.datetime): Time when notification was created.
        user_id (str): Id of a user whom notification is for.
        task_id (str): Id of a task which raised an event.
        notification_type (models.NotificationType): Defines what has happened.
        viewed (bool): Whether or not user has seen a notification
                      (or at least whether the lib has done everything for this to happen).
    """

    def __init__(self, instance_id, user_id, task_id, creation_time, notification_type, viewed=False):
        self.instance_id = instance_id
        self.creation_time = creation_time
        self.user_id = user_id
        self.task_id = task_id
        self.notification_type = notification_type
        self.viewed = viewed

    def __str__(self):
        return "Task id: {} What happened: {}".format(self.task_id, self.notification_type.name)


class TestObject:
    def __init__(self, instance_id, name):
        self.instance_id = instance_id
        self.name = name

    def __str__(self):
        return "Id: {} Name: {}".format(self.instance_id, self.name)