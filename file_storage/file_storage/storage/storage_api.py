import logging
from datetime import datetime

from dateutil import rrule

import file_storage.models as models
import file_storage.models.str_constants as str_constants
import file_storage.storage.errors as errors
import file_storage.storage.low_level_api as storage
from file_storage.storage_logging import file_storage_log


CREATOR_BOX_TITLE = 'My Box'
SHARED_BOX_TITLE = 'Shared with me'


#def file_storage_log(level=logging.INFO):
    #return storage_logging.file_storage_log(level=level)


def get_next_date(current_date, rule_str):
    """Get the nearest term specified by rule_str."""
    following = list(rrule.rrulestr(rule_str))
    amount = len(following)
    if amount == 0 or amount == 1 and following[0] == current_date:
        return None
    if following[0] == current_date:
        return following[1]

    return following[0]


def shift_rule(rule_str, new_dtstart):
    """Change rule's start date and decrement counter if needed."""
    rule = rrule.rrulestr(rule_str)
    count = rule.count()
    if count > 0:
        count -= 1
    rule = rule.replace(count=count, dtstart=new_dtstart)
    return str(rule)


def make_rule_str(freq, interval=1, dtstart=None, count=None, until=None, byweekday=None):
    return str(rrule.rrule(freq, interval=interval, dtstart=dtstart, count=count, until=until, byweekday=byweekday))


def change_rule_start_date(rule_str, new_dtstart):
    return str(rrule.rrulestr(rule_str).replace(dtstart=new_dtstart))


def select(obj_dict, selector, *args):
    """Apply selector function to each dict's value and return the list of projections."""
    projection = []
    for obj_id in obj_dict:
        selected_obj = selector(obj_dict[obj_id], *args)
        projection.append(selected_obj)

    return projection


class ContainerObject:
    """Use to collect everything during a walk around the storage."""
    def __init__(self):
        self.tasks_list = []
        self.terms_list = []
        self.permissions_list = []
        self.relations_list = []
        self.task_tag_relations_list = []
        self.boxes_list = []
        self.task_box_relations_list = []
        self.notifications_list = []

        self.tasks_dict = {}
        self.terms_dict = {}
        self.permissions_dict = {}
        self.relations_dict = {}
        self.task_tag_relations_dict = {}
        self.boxes_dict = {}
        self.task_box_relations_dict = {}
        self.notifications_dict = {}


class StorageAPI:
    """Provides timd_lib API for managing operations on tasks and other objects from models."""

    @file_storage_log()
    def __init__(self, paths):
        """Set paths to the storage.

        Args:
            paths (dict of type: str): Dict matching classes from models (User, Task, Permission, Term,
                                       Relation, Tag, TaskTagRelation, Box, TaskBoxRelation, Notification)
                                       to json files' names where objects of each type are stored.

        Side effects:
            It's supposed that User's file_storage contains all valid users and Box's file_storage is populated with default users'
            boxes for created and shared tasks.
        """
        self._paths = paths
        self._default_id = 'default_id'

    @file_storage_log()
    def _is_subtask(self, task):
        return task.parent_id is not None

    @file_storage_log()
    def _is_periodical_subtask(self, task):
        return task.parent_id is not None and task.periodical

    @file_storage_log()
    def _is_deadlined(self, task):
        return task.deadline is not None


    @file_storage_log()
    def create_object(self, obj_type, *args, **kwargs):
        """Create new object and save to corresponding file_storage."""
        return storage.create_object(self._paths[obj_type], obj_type, *args, **kwargs)

    @file_storage_log()
    def get_object_by_id(self, obj_type, obj_id):
        """Get object from corresponding file_storage by its id."""
        return storage.get_object_by_id(self._paths[obj_type], obj_type, obj_id)

    @file_storage_log()
    def get_objects_by_id_list(self, obj_type, id_list):
        """Get all objects whose id is included in list as (dict of id: obj)."""
        return storage.get_objects_by_id_list(self._paths[obj_type], obj_type, id_list)

    @file_storage_log()
    def get_objects_by_task_id(self, obj_type, task_id):
        """Get objects of type `obj_type` whose `task_id` attribute equals to given task id as (dict of id: obj)."""
        return storage.find_objects_by_key_field(self._paths[obj_type], obj_type, str_constants.TASK_ID, task_id)

    @file_storage_log()
    def get_first_corresponding_object(self, obj_type, *args, **kwargs):
        """Get any object matching the predicate (see `storage.find_first_corresponding_object` for reference)."""
        return storage.find_first_corresponding_object(self._paths[obj_type], obj_type, *args, **kwargs)

    @file_storage_log()
    def object_exists(self, obj_type, *args, **kwargs):
        """Return True if object with given parameters lies in storage, otherwise return False."""
        try:
            self.get_first_corresponding_object(obj_type, *args, **kwargs)

        except errors.ObjectNotFoundError:
            return False

        return True

    @file_storage_log()
    def get_corresponding_objects(self, obj_type, *args, **kwargs):
        """Get all objects matching the predicate (see `storage.find_corresponding_objects`) as (dict of id: obj)."""
        return storage.find_corresponding_objects(self._paths[obj_type], obj_type, *args, **kwargs)

    @file_storage_log()
    def delete_objects_by_task_id(self, obj_type, task_id):
        """Delete objects of type `obj_type` whose `task_id` attribute equals to given task id."""
        storage.delete_objects_by_key_field(self._paths[obj_type], obj_type, str_constants.TASK_ID, task_id)

    @file_storage_log()
    def delete_corresponding_objects(self, obj_type, *args, **kwargs):
        """Delete all objects matching the predicate (see `storage.delete_corresponding_objects` for reference)."""
        storage.delete_corresponding_objects(self._paths[obj_type], obj_type, *args, **kwargs)

    @file_storage_log()
    def delete_first_corresponding_object(self, obj_type, *args, **kwargs):
        """Delete any object matching the predicate (see `storage.find_first_corresponding_object` for reference)."""
        storage.delete_first_corresponding_object(self._paths[obj_type], obj_type, *args, **kwargs)


    @file_storage_log()
    def create_task_object(self, *args, **kwargs):
        """Create new task object and save it to storage."""
        now = datetime.now()
        return self.create_object(models.Task, now, now, *args, **kwargs)

    @file_storage_log()
    def update_task_object(self, task_id, **kwargs):
        """Update task's attributes with corresponding values from `**kwargs`."""
        storage.update_object_by_id(self._paths[models.Task], models.Task, task_id, **kwargs)

    @file_storage_log()
    def create_deadline_term_object(self, task_id, deadline):
        """Create new deadline term and save to storage."""
        return self.create_object(models.Term, task_id, deadline, term_type=models.TermType.DEADLINE)

    @file_storage_log()
    def create_reminder_term_object(self, task_id, term, relative_delta):
        """Create new reminder term and save to storage."""
        return self.create_object(models.Term, task_id, term, relative_delta=relative_delta,
                                  term_type=models.TermType.REMINDER)

    @file_storage_log()
    def create_tag_object(self, tag):
        """Create new tag and save to storage."""
        return self.create_object(models.Tag, tag, keyfunc=lambda tag: tag.tag)

    @file_storage_log()
    def create_task_box_relation(self, user_id, task_id, box_id=None):
        """Put task in user's box."""
        if box_id is None:
            user = self.get_user_by_id(user_id)
            box_id = user.creator_box

        self.create_object(models.TaskBoxRelation, user_id, task_id, box_id)


    @file_storage_log()
    def get_task_by_id(self, task_id):
        """Return Task object with specified id."""
        return self.get_object_by_id(models.Task, task_id)

    @file_storage_log()
    def get_user_by_id(self, user_id):
        """Return User object with specified id."""
        return self.get_first_corresponding_object(models.User, instance_id=user_id)

    @file_storage_log()
    def get_user_by_username(self, username):
        """Return User with specified login."""
        return self.get_object_by_id(models.User, username)
        #return self.get_first_corresponding_object(models.User, username=user_login)

    @file_storage_log()
    def get_tasks_by_box_id(self, box_id):
        """Get all tasks directly lying in a box and return them as (dict of task_id: Task).

        Side effects:
            This method doesn't search for subtasks these tasks might have.
        """
        task_box_relations_dict = self.get_corresponding_objects(models.TaskBoxRelation, box_id=box_id)
        task_ids = select(task_box_relations_dict, lambda relation: relation.task_id)
        return self.get_objects_by_id_list(models.Task, task_ids)

    @file_storage_log()
    def get_task_permissions(self, task_id):
        """Get all permissions related to specified task as (dict of id: Permission)."""
        return self.get_objects_by_task_id(models.Permission, task_id)

    @file_storage_log()
    def get_task_tag_relations_by_task_id(self, task_id):
        """Get all task-tag relations for specified task as (dict of id: Relation)."""
        return self.get_objects_by_task_id(models.TaskTagRelation, task_id)

    @file_storage_log()
    def get_user_tags(self, username, task_id):
        task_tag_relations = self.get_corresponding_objects(models.TaskTagRelation, task_id=task_id, user_id=username)
        return select(task_tag_relations, selector=lambda relation: relation.tag)

    @file_storage_log()
    def get_relations_by_task_id(self, task_id):
        """Get all relations where specified task is included as to-task or from-task as (dict of id: Relation)."""
        relations_dict = {}
        relations_dict.update(self.get_corresponding_objects(models.Relation, task_from=task_id))
        relations_dict.update(self.get_corresponding_objects(models.Relation, task_to=task_id))

        return relations_dict

    @file_storage_log()
    def get_all_user_relations(self, username, task_id):
        blocking = self.get_corresponding_objects(models.Relation, user_id=username, task_to=task_id,
                                                  relation_type=models.RelationType.BLOCKING)
        blocked = self.get_corresponding_objects(models.Relation, user_id=username, task_from=task_id,
                                                 relation_type=models.RelationType.BLOCKING)
        simultaneous = self.get_corresponding_objects(models.Relation, predicate=lambda relation:
                                                      relation.user_id == username and
                                                      relation.relation_type == models.RelationType.SIMULTANEOUS and
                                                      (relation.task_to == task_id or relation.task_from == task_id))
        return blocking, simultaneous, blocked

    def get_all_user_related_tasks(self, username, task_id):
        blocking, simultaneous, blocked = self.get_all_user_relations(username, task_id)
        blocking_ids = select(blocking, selector=lambda  rel: rel.task_from)
        blocked_ids = select(blocked, selector=lambda rel: rel.task_to)
        simultaneous_ids = select(simultaneous, self.simultaneous_selector, task_id)

        blocking_tasks = self.get_objects_by_id_list(models.Task, blocking_ids)
        simultaneous_tasks = self.get_objects_by_id_list(models.Task, simultaneous_ids)
        blocked_tasks = self.get_objects_by_id_list(models.Task, blocked_ids)

        return blocking_tasks, simultaneous_tasks, blocked_tasks


    def simultaneous_selector(self, relation, task_id):
        if relation.task_from == task_id:
            return relation.task_to

        return relation.task_from



    @file_storage_log()
    def get_subtasks_by_task_id(self, task_id):
        """Get all tasks whose `parent_id` equals `task_id` as (dict of id: Task)."""
        return storage.find_objects_by_key_field(self._paths[models.Task], models.Task,
                                                 str_constants.PARENT_ID, task_id)

    @file_storage_log()
    def get_deadline_term_by_task_id(self, task_id):
        """Get task's deadline term from storage."""
        return storage.find_first_corresponding_object(self._paths[models.Term], models.Term,
                                                       task_id=task_id, term_type=models.TermType.DEADLINE)

    @file_storage_log()
    def get_active_reminders(self, task_id):
        """Get task's reminders that hasn't been deactivated as (dict of id: Term)."""
        return self.get_corresponding_objects(models.Term, task_id=task_id, term_type=models.TermType.REMINDER,
                                              active=True)

    @file_storage_log()
    def get_reminders(self, task_id):
        """Get task's reminders that hasn't been deactivated as (dict of id: Term)."""
        return self.get_corresponding_objects(models.Term, task_id=task_id, term_type=models.TermType.REMINDER)

    @file_storage_log()
    def get_terms_by_task_id(self, task_id):
        """Get all task's terms (both deadline and reminders) as (dict of id: Term)."""
        return self.get_objects_by_task_id(models.Term, task_id)

    @file_storage_log()
    def get_relations_for_user(self, user_id, task_id):
        """Get all user's relations for specified task as (dict of id: Relation)."""
        relations_dict = self.get_corresponding_objects(models.Relation, task_from=task_id, user_id=user_id)
        relations_dict.update(self.get_corresponding_objects(models.Relation, task_to=task_id, user_id=user_id))
        return relations_dict

    @file_storage_log()
    def get_task_tag_relations_for_user(self, user_id, task_id):
        """Get all user's task-tag relations for specified task as (dict of id: TaskTagRelation)."""
        return self.get_corresponding_objects(models.TaskTagRelation, task_id=task_id, user_id=user_id)

    @file_storage_log()
    def get_permissions_by_user_id(self, user_id):
        """Get all permissions given to user as (dict of id: Permission)."""
        return storage.find_objects_by_key_field(self._paths[models.Permission], models.Permission, 'user_id',
                                                 user_id)

    @file_storage_log()
    def get_user_box_by_title(self, user_id, title):
        """Get a user's box's by its title and return its id."""
        box = self.get_first_corresponding_object(models.Box, user_id=user_id, title=title)
        return box.instance_id

    @file_storage_log()
    def get_boxes_by_user_id(self, user_id):
        """Get all user's boxes and return them as (dict of box_id: Box)."""
        return self.get_corresponding_objects(models.Box, user_id=user_id)

    @file_storage_log()
    def get_task_box_relations(self, box_id):
        """Get all TaskBoxRelation objects for box and return them as (dict of id: TaskBoxRelation)."""
        return self.get_corresponding_objects(models.TaskBoxRelation, box_id=box_id)

    @file_storage_log()
    def get_permission(self, username, task_id):
        """Get permission specified user has for this task."""
        return self.get_first_corresponding_object(models.Permission, user_id=username, task_id=task_id)

    @file_storage_log()
    def delete_task_box_relation(self, task_id, user_id=None, box_id=None):
        """Delete corresponding TaskBoxRelation if there is any."""
        try:
            if box_id is None:
                self.delete_first_corresponding_object(models.TaskBoxRelation, user_id=user_id, task_id=task_id)
            elif user_id is None:
                self.delete_first_corresponding_object(models.TaskBoxRelation, box_id=box_id, task_id=task_id)
        except errors.ObjectNotFoundError:
            pass

    @file_storage_log()
    def peek_overdue_terms(self, date):
        """Return all terms whose `term` attribute precedes `date` as (dict of id: Term)."""
        return self.get_corresponding_objects(models.Term, date,
                                              predicate=lambda term, date:
                                              not term.archived and term.active and term.term <= date)

    @file_storage_log()
    def peek_overdue_deadlines(self, date):
        """Return all terms whose `term` attribute precedes `date` as (dict of id: Term)."""
        return self.get_corresponding_objects(models.Term, date,
                                              predicate=lambda term, date:
                                              not term.archived and term.active and term.term <= date
                                              and term.term_type == models.TermType.DEADLINE)


    @file_storage_log()
    def permission_exists(self, user_id, task_id):
        """Return True if permission with specified parameters exists."""
        return self.object_exists(models.Permission, user_id=user_id, task_id=task_id)

    @file_storage_log()
    def reminder_exists(self, task_id, relative_delta):
        """Return True if reminder with specified `task_id` and `relative_delta` exists."""
        return self.object_exists(models.Term, task_id=task_id, relative_delta=relative_delta,
                                  term_type=models.TermType.REMINDER)

    @file_storage_log()
    def relation_exists(self, user_id, task_from, task_to):
        """Return True if any user's relation between these two tasks exists."""
        from_exists = self.object_exists(models.Relation, user_id=user_id, task_from=task_from, task_to=task_to)

        to_exists = self.object_exists(models.Relation, user_id=user_id, task_from=task_to, task_to=task_from)

        return from_exists or to_exists

    @file_storage_log()
    def tag_exists(self, tag):
        """Return True tag already lies in storage."""
        try:
            storage.get_object_by_id(self._paths[models.Tag], models.Tag, tag)
        except errors.ObjectNotFoundError:
            return False

        return True

    @file_storage_log()
    def task_tag_relation_exists(self, user_id, task_id, tag):
        """Return True if task is tagged with specified tag."""
        return self.object_exists(models.TaskTagRelation, user_id=user_id, task_id=task_id, tag=tag)

    @file_storage_log()
    def box_exists(self, user_id, title):
        """Return True if user has a box with specified title."""
        return self.object_exists(models.Box, user_id=user_id, title=title)

    @file_storage_log()
    def is_blocked(self, task_id):
        """Return True if blocking relation where task is to-task exists."""
        return self.object_exists(models.Relation, task_to=task_id, relation_type=models.RelationType.BLOCKING)

    @file_storage_log()
    def task_directly_lies_in_box(self, task_id, box_id):
        """Return True if task is on upper hierarchical level."""
        return self.object_exists(models.TaskBoxRelation, task_id=task_id, box_id=box_id)

    @file_storage_log()
    def has_subtasks(self, task_id):
        """Return True if specified task has subtasks."""
        return self.object_exists(models.Task, parent_id=task_id)

    @file_storage_log()
    def has_uncompleted_subtasks(self, task_id):
        """Return True if any of task's subtasks' statuses is not COMPLETED."""
        subtasks_dict = self.get_subtasks_by_task_id(task_id)
        for subtask_id in subtasks_dict:
            subtask = subtasks_dict[subtask_id]
            if subtask.status != models.Status.COMPLETED:
                return True

        return False

    @file_storage_log()
    def has_admin_access(self, user_id, task_id):
        """Return True if task's permission for user with ADMIN access level exists."""
        return self.object_exists(models.Permission, user_id=user_id, task_id=task_id,
                                  access_level=models.AccessLevel.ADMIN)

    @file_storage_log()
    def has_executor_access(self, user_id, task_id):
        """Return True if task's permission for user with EXECUTOR access level exists."""
        return self.object_exists(models.Permission, user_id=user_id, task_id=task_id,
                                  access_level=models.AccessLevel.EXECUTOR)

    @file_storage_log()
    def has_watcher_access(self, user_id, task_id):
        """Return True if task's permission for user with WATCHER access level exists."""
        return self.object_exists(models.Permission, user_id=user_id, task_id=task_id,
                                  access_level=models.AccessLevel.WATCHER)

    @file_storage_log()
    def has_creator_access(self, user_id, task_id):
        """Return True if user created a task (its `user_id` attribute equals to `user_id` argument).

        Side effects:
            It also means that task's permission for user with CREATOR access level exists.
        """
        task = self.get_task_by_id(task_id)
        if task.user_id == user_id:
            return True

            return False


    @file_storage_log()
    def create_user(self, username):
        """Create new ready-to-use User with boxes for created an shared tasks."""
        try:
            user_id = self.create_object(models.User, username, None, None, keyfunc=lambda user: user.username)
        except errors.ObjectExistsError as e:
            e.message = 'Username `{}` is already taken.'.format(username)
            raise

        creator_box = self.create_box(username, CREATOR_BOX_TITLE)
        shared_box = self.create_box(username, SHARED_BOX_TITLE)
        storage.update_object_by_id(self._paths[models.User], models.User, username,
                                    creator_box=creator_box, shared_box=shared_box)
        return user_id

    @file_storage_log()
    def give_permission(self, username, task_id, access_level):
        """Create a permission with specified access level, save it to storage and put task to user's shared box.

        Raises:
            ObjectExistsError: If permission with specified parameters already exists.
            StorageAPIError: If user tries to give to someone creator's access.
        """
        if self.permission_exists(username, task_id):
            raise errors.ObjectExistsError('User `{}` already has permission for task with id=`{}`.'
                                           .format(username, task_id))

        self.create_object(models.Permission, username, task_id, access_level=access_level)

        user = self.get_user_by_username(username)
        self.create_task_box_relation(username, task_id, box_id=user.shared_box)

    @file_storage_log()
    def withdraw_permission(self, user_id, task_id):
        """Delete permission with specified parameters.

        Raises:
            StorageAPIError: If user tries to withdraw creator's access.

        Side effects:
            Once all permission user had for a task is withdrawn, user  won't see this task in any of his boxes and
            won't be able to find it by tags he attached to it. All relations he attached to or from this task will
            be deleted, too.
        """
        if self.has_creator_access(user_id, task_id):
            raise errors.StorageAPIError()

        self.delete_corresponding_objects(models.TaskTagRelation, user_id=user_id, task_id=task_id)
        self.delete_task_box_relation(task_id, user_id=user_id)
        self.delete_corresponding_objects(models.Relation, user_id, task_id,
                                          predicate=self._relation_belongs_to_user)

        self.delete_first_corresponding_object(models.Permission, user_id=user_id, task_id=task_id)

    @file_storage_log()
    def _relation_belongs_to_user(self, relation, user_id, task_id):
        return relation.user_id == user_id and (relation.task_from == task_id or relation.task_to == task_id)

    @file_storage_log()
    def create_box(self, user_id, title):
        """Create a new box and return its id.

        Raises:
            ObjectExistsError: If user already has a box with such title.
        """
        if self.box_exists(user_id, title):
            raise errors.ObjectExistsError()

        return self.create_object(models.Box, user_id, title)

    @file_storage_log()
    def create_task(self, user_id, title, deadline=None, priority=models.Priority.NONE, box_id=None):
        """Create new Task object with specified parameters, save to storage and return its id.

        Side effects:
            If box_id is None, task will get in default user's `creator_box`.
        """
        task_id = self.create_task_object(user_id, title, deadline=deadline, priority=priority)
        self.create_object(models.Permission, user_id, task_id, access_level=models.AccessLevel.CREATOR)
        self.create_task_box_relation(user_id, task_id, box_id)

        if deadline is not None:
            self.create_deadline_term_object(task_id, deadline)

        return task_id

    @file_storage_log()
    def create_subtask(self, user_id, parent_id, title, deadline=None, priority=models.Priority.NONE):
        """Create a subtask with specified parameters, save to storage and return its id.

        Raises:
            SubtaskError: If hierarchy depth exceeds two levels (parent is a subtask itself).

        Side effects:
            If subtask's deadline is later than parent's deadline (None is supposed to be later than any other term,
            obviously), it will be made equal to parent's deadline. If parent is periodical, subtask will follow
            parent's periodicity rule. All users who had access to parent automatically get the same permissions for a
            subtask.
        """
        parent = self.get_task_by_id(parent_id)

        if self._is_subtask(parent):
            raise errors.SubtaskError()

        deadline, periodical, rule_str = self._get_subtask_valid_attributes(parent, deadline)
        subtask_id = self.create_task_object(user_id, title, parent_id=parent_id, deadline=deadline, priority=priority,
                                             periodical=periodical, rule_str=rule_str)

        self._copy_permissions(parent.instance_id, subtask_id)

        if deadline is not None:
            self.create_deadline_term_object(subtask_id, deadline)

        return subtask_id

    @file_storage_log()
    def _get_subtask_valid_attributes(self, parent, deadline):
        if parent.deadline is not None and (deadline is None or deadline > parent.deadline):
            deadline = parent.deadline

        rule_str = None
        periodical = parent.periodical
        if periodical:
            rule_str = change_rule_start_date(parent.rule_str, deadline)

        return deadline, periodical, rule_str

    @file_storage_log()
    def _copy_permissions(self, source_task_id, dest_task_id):
        container_obj = ContainerObject()
        self.collect_objects_to_copy(models.Permission, source_task_id, container_obj.permissions_list,
                                     task_id=dest_task_id)
        self.create_collected_objects(container_obj)


    @file_storage_log()
    def move_to_parent(self, task_id, parent_id):
        """Add task as a subtask to parent.

        Raises:
            ObjectExistsError: If task is already a subtask of parent.
            SubtaskError: If hierarchy depth exceeds two levels (parent is also a subtask).

        Side effects:
            If subtask's deadline is later than parent's deadline (None is supposed to be later than any other term,
            obviously), it will be made equal to parent's deadline. If parent is periodical, subtask will follow
            parent's periodicity rule. New subtask preserves permissions it had before being moved.
        """
        task = self.get_task_by_id(task_id)
        parent = self.get_task_by_id(parent_id)

        if task.parent_id == parent.instance_id:
            raise errors.ObjectExistsError()

        if self._is_subtask(parent) or self.has_subtasks(task):
            raise errors.SubtaskError()

        self.delete_task_box_relation(task_id, user_id=task.user_id)

        deadline, periodical, rule_str = self._get_subtask_valid_attributes(parent, task.deadline)
        self.update_task_object(task_id, parent_id=parent_id, deadline=deadline,
                                periodical=periodical, rule_str=rule_str)

        container_obj = ContainerObject()
        self._update_terms(task_id, deadline, container_obj)
        self._update_collected_tasks_and_terms(container_obj)

    @file_storage_log()
    def move_to_box(self, task_id, box_id):
        """Move task from its current box or from its parent to a box with specified id.

        Raises:
            ObjectExistsError: If task already directly lies in this box.

        Side effects:
            After subtask is moved to a box, it has no parent.
        """
        task = self.get_task_by_id(task_id)

        if self.task_directly_lies_in_box(task_id, box_id):
            raise errors.ObjectExistsError()


        box = self.get_object_by_id(models.Box, box_id)
        self.delete_task_box_relation(task_id, user_id=box.user_id)
        self.create_task_box_relation(box.user_id, task_id, box_id)

        if self._is_subtask(task):
            self.update_task_object(task_id, parent_id=None)


    @file_storage_log()
    def set_deadline(self, task_id, deadline):
        """Create new deadline term for a task, update task's deadline and save to storage.

        Raises:
            ObjectExistsError: If task is already deadlined (deadline is not None).
        """
        task = self.get_task_by_id(task_id)

        if self._is_deadlined(task):
            raise errors.ObjectExistsError('Task with id=`{}` already has deadline set.'.format(task_id))

        self.update_task_object(task_id, deadline=deadline)
        self.create_deadline_term_object(task_id, deadline)

        return task_id

    @file_storage_log()
    def unset_deadline(self, task_id):
        """Delete task's deadline and reminder terms, set task's deadline attribute to None and save changes.

        Raises:.
            PeriodicityError: If task is periodical.
            SubtaskError: If parent task has its own not-None deadline, becouse None is later than any specified term.
        """
        task = self.get_task_by_id(task_id)

        if task.periodical:
            raise errors.PeriodicityError()

        if self._is_subtask(task):
            parent = self.get_task_by_id(task.parent_id)
            if parent.deadline is not None:
                raise errors.SubtaskError()

        self.update_task_object(task_id, deadline=None)
        self.delete_objects_by_task_id(models.Term, task_id=task_id)

    @file_storage_log()
    def update_deadline(self, task_id, new_deadline):
        """Update task's deadline and reminder terms, update task's deadline attribute, save all changes.

        Raises:
            SubtaskError: If task has a parent and parent's deadline is earlier than `new_deadline`.

        Side effects:
            All reminders will be activated again.
            If task has subtasks whose deadlines are later than `new_deadline`, they'll also be updated..
        """
        task = self.get_task_by_id(task_id)

        if self._is_subtask(task):
            parent = self.get_task_by_id(task.parent_id)
            if parent.deadline is not None and new_deadline > parent.deadline:
                raise errors.SubtaskError()

        container_obj = ContainerObject()
        self.collect_recursively(task_id, container_obj, self._collect_update_deadline_func, new_deadline, task=task)
        self._update_collected_tasks_and_terms(container_obj)

    @file_storage_log()
    def _collect_update_deadline_func(self, task_id, task, container_obj, new_deadline):
        if self._is_subtask(task) and task.deadline <= new_deadline:
            return

        rule_str = None
        if task.rule_str is not None:
            rule_str = change_rule_start_date(task.rule_str, new_deadline)

        container_obj.tasks_list.append((task_id, {str_constants.RULE_STR: rule_str,
                                                   str_constants.DEADLINE: new_deadline}))

        self._update_terms(task_id, new_deadline, container_obj)

    def _update_terms(self, task_id, new_deadline, container_obj):
        terms_dict = self.get_terms_by_task_id(task_id)

        for term_id in terms_dict:
            term = terms_dict[term_id]

            if term.term_type == models.TermType.DEADLINE:
                container_obj.terms_list.append((term_id, {str_constants.TERM: new_deadline,
                                                           str_constants.ACTIVE: True}))

            if term.term_type == models.TermType.REMINDER:
                new_term = new_deadline - term.relative_delta
                container_obj.terms_list.append((term_id, {str_constants.TERM: new_term,
                                                           str_constants.ACTIVE: True}))


    @file_storage_log()
    def set_periodicity(self, task_id, freq, interval=1, count=None, until=None, byweekday=None):
        """Set task's periodicity rule.

        Method forms periodicity rule from the arguments given, updates task's `periodical` and `rule_str` attributes.
        If task is a parent for any subtasks, they'll become periodical, too, with the same rule.

        Args:
            task_id (str):
            freq (int): variable from `dateutil.rrule` module (DAILY, WEEKLY, etc.) indicating what time units period is
                        measured in
            interval (int): Interval between each `freq` iteration.
            count (int): How many occurrences will be generated.
            until (datetime.datetime): If given,, that will specifies the limit of the recurrence.
            byweekday (list of int): If given, define the weekdays where the recurrence will be applied.

        Raises:
            SubtaskError: If you try to set periodicity for a subtask with deadlined parent.
            DeadlineUnsetError: If task's deadline is None.
            ObjectExistsError: If task is already periodical or has periodical subtasks.
        """
        task = self.get_task_by_id(task_id)

        if self._is_subtask(task):
            parent = self.get_task_by_id(task.parent_id)
            if parent.deadline is not None:
                raise errors.SubtaskError('Task id=`{}` has deadlined parent id=`{}`.'.format(task_id, task.parent_id))

        if task.deadline is None:
            raise errors.DeadlineUnsetError('Task id=`{}` has deadline unset.'.format(task_id))

        container_obj = ContainerObject()
        self.collect_recursively(task_id, container_obj, self._collect_set_periodicity_func,
                                 freq, task=task, interval=interval, count=count, until=until, byweekday=byweekday)

        self._update_collected_tasks_and_terms(container_obj)

    @file_storage_log()
    def _collect_set_periodicity_func(self, task_id, task, container_obj, *args, **kwargs):
        if task.periodical:
            raise errors.ObjectExistsError()

        rule_str = make_rule_str(dtstart=task.deadline, *args, **kwargs)
        container_obj.tasks_list.append((task_id, {str_constants.RULE_STR: rule_str, str_constants.PERIODICAL: True}))

    @file_storage_log()
    def unset_periodicity(self, task_id):
        """Unset task's periodicity rule.

        Raises:
            SubtaskError: If task has a periodical parent.

        Side effects:
            All subtasks will stop being periodical, too.
        """
        task = self.get_task_by_id(task_id)

        if self._is_subtask(task):
            parent = self.get_task_by_id(task.parent_id)
            if parent.periodical:
                raise errors.SubtaskError()

        container_obj = ContainerObject()
        self.collect_recursively(task_id, container_obj, self._collect_unset_periodicity_func, task=task)
        self._update_collected_tasks_and_terms(container_obj)

    @file_storage_log()
    def _collect_unset_periodicity_func(self, task_id, task, container_obj):
        container_obj.tasks_list.append((task_id, {str_constants.RULE_STR: None, str_constants.PERIODICAL: False}))


    @file_storage_log()
    def set_reminder(self, task_id, relative_delta):
        """Create reminder term with specified relative delta.

        Raises:
            DeadlineUnsetError: If task's deadline is None.
            ObjectExistsError: If task already has such reminder.
        """
        task = self.get_task_by_id(task_id)

        if not self._is_deadlined(task):
            raise errors.DeadlineUnsetError('Task with id=`{}` has no deadline set.'.format(task_id))

        if self.reminder_exists(task_id, relative_delta):
            raise errors.ObjectExistsError('Task with id=`{}` already has reminder on that time.'.format(task_id))

        self.create_reminder_term_object(task_id, task.deadline - relative_delta, relative_delta)

    @file_storage_log()
    def unset_reminder(self, task_id, relative_delta):
        """Delete specified reminder term."""
        self.delete_corresponding_objects(models.Term, task_id=task_id, term_type=models.TermType.REMINDER,
                                          relative_delta=relative_delta)

    @file_storage_log()
    def unset_all_reminders(self, task_id):
        """Delete all task's reminder terms."""
        self.delete_corresponding_objects(models.Term, task_id=task_id, term_type=models.TermType.REMINDER)


    @file_storage_log()
    def attach_tag(self, user_id, task_id, tag):
        """Create TaskTagRelation between user, task and tag.

        Raises:
            ObjectExistsError: If task is already tagged with this tag.
        """
        if self.task_tag_relation_exists(user_id, task_id, tag):
            raise errors.ObjectExistsError()

        if not self.tag_exists(tag):
            self.create_tag_object(tag)

        self.create_object(models.TaskTagRelation, task_id, tag, user_id)

    @file_storage_log()
    def detach_tag(self, user_id, task_id, tag):
        """Delete corresponding TaskTagRelation."""
        self.delete_corresponding_objects(models.TaskTagRelation, user_id=user_id, task_id=task_id, tag=tag)


    @file_storage_log()
    def attach_relation(self, user_id, task_from, task_to, relation_type):
        """Create Relation object between user and specified tasks.

        Raises:
            ObjectExistsError: If user established relation between these two tasks earlier.
        """
        if self.relation_exists(user_id, task_from, task_to):
            raise errors.ObjectExistsError()

        self.create_object(models.Relation, task_from, task_to, relation_type, user_id)

    @file_storage_log()
    def detach_relation(self, user_id, task_from, task_to):
        """Delete corresponding relation object."""
        self.delete_corresponding_objects(models.Relation, user_id=user_id,
                                          task_from=task_from, task_to=task_to)


    @file_storage_log()
    def complete_task(self, task_id):
        """Set task's status to COMPLETED.

        If task is not periodical, all its terms are deactivated. The same happens when task is a periodical subtask.
        In case task is a periodical parent, method checks if there are any repeats left. If yes, it resumes all
        subtasks and a parent itself, shifting their deadlines according to rule_str, so they all we'll have status
        PENDING.

        Raises:
            StorageAPIError: If there is a BLOCKING relation where specified task is to-task (means it's blocked).
            SubtaskError: If task has uncompleted subtasks.
        """
        task = self.get_task_by_id(task_id)

        if self.is_blocked(task_id):
            raise errors.StorageAPIError('Task id=`{}` is blocked by other tasks.'.format(task_id))

        if self.has_uncompleted_subtasks(task_id):
            raise errors.SubtaskError('Task id=`{}` has uncompleted subtasks.'.format(task_id))

        container_obj = ContainerObject()
        self._collect_complete_task(task_id, task, container_obj)
        self._update_collected_tasks_and_terms(container_obj)

    @file_storage_log()
    def _collect_complete_task(self, task_id, task, container_obj):
        if not task.periodical or self._is_periodical_subtask(task):
            self._collect_complete_task_deactivate_terms(task_id, container_obj)
        else:
            self.collect_recursively(task_id, container_obj, self._collect_complete_task_periodical_parent, task=task)

    @file_storage_log()
    def _collect_complete_task_periodical_parent(self, task_id, task, container_obj):
        next_deadline = get_next_date(task.deadline, task.rule_str)

        if next_deadline is None:
            self._collect_complete_task_deactivate_terms(task_id, container_obj)
        else:
            self._collect_resume_task_shift_terms(task_id, task, container_obj, next_deadline)

    @file_storage_log()
    def _collect_complete_task_deactivate_terms(self, task_id, container_obj):
        container_obj.tasks_list.append((task_id, {str_constants.STATUS: models.Status.COMPLETED}))

        terms = self.get_terms_by_task_id(task_id)
        for term in terms:
            container_obj.terms_list.append((term, {str_constants.ACTIVE: False}))

    @file_storage_log()
    def _collect_resume_task_shift_terms(self, task_id, task, container_obj, next_deadline):
        container_obj.tasks_list.append((task_id, {str_constants.DEADLINE: next_deadline,
                                                   str_constants.RULE_STR: shift_rule(task.rule_str, next_deadline),
                                                   str_constants.STATUS: models.Status.PENDING}))

        terms_dict = self.get_terms_by_task_id(task_id)
        for term_id in terms_dict:
            term = terms_dict[term_id]
            if term.term_type == models.TermType.DEADLINE:
                container_obj.terms_list.append((term_id, {str_constants.TERM: next_deadline,
                                                           str_constants.ACTIVE: True}))
            if term.term_type == models.TermType.REMINDER:
                container_obj.terms_list.append((term_id, {str_constants.TERM: next_deadline - term.relative_delta,
                                                           str_constants.ACTIVE: True}))

    @file_storage_log()
    def is_completed(self, task):
        return task.status == models.Status.COMPLETED

    @file_storage_log()
    def resume_task(self, task_id):
        """Set task's status to PENDING and activate all terms.

        Raises:
            ObjectExistsError: If task's status is not COMPLETED.

        Side effects:
            If subtask is resumed, though its parent's status is COMPLETED, parent will be resumed, too.
        """
        task = self.get_task_by_id(task_id)

        if not self.is_completed(task):
            raise errors.ObjectExistsError()

        container_obj = ContainerObject()
        container_obj.tasks_list.append((task.instance_id, {str_constants.STATUS: models.Status.PENDING}))
        self._update_terms(task_id, task.deadline, container_obj)

        if self._is_subtask(task):
            parent_id = task.parent_id
            parent = self.get_task_by_id(parent_id)
            if self.is_completed(parent):
                container_obj.tasks_list.append((parent_id, {str_constants.STATUS: models.Status.PENDING}))
                self._update_terms(task.parent_id, parent.deadline, container_obj)

        self._update_collected_tasks_and_terms(container_obj)


    @file_storage_log()
    def collect_box_contents(self, box_id, container_obj, collect_func):
        """Apply `collect_func` to each task in a box and all their subtasks (if there are any)."""
        container_obj.boxes_dict[box_id] = box_id
        task_box_relations = self.get_task_box_relations(box_id)
        task_ids = select(task_box_relations, lambda relation: relation.task_id)

        for task_id in task_ids:
            self.collect_recursively(task_id, container_obj, collect_func)

    @file_storage_log()
    def collect_recursively(self, task_id, container_obj, collect_func, *args, task=None, **kwargs):
        """Apply `collect_func` to a task and all its subtasks (if there are any).

        Method walks through all task's subtasks, calls for each of them `collect_func`, passing `container_obj` to it.
        Finally it applies `collect_func` to a root task.

        Args:
            task_id (str): Root task'd id.
            container_obj (ContainerObject): Temporary container passed to each task during the recursion.
            collect_func: Func to populate `container_obj` with necessary objects.
            *args: Positional arguments required by `collect_func`.
            task (Task): Root task (if needed by `collect_func`).
            **kwargs: Keyword arguments required by `collect_func`.
        """
        if task is None or not self._is_subtask(task):
            subtasks_dict = self.get_subtasks_by_task_id(task_id)
            for id in subtasks_dict:
                subtask = subtasks_dict[id]
                self.collect_recursively(id, container_obj, collect_func, task=subtask, *args, **kwargs)

        collect_func(task_id, task, container_obj, *args, **kwargs)


    @file_storage_log()
    def handle_collected_objects(self, container_obj, handler, restore_relations=False, *args, **kwargs):
        """Apply `handler` function to every dict in container object.

        Args:
            container_obj (ContainerObject): Container with objects waiting to be processed.
            handler: Function from `low_level_api` to be applied to each dict.
            restore_relations: If True, `relations_dict` is not being processed (yeah, that's a crutch).
            *args: Positional arguments required by handler function.
            **kwargs: Keyword arguments required by handler function.
        """
        if container_obj.boxes_dict:
            handler(self._paths[models.Box], models.Box, container_obj.boxes_dict, *args, **kwargs)
        if container_obj.task_box_relations_dict:
            handler(self._paths[models.TaskBoxRelation], models.TaskBoxRelation,
                    container_obj.task_box_relations_dict, *args, **kwargs)
        if container_obj.tasks_dict:
            handler(self._paths[models.Task], models.Task, container_obj.tasks_dict, *args, **kwargs)
        if container_obj.terms_dict:
            handler(self._paths[models.Term], models.Term, container_obj.terms_dict, *args, **kwargs)
        if container_obj.permissions_dict:
            handler(self._paths[models.Permission], models.Permission, container_obj.permissions_dict, *args, **kwargs)
        if container_obj.task_tag_relations_dict:
            handler(self._paths[models.TaskTagRelation], models.TaskTagRelation, container_obj.task_tag_relations_dict,
                    *args, **kwargs)
        if not restore_relations and container_obj.relations_dict:
            handler(self._paths[models.Relation], models.Relation, container_obj.relations_dict, *args, **kwargs)

    @file_storage_log()
    def _update_collected_tasks_and_terms(self, container_obj):
        if container_obj.tasks_list:
            storage.update_objects(self._paths[models.Task], models.Task, container_obj.tasks_list)
        if container_obj.terms_list:
            storage.update_objects(self._paths[models.Term], models.Term, container_obj.terms_list)

    @file_storage_log()
    def create_collected_objects(self, container_obj):
        """Create all objects included in `container_obj`'s lists."""
        if container_obj.terms_list:
            storage.add_new_objects(self._paths[models.Term], models.Term, container_obj.terms_list)
        if container_obj.permissions_list:
            storage.add_new_objects(self._paths[models.Permission], models.Permission,
                                    container_obj.permissions_list)
        if container_obj.relations_list:
            storage.add_new_objects(self._paths[models.Relation], models.Relation, container_obj.relations_list)
        if container_obj.task_tag_relations_list:
            storage.add_new_objects(self._paths[models.TaskTagRelation], models.TaskTagRelation,
                                    container_obj.task_tag_relations_list)
        if container_obj.task_box_relations_list:
            storage.add_new_objects(self._paths[models.TaskBoxRelation], models.TaskBoxRelation,
                                    container_obj.task_tag_relations_list)

    @file_storage_log()
    def delete_collected_objects(self, container_obj):
        """Delete all objects collected in container's dicts from storage."""
        self.handle_collected_objects(container_obj, storage.delete_objects_by_id_list)

    @file_storage_log()
    def archive_collected_objects(self, container_obj):
        """Set each container's object's ARCHIVED flag to True and save to storage."""
        self.handle_collected_objects(container_obj, storage.update_objects_with_same_value,
                                      field_name=str_constants.ARCHIVED, field_value=True)

    @file_storage_log()
    def restore_collected_objects(self, container_obj):
        """Set each container's object's ARCHIVED flag to False and save to storage."""
        self.handle_collected_objects(container_obj, storage.update_objects_with_same_value, restore_relations=True,
                                      field_name=str_constants.ARCHIVED, field_value=False)


    @file_storage_log()
    def delete_task(self, task_id):
        """Delete task with specified id, all its subtasks and task-related objects (terms, relations, etc.)"""
        container_obj = ContainerObject()
        self.collect_recursively(task_id, container_obj, self._collect_delete_task)
        self.delete_collected_objects(container_obj)

    @file_storage_log()
    def delete_box(self, box_id):
        """ Delete box, all tasks and subtasks inside and all task-related objects from storage."""
        container_obj = ContainerObject()
        self.collect_box_contents(box_id, container_obj, self._collect_delete_task)
        self.delete_collected_objects(container_obj)

    def _collect_delete_task(self, task_id, task, container_obj):
        container_obj.tasks_dict[task_id] = task_id
        container_obj.terms_dict.update(self.get_terms_by_task_id(task_id))
        container_obj.permissions_dict.update(self.get_task_permissions(task_id))
        container_obj.task_tag_relations_dict.update(self.get_task_tag_relations_by_task_id(task_id))
        container_obj.task_box_relations_dict.update(self.get_objects_by_task_id(models.TaskBoxRelation, task_id))
        container_obj.relations_dict.update(self.get_relations_by_task_id(task_id))


    @file_storage_log()
    def archive_task(self, task_id):
        """Archive task with specified id, all its subtasks, their terms and relations."""
        container_obj = ContainerObject()
        self.collect_recursively(task_id, container_obj, self._collect_archive_task)
        self.archive_collected_objects(container_obj)

    @file_storage_log()
    def archive_box(self, box_id):
        """Archive box, all tasks and subtasks inside and all task-related objects (terms, relations, etc.)."""
        container_obj = ContainerObject()
        self.collect_box_contents(box_id, container_obj, self._collect_archive_task)
        self.archive_collected_objects(container_obj)

    @file_storage_log()
    def _collect_archive_task(self, task_id, task, container_obj):
        container_obj.tasks_dict[task_id] = task_id
        container_obj.terms_dict.update(self.get_terms_by_task_id(task_id))
        container_obj.relations_dict.update(self.get_relations_by_task_id(task_id))


    @file_storage_log()
    def restore_task(self, task_id):
        """Restore task with specified id, all its subtasks, their terms and relations."""
        container_obj = ContainerObject()
        self.collect_recursively(task_id, container_obj, self._collect_archive_task)
        self.restore_collected_objects(container_obj)
        self._restore_relations(container_obj.relations_dict)

    @file_storage_log()
    def restore_box(self, box_id):
        """Restore box, all tasks and subtasks inside and all task-related objects (terms, relations, etc.)."""
        container_obj = ContainerObject()
        self.collect_box_contents(box_id, container_obj, self._collect_archive_task)
        self.restore_collected_objects(container_obj)
        self._restore_relations(container_obj.relations_dict)

    def _restore_relations(self, relations_dict):
        """Crutch for more or less sensible archiving and restoring relations.

        Relation connects two tasks. If one of them is being restored, the other one may still be archived. So the
        process requires further check to restore only relations between two not archived tasks. Method is to be called
        after tasks themselves have been restored.
        """
        dict_to_restore = {}
        for relation_id in relations_dict:
            relation = relations_dict[relation_id]

            task_to = self.get_task_by_id(relation.task_to)
            if task_to.archived:
                continue

            task_from = self.get_task_by_id(relation.task_from)
            if task_from.archived:
                continue
            dict_to_restore[relation.instance_id] = relation

        storage.update_objects_with_same_value(self._paths[models.Relation], models.Relation, dict_to_restore,
                                               field_name=str_constants.ARCHIVED, field_value=False)


    @file_storage_log()
    def collect_objects_to_copy(self, obj_type, source_task_id, container_obj_list, **kwargs):
        """Populate list with copies of `obj_type` task-related objects, replacing fields specified in `**kwargs`."""
        source_obj_dict = self.get_objects_by_task_id(obj_type, source_task_id)
        self.collect_objects_to_copy_from_dict(obj_type, source_obj_dict, container_obj_list, **kwargs)

    @file_storage_log()
    def collect_objects_to_copy_from_dict(self, obj_type, source_obj_dict, container_obj_list, **kwargs):
        """Populate list with copies of `obj_type` objects from dict and replace fields specified in `**kwargs`."""
        for source_id in source_obj_dict:
            source_obj = source_obj_dict[source_id]
            dest_obj = models.copy(obj_type, source_obj, **kwargs)
            container_obj_list.append(dest_obj)

    @file_storage_log()
    def copy_task_object(self, source_task, **kwargs):
        """Return copy of a source task, replacing fields specified in `**kwargs`."""
        fields = source_task.__dict__
        fields.update(kwargs)
        fields.pop(str_constants.INSTANCE_ID)
        return self.create_object(models.Task, **fields)

    @file_storage_log()
    def copy_task(self, source_task_id):
        """Copy source task with all its subtasks and task-related objects, save copy to storage and return its id."""
        container_obj = ContainerObject()

        now = datetime.now()
        source_task = self.get_task_by_id(source_task_id)
        owner_id = source_task.user_id
        copy_id = self.copy_task_object(source_task, creation_time=now, last_update_time=now)
        self._collect_task_attributes_to_copy(owner_id, source_task_id, container_obj, copy_id)

        source_subtasks_dict = self.get_subtasks_by_task_id(source_task_id)
        for id in source_subtasks_dict:
            source_subtask = source_subtasks_dict[id]
            subtask_copy_id = self.copy_task_object(source_subtask, creation_time=now, last_update_time=now,
                                                    parent_id=copy_id)

            self._collect_task_attributes_to_copy(owner_id, id, container_obj, subtask_copy_id)

        self.create_collected_objects(container_obj)

        return copy_id

    @file_storage_log()
    def _collect_task_attributes_to_copy(self, user_id, source_task_id, container_obj, dest_task_id):
        try:
            rel = self.get_first_corresponding_object(models.TaskBoxRelation, user_id=user_id, task_id=source_task_id)
            container_obj.task_box_relations_list.append(models.copy(models.TaskBoxRelation, rel, task_id=dest_task_id))
        except errors.ObjectNotFoundError:
            pass

        self.collect_objects_to_copy(models.Permission, source_task_id, container_obj.permissions_list,
                                     task_id=dest_task_id)
        self.collect_objects_to_copy(models.Term, source_task_id, container_obj.terms_list, task_id=dest_task_id)

        task_tag_relations_dict = self.get_task_tag_relations_for_user(user_id, source_task_id)
        self.collect_objects_to_copy_from_dict(models.TaskTagRelation, task_tag_relations_dict,
                                               container_obj.task_tag_relations_list, task_id=dest_task_id)

        relations_from = self.get_corresponding_objects(models.Relation, task_from=source_task_id, user_id=user_id)
        self.collect_objects_to_copy_from_dict(models.Relation, relations_from,
                                               container_obj.relations_list, task_from=dest_task_id)

        relations_to = self.get_corresponding_objects(models.Relation, task_to=source_task_id, user_id=user_id)
        self.collect_objects_to_copy_from_dict(models.Relation, relations_to,
                                               container_obj.relations_list, task_to=dest_task_id)


    @file_storage_log()
    def handle_overdue_terms(self, date):
        """Process tasks whose deadline or reminder terms are later than `date`.

        Reminder terms are deactivated after they worked once, deadlines stay active until task is completed. In case
        deadline term fires, task's status is set to OVERDUE. Notifications are created for both.
        """

        overdue_terms_dict = self.peek_overdue_terms(date)
        container_obj = ContainerObject()

        task_ids = select(overdue_terms_dict, lambda term: term.task_id)
        tasks_dict = storage.get_objects_by_id_list(self._paths[models.Task], models.Task, task_ids)
        notifications_list = []


        for term_id in overdue_terms_dict:
            term = overdue_terms_dict[term_id]
            task = tasks_dict[term.task_id]
            self._handle_overdue_term(task, term, container_obj, notifications_list)

        self._update_collected_tasks_and_terms(container_obj)
        storage.add_new_objects(self._paths[models.Notification], models.Notification, notifications_list)

    @file_storage_log()
    def _handle_overdue_term(self, task, term, container_obj, notifications_list):

        if term.term_type == models.TermType.DEADLINE:
            container_obj.tasks_list.append((task.instance_id, {str_constants.STATUS: models.Status.OVERDUE}))
            self._form_notification(task.instance_id, models.NotificationType.DEADLINE, notifications_list)
        else:
            container_obj.terms_list.append((term.instance_id, {str_constants.ACTIVE: False}))
            self._form_notification(task.instance_id, models.NotificationType.REMINDER, notifications_list)

    @file_storage_log()
    def _form_notification(self, task_id, notification_type, notlist):
        permissions = self.get_task_permissions(task_id)
        user_ids = select(permissions, lambda permission: permission.user_id)
        id_set = set(user_ids)
        for user_id in id_set:
            print(user_id)
            if not self.object_exists(models.Notification, user_id=user_id, task_id=task_id,
                                      notification_type=notification_type, viewed=False):
                notlist.append(models.Notification(self._default_id, user_id, task_id, datetime.now(), notification_type))


    @file_storage_log()
    def get_hierarchy(self, username, box_key=lambda box: box.title, task_key=lambda task: task.creation_time):
        """Get all tasks user has access to and return them sorted and hierarchically grouped.

        Args:
            username (str):
            box_key: Key function to be called on each box before making comparisons (default key is `title`)
            task_key: Key function to be called on each task before making comparisons (default key is `creation_time`)

        Returns:
            (list of Box, list of list of list of Task): #sorry for that
            The first tuple's item is a list of user's boxes sorted with `box_key`, second is a list of
            corresponding box's contents (for reference see get_box_contents help).

        Side effects:
            The first box in the boxes' list is always user's default `creator_box` and the second is 'shared_box`.
        """
        boxes_list = []
        boxes_contents_list = []

        user = self.get_user_by_username(username)
        boxes_dict = self.get_boxes_by_user_id(username)
        boxes_list.append(boxes_dict.pop(user.shared_box))
        boxes_list.append(boxes_dict.pop(user.creator_box))
        boxes_list.extend(sorted(list(boxes_dict.values()), key=box_key))

        for box in boxes_list:
            contents = self.get_box_contents(box.instance_id, task_key)
            boxes_contents_list.append(contents)

        return boxes_list, boxes_contents_list

    @file_storage_log()
    def get_box_contents(self, box_id, task_key=lambda task: task.creation_time):
        """Get box's contents and return it sorted and hierarchically grouped.

        Method gets all tasks directly lying in a box (we'll call them uppers) and sorts them using `task_key`. Contents
        is a list of the same length as the list of box's uppers. For each upper task method collects subtasks, sorting
        them with `task_key`, extends a list where the upper is the first item with upper's sorted subtasks, and appends
        result to contents.

        Args:
            box_id (str):
            task_key: Key function to be called on each task before making comparisons (default key is `creation_time`).

        Returns:
            (list of lists of tasks): The first task in each list is a parent for all the following tasks in that list.
        """
        contents = []
        upper_dict = self.get_tasks_by_box_id(box_id)
        upper_list = sorted(list(upper_dict.values()), key=task_key)
        length = len(upper_list)

        for i in range(length):
            contents.append([upper_list[i]])
            subtasks_dict = self.get_subtasks_by_task_id(upper_list[i].instance_id)
            subtasks_list = sorted(list(subtasks_dict.values()), key=task_key)
            contents[i].extend(subtasks_list)

        return contents

    @file_storage_log()
    def get_user_overdue_tasks(self, user_id):
        """Get user's tasks whose status is OVERDUE."""
        permissions = self.get_permissions_by_user_id(user_id)
        task_ids = set(select(permissions, lambda perm: perm.task_id))

        tasks = self.get_objects_by_id_list(models.Task, task_ids)
        res = {}
        for t in tasks.values():
            if t.status == models.Status.OVERDUE:
                res[t.instance_id] = t

        return res

    @file_storage_log()
    def get_notifications(self, user_id):
        """Get user's notifications."""
        return storage.find_objects_by_key_field(self._paths[models.Notification], models.Notification,
                                                 str_constants.USER_ID, user_id)

    def get_fresh_user_notifications(self, user_id):
        """Get notifications user hasn't seen yet."""
        return self.get_corresponding_objects(models.Notification, user_id=user_id, viewed=False)

    def view_notifications(self, notifications_list):
        """Set notifications' `viewed` attribute to True."""
        storage.update_objects_with_same_value(self._paths[models.Notification], models.Notification,
                                               notifications_list, str_constants.VIEWED, True)

    def get_tasks_until_date(self, user_id, date):
        """Get tasks that should be completed until `date`."""
        tasks = self.get_corresponding_objects(models.Task, user_id=user_id)
        tasks_list = []
        for t in tasks:
            if t.status != models.Status.COMPLETED and t.deadline <= date:
                tasks_list.append(t)

        return tasks_list

    def update_task_attribute(self, task_id, **kwargs):
        simple_update_kwargs = {}
        for attr_name in kwargs:
            attr_value = kwargs[attr_name]
            if attr_name == str_constants.DEADLINE:
                self.update_deadline(task_id, attr_value)
            elif attr_name == str_constants.TITLE or attr_name == str_constants.PRIORITY:
                simple_update_kwargs[attr_name] = attr_value

        self.update_task_object(task_id, **simple_update_kwargs)
