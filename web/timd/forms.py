from django import forms


from dateutil import rrule

import timd.models as models


FREQUENCY = {
    'MINUTELY': rrule.MINUTELY,
    'HOURLY': rrule.HOURLY,
    'DAILY': rrule.DAILY,
    'WEEKLY': rrule.WEEKLY,
    'MONTHLY': rrule.MONTHLY,
    'YEARLY': rrule.YEARLY
}


WEEKDAY = {
    'MO': 0,
    'TU': 1,
    'WE': 2,
    'TH': 3,
    'FR': 4,
    'SA': 5,
    'SU': 6
}


class BoxForm(forms.ModelForm):
    class Meta:
        model = models.Box
        fields = ['title']


class TaskForm(forms.ModelForm):
    class Meta:
        model = models.Task
        fields = ['title', 'priority']


class RelationForm(forms.ModelForm):
    class Meta:
        model = models.Relation
        fields = ['from_task', 'to_task', 'relation_type']


class CutRelationForm(forms.Form):
    related_task = forms.ModelChoiceField(queryset=models.Task.objects.all())
    type = forms.TypedChoiceField(required=True,
                                  choices=[(rel.value, rel.name) for rel in models.RelationType])


class TagForm(forms.ModelForm):
    class Meta:
        model = models.Tag
        fields = ['tag_text']


class DeadlineForm(forms.Form):
    deadline = forms.DateTimeField(required=False)
    #

#  "DD HH:MM:SS.uuuuuu"
class ReminderForm(forms.ModelForm):
    class Meta:
        model = models.Reminder
        fields = ['timedelta']



class PermissionForm(forms.ModelForm):
    class Meta:
        model = models.Permission
        fields = ['access_level', 'user']


class ConfirmDeleteAttributeForm(forms.Form):
    attribute_name = ''

    @property
    def confirm_question(self):
        return 'Do you wish to delete this {}?'.format(self.attribute_name)


class PeriodicityForm(forms.Form):

    freq = forms.TypedChoiceField(required=True,
                                  choices=[(FREQUENCY[key], key) for key in FREQUENCY],
                                  coerce=int)

    interval = forms.IntegerField(min_value=1, initial=1)
    count = forms.IntegerField(min_value=1, required=False)
    until = forms.SplitDateTimeField(input_date_formats=['%d.%m.%Y'], input_time_formats=['%H:%M'], required=False)
    byweekday = forms.TypedMultipleChoiceField(choices=[(WEEKDAY[key], key) for key in WEEKDAY],
                                               widget=forms.CheckboxSelectMultiple, required=False)

    def clean(self):
        cleaned_data = super(PeriodicityForm, self).clean()
        count = cleaned_data.get('count')
        until = cleaned_data.get('until')
        freq = cleaned_data.get('frequency')
        weekdays = cleaned_data.get('byweekday')

        if count is None and until is None:
            raise forms.ValidationError('Either count or until must be specified.')

        if count is not None and until is not None:
            raise forms.ValidationError('Cannot set both count and until options.')

        if freq == FREQUENCY['WEEKLY'] and not weekdays:
            self.add_error('byweekday', 'Specify weekdays when task should be repeated.')

        #if freq != FREQUENCY['WEEKLY'] and weekdays:
            #self.add_error('byweekday', 'Option is only available for weekly frequency.')

        return self.cleaned_data

class SearchForm(forms.Form):
    text = forms.CharField(widget=forms.TextInput(attrs={'size': '40'}), required=True)