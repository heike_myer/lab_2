##ThisIsMyDesign - Library

_Lib is designed to facilitate writing tasktracking applications._

Module named `lib_api.py` contains class `LibAPI`, which provides interface methods to 
deal with tasks and related objects like deadlines, reminders, tags, permissions and 
relationships.

There is a ready-to-use implementation of json storage, defined in two packages:

  - `models` - defines all necessary classes embodying tasks, users, relations and etc.;
  
  - `storage` - defines functions and classes for processing various storage queries on
  all levels.
  
For further details consult modules' and classes' docs:

    help(<module, class or function>)

Lib supports logging. To enable it, you need to provide following loggers (you should 
also define all settings for them):

  - `library_api` - `INFO` level
  
  - `storage` - `DEBUG` level
  
  - `storage.low_level_api` - propagates to `storage`
  
  - `storage.storage_api` - propagates to `storage`
  
####Installation how-to:

  - clone project from repo
  - move to `thisismydesign_lib` directory:
  
        cd thisismydesign_lib
        
  - install package to your environment:
        
        python setup.py install
