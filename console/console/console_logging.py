import sys
import logging
import logging.handlers
import logging.config

import file_storage.storage_logging as storage_logging
import timd_lib.lib_logging as lib_logging


level_map = {
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
    'NOTSET': logging.NOTSET
}


def configure_loggers(filename, max_bytes=65536, backup_count=2,
                      file_handler_level=logging.DEBUG, console_handler_level=logging.CRITICAL):
    default_file_formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(name)s - '
                                                   '%(action)s - %(func)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    default_console_formatter = logging.Formatter(fmt='%(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    file_handler = logging.handlers.RotatingFileHandler(filename=filename, maxBytes=max_bytes, backupCount=backup_count)
    file_handler.setFormatter(default_file_formatter)
    file_handler.setLevel(file_handler_level)

    console_handler = logging.StreamHandler(stream=sys.stdout)
    console_handler.setFormatter(default_console_formatter)
    console_handler.setLevel(console_handler_level)

    lib_logger = lib_logging.get_lib_logger()
    lib_logger.setLevel(logging.INFO)
    lib_logger.addHandler(file_handler)
    lib_logger.addHandler(console_handler)

    storage_logger = storage_logging.get_file_storage_logger()
    storage_logger.setLevel(logging.DEBUG)
    storage_logger.addHandler(file_handler)
    storage_logger.addHandler(console_handler)
