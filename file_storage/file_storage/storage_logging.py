import functools
import logging
import logging.config


LOGGER_NAME = 'file_storage'


def get_file_storage_logger():
    return logging.getLogger(LOGGER_NAME)


def file_storage_log(level=logging.INFO):
    def log_decorator(func):

        @functools.wraps(func)
        def logging_wrapper(*args, **kwargs):
            logger = get_file_storage_logger()
            extra = {'func': func.__name__, 'action': 'ENTER'}

            logger.log(level, 'Args: {}\tKwargs: {}'.format(args, kwargs), extra=extra)

            try:
                result = func(*args, **kwargs)
                extra['action'] = 'EXIT'
                logger.log(level, 'Result: {}'.format(result), extra=extra)
                return result

            except Exception as e:
                extra['action'] = 'EXCEPTION RAISED'
                logger.log(logging.WARNING, 'Exception: {}'.format(type(e)), extra=extra)
                raise

        return logging_wrapper

    return log_decorator


def configure_default_logger():
    config_dict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '%(asctime)s - %(levelname)s - %(name)s\n%(action)s\t%(func)s\t%(message)s\n',
                'datefmt': '%d-%m-%Y %H:%M:%S'
            }
        },
        'handlers': {
            'console': {
                'level': 'CRITICAL',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
                'formatter': 'default'
            }
        },
        'loggers': {
            'file_storage': {
                'level': 'CRITICAL',
                'handlers': ['console']
            }
        }
    }

    logging.config.dictConfig(config_dict)
