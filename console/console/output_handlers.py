import file_storage.models as models


def print_user(user, *args, **kwargs):
    print('Id: {}\nUsername: {}'.format(user.instance_id, user.username))


def print_task(task, permissions={}, reminders={}, tags=[], relations=[]):
    print('Id: {}\n'
          'Title: {}\n'
          'Creation time: {}\tLast updated: {}\n'
          'Priority: {}\n'
          'Deadline: {}\tStatus: {}\n'
          'Periodicity: {}\n'
          'Archived: {}'.format(task.instance_id, task.title, task.creation_time, task.last_update_time,
                                task.priority.name, task.deadline, task.status.name, task.rule_str,
                                task.archived))

    print('Permissions:')
    for permission in sorted(list(permissions.values()),
                             key=lambda permission: permission.access_level.value):
        print('\t{}: {}'.format(permission.access_level.name, permission.user_id))

    if reminders:
        print('Reminders:')
        for reminder in sorted(list(reminders.values()), key=lambda reminder: reminder.term):
            print('\tTerm: {}\tActive:{}\tTimedelta: {}'.format(reminder.term, reminder.active, reminder.relative_delta))

    if tags:
        print('Tags:')
        for tag in sorted(tags):
            print('\t#{}'.format(tag))

    if relations:

        blocking = relations[0]
        if blocking:
            print('Blocking tasks:')
            for t in blocking.values():
                print('\t{}'.format(t))

        simultaneous = relations[1]
        if simultaneous:
            print('Simultaneous tasks:')
            for t in simultaneous.values():
               # another_task = t.task_from
               # if another_task == task.instance_id:
                #    another_task = t.task_to
                print('\t{}'.format(t))

        blocked = relations[2]
        if blocked:
            print('Blocked tasks:')
            for t in blocked.values():
                print('\t{}'.format(t))


def print_smth(obj):
    print(obj)


def print_archivable_object(obj, indent=''):
    if obj.archived:
        color = 35
    else:
        color = 33

    print(indent + '\033[1;' + str(color) + 'm' + str(obj) + '\033[1;m')


def print_hierarchy(hierarchy, depth):
    #print(type(hierarchy))
    boxes = hierarchy[0]
    tasks = hierarchy[1]
    length = len(boxes)

    for i in range(length):
        print_archivable_object(boxes[i], '')
        if depth == 0:
            continue

        for task_list in tasks[i]:
            print_archivable_object(task_list[0], '\t')
            if depth == 1:
                continue

            task_list_len = len(task_list)
            if task_list_len > 1:
                for j in range(1, task_list_len):
                    print_archivable_object(task_list[j], '\t\t')


print_model_map = {
        models.Task: print_task,
        models.Term: print_smth,
        models.Permission: print_smth,
        models.User: print_user,
        models.Relation: print_smth,
        models.Tag: print_smth,
        models.TaskTagRelation: print_smth,
        models.Box: print_smth,
        models.Notification: print_smth,
        models.TaskBoxRelation: print_smth,
        tuple: print_hierarchy
}


def print_object(obj, *args, **kwargs):
    print_model_map[type(obj)](obj, *args, **kwargs)





def print_tasks_list(tasks):
    for task in tasks:
        print_archivable_object(task)


def print_notifications_list(notifications):
    for n in notifications.values():
        print(n)


def print_task_info(task, reminders):
    print('Id: {}\tTitle: {}'.format(task.instance_id, task.title))
    print('Deadline: {}'.format(task.deadline))
    print('Reminders:')
    for r in reminders.values():
        print(r.term, r.active)
