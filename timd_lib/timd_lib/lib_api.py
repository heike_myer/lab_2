from timd_lib.lib_logging import lib_api_log


class LibraryAPI:
    """Class is made for using it as tasktracking interface.

    It just calls the same name methods of `storage_api` instance, which you pass as an `__init__` argument.
    To learn about methods' arguments' types and return type, you need to consult provided instance's docs.
    """

    @lib_api_log()
    def __init__(self, storage_api):
        """Set instance whose methods will be used for handling tasks.

        Args:
            storage_api: instance that implements required interface.
        """
        self.storage_api = storage_api

    # user methods
    @lib_api_log()
    def create_user(self, username, *args, **kwargs):
        return self.storage_api.create_user(username, *args, **kwargs)

    @lib_api_log()
    def get_user_by_username(self, username):
        return self.storage_api.get_user_by_username(username)

    @lib_api_log()
    def get_user_by_id(self, user_id):
        return self.storage_api.get_user_by_id(user_id)

    # box methods
    @lib_api_log()
    def create_box(self, user, title):
        return self.storage_api.create_box(user, title)

    @lib_api_log()
    def rename_box(self, box, title):
        return self.storage_api.rename_box(box, title)

    @lib_api_log()
    def archive_box(self, box):
        return self.storage_api.archive_box(box)

    @lib_api_log()
    def restore_box(self, box):
        return self.storage_api.restore_box(box)

    @lib_api_log()
    def delete_box(self, box):
        return self.storage_api.delete_box(box)

    @lib_api_log()
    def get_user_boxes(self, user):
        return self.storage_api.get_user_boxes(user)

    @lib_api_log()
    def get_box_by_id(self, box_id):
        return self.storage_api.get_box_by_id(box_id)

    @lib_api_log()
    def get_user_box_by_title(self, user, title):
        return self.storage_api.get_user_box_by_title(user, title)

    @lib_api_log()
    def get_hierarchy(self, user):
        return self.storage_api.get_hierarchy(user)

    #permission methods
    @lib_api_log()
    def give_permission(self, user, task, *args, **kwargs):
        return self.storage_api.give_permission(user, task, *args, **kwargs)

    @lib_api_log()
    def withdraw_permission(self, user, task, *args, **kwargs):
        return self.storage_api.withdraw_permission(user, task, *args, **kwargs)

    @lib_api_log()
    def get_permission(self, user, task):
        return self.storage_api.get_permission(user, task)

    #task and subtask methods
    @lib_api_log()
    def create_task(self, user, *args, **kwargs):
        return self.storage_api.create_task(user, *args, **kwargs)

    @lib_api_log()
    def create_subtask(self, user, parent, *args, **kwargs):
        return self.storage_api.create_subtask(user, parent, *args, **kwargs)

    @lib_api_log()
    def complete_task(self, task):
        return self.storage_api.complete_task(task)

    @lib_api_log()
    def resume_task(self, task):
        return self.storage_api.resume_task(task)

    @lib_api_log()
    def archive_task(self, task):
        return self.storage_api.archive_task(task)

    @lib_api_log()
    def restore_task(self, task):
        return self.storage_api.restore_task(task)

    @lib_api_log()
    def delete_task(self, task):
        return self.storage_api.delete_task(task)

    @lib_api_log()
    def get_task_by_id(self, task_id):
        return self.storage_api.get_task_by_id(task_id)

    @lib_api_log()
    def get_all_subtasks(self, parent):
        return self.storage_api.get_all_subtasks(parent)

    @lib_api_log()
    def get_accessible_subtasks(self, user, parent):
        return self.storage_api.get_accessible_subtasks(user, parent)

    @lib_api_log()
    def get_tasks_from_box(self, box):
        return self.storage_api.get_tasks_from_box(box)

    @lib_api_log()
    def get_shared_tasks(self, user):
        return self.storage_api.get_shared_tasks(user)

    @lib_api_log()
    def get_created_tasks(self, user):
        return self.storage_api.get_created_tasks(user)

    @lib_api_log()
    def get_task_permissions(self, task):
        return self.storage_api.get_task_permissions(task)

    @lib_api_log()
    def get_user_overdue_tasks(self, user):
        return self.storage_api.get_user_overdue_tasks(user)

    @lib_api_log()
    def get_user_completed_tasks(self, user):
        return self.storage_api.get_user_completed_tasks(user)

    @lib_api_log()
    def get_user_tasks_until(self, user, date):
        return self.storage_api.get_user_tasks_until(self, user, date)

    @lib_api_log()
    def get_user_tasks(self, user, *args, **kwargs):
        return self.storage_api.get_user_tasks(user, *args, **kwargs)

    @lib_api_log()
    def get_all_user_related_tasks(self, user, task):
        return self.storage_api.get_all_user_related_tasks(user, task)

    @lib_api_log()
    def update_task_attribute(self, task, **kwargs):
        return self.storage_api.update_task_attribute(task, **kwargs)

    def move_to_parent(self, parent, task):
        return self.storage_api.move_to_parent(parent, task)

    def move_to_box(self, box, task):
        return self.storage_api.move_to_box(box, task)

    def get_user_box(self, user, task):
        return self.storage_api.get_user_box(user, task)

    #deadline methods
    @lib_api_log()
    def set_deadline(self, task, deadline):
        return self.storage_api.set_deadline(task, deadline)

    @lib_api_log()
    def update_deadline(self, task, deadline):
        return self.storage_api.update_deadline(task, deadline)

    @lib_api_log()
    def unset_deadline(self, task):
        return self.storage_api.unset_deadline(task)

    #reminder methods
    @lib_api_log()
    def set_reminder(self, task, timedelta):
        return self.storage_api.set_reminder(task, timedelta)

    @lib_api_log()
    def unset_reminder(self, reminder, *args, **kwargs):
        return self.storage_api.unset_reminder(reminder, *args, **kwargs)

    @lib_api_log()
    def unset_all_reminders(self, task):
        return self.storage_api.unset_all_reminders(task)

    @lib_api_log()
    def get_reminder(self, task, timedelta):
        return self.storage_api.get_reminder(task, timedelta)

    @lib_api_log()
    def get_reminders(self, task):
        return self.storage_api.get_reminders(task)

    #periodicity
    @lib_api_log()
    def set_periodicity(self, task, **kwargs):
        return self.storage_api.set_periodicity(task, **kwargs)

    @lib_api_log()
    def unset_periodicity(self, task):
        return self.storage_api.unset_periodicity(task)

    #tag methods
    @lib_api_log()
    def attach_tag(self, user, task, tag_text):
        return self.storage_api.attach_tag(user, task, tag_text)

    @lib_api_log()
    def detach_tag(self, *args, **kwargs):
        return self.storage_api.detach_tag(*args, **kwargs)

    @lib_api_log()
    def get_tag(self, user, task, tag_text):
        return self.storage_api.get_tag(user, task, tag_text)

    @lib_api_log()
    def get_user_tags(self, user, task):
        return self.storage_api.get_user_tags(user, task)

    #relation methods
    @lib_api_log()
    def attach_relation(self, user, task_x, task_y, *args, **kwargs):
        return self.storage_api.attach_relation(user, task_x, task_y, *args, **kwargs)

    @lib_api_log()
    def detach_relation(self, *args, **kwargs):
        return self.storage_api.detach_relation(*args, **kwargs)

    @lib_api_log()
    def get_relation(self, user, task_x, task_y, *args, **kwargs):
        return self.storage_api.get_relation(user, task_x, task_y, *args, **kwargs)

    @lib_api_log()
    def get_all_user_relations(self, user, task):
        return self.storage_api.get_all_user_relations(user, task)

    @lib_api_log()
    def get_user_relations(self, user, task, *args, **kwargs):
        return self.storage_api.get_user_relations(user, task, *args, **kwargs)

    #notification methods
    @lib_api_log()
    def get_all_user_notifications(self, user):
        return self.storage_api.get_all_user_notifications(user)

    @lib_api_log()
    def get_fresh_user_notifications(self, user):
        return self.storage_api.get_fresh_user_notifications(user)

    @lib_api_log()
    def view_notifications(self, notifications, *args):
        return self.storage_api.view_notifications(notifications, *args)

    @lib_api_log()
    def delete_viewed_notifications(self, date):
        return self.storage_api.delete_viewed_notifications(date)

    #special
    @lib_api_log()
    def handle_overdue_terms(self, date):
        return self.storage_api.handle_overdue_terms(date)

    @lib_api_log()
    def get_blocked(self, user, task):
        return self.storage_api.get_blocked(user, task)

    @lib_api_log()
    def get_tasks_for_date(self, user, date):
        return self.storage_api.get_tasks_for_date(user, date)

    @lib_api_log()
    def get_tasks_in_date_range(self, user, from_date, until_date):
        return self.storage_api.get_tasks_in_date_range(user, from_date, until_date)
