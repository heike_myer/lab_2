import os.path as path
import setuptools

setuptools.setup(
    name='ThisIsMyDesign_console',
    version='1.0',

    package_dir={'': 'console'},
    packages=setuptools.find_packages(where='console'),
    long_description=open(path.join(path.dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
            ['timd = console.main:main']
    },
    test_suite='test_console'
)
