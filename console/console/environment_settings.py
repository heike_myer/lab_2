import os.path
import configparser

import file_storage.models as models

import console.console_logging as logging


DIR_NAME = os.path.dirname(__file__)
CONFIG_PATH = os.path.join(DIR_NAME, 'config.ini')
SESSION_PATH = os.path.join(DIR_NAME, 'session.ini')


def form_storage_paths(data_directory):
    paths = {
        models.Task: os.path.join(data_directory, "tasks.json"),
        models.Term: os.path.join(data_directory, "terms.json"),
        models.Permission: os.path.join(data_directory, "permissions.json"),
        models.User: os.path.join(data_directory, "users.json"),
        models.Relation: os.path.join(data_directory, "relations.json"),
        models.Tag: os.path.join(data_directory, "tags.json"),
        models.TaskTagRelation: os.path.join(data_directory, "task_tag_relations.json"),
        models.Box: os.path.join(data_directory, "boxes.json"),
        models.Notification: os.path.join(data_directory, "notifications.json"),
        models.TaskBoxRelation: os.path.join(data_directory, "task_box_relations.json"),
    }

    return paths


def get_config_parser():
    conf = configparser.ConfigParser()
    conf.read(CONFIG_PATH)
    return conf


def get_session_parser():
    conf = configparser.ConfigParser()
    conf.read(SESSION_PATH)
    return conf


def configure_loggers(config_parser):
    section = config_parser['logging']
    filename = section['filename']
    max_bytes = section.getint('max_bytes')
    backup_count = section.getint('backup_count')
    file_handler_level = section['file_handler_level']
    console_handler_level = section['console_handler_level']

    logging.configure_loggers(filename, max_bytes=max_bytes, backup_count=backup_count,
                              file_handler_level=file_handler_level, console_handler_level=console_handler_level)


def get_storage_paths(config_parser):
    section = config_parser['storage']
    data_directory_path = section['directory']
    data_directory = build_storage(data_directory_path)

    return form_storage_paths(data_directory)


def get_current_username():
    session_parser = get_session_parser()
    user_id = None
    if session_parser.has_section('current_user'):
        user_id = session_parser['current_user']['username']

    return user_id


def build_storage(data_directory_path):
    if is_folder_empty(data_directory_path):
        create_data_files(data_directory_path)

    return data_directory_path


def is_folder_empty(path):
    count = 0
    for dirpath, dirname, files in os.walk(path):
        if files:
            return False
        count += 1

        if count > 1:
            return False

    return True


def create_data_files(data_folder):
    paths = form_storage_paths(data_folder)

    for path in paths.values():
        with open(path, mode="w") as file:
            file.write("{}")

    return paths
