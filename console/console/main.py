from datetime import datetime

from file_storage.storage.storage_api import StorageAPI
from timd_lib.lib_api import LibraryAPI

import console.environment_settings as settings
import console.parser as console_parser


def human_friendly_error(func):
    def decorator(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return result

        except Exception as e:
            print('Error: {}'.format(e))

    return decorator


@human_friendly_error
def main():
    config_parser = settings.get_config_parser()

    settings.configure_loggers(config_parser)
    storage_paths = settings.get_storage_paths(config_parser)
    user_id = settings.get_current_username()

    lib_api = LibraryAPI(StorageAPI(storage_paths))
    lib_api.handle_overdue_terms(datetime.now())

    parser = console_parser.create_main_parser()
    namespace = parser.parse_args()
    result = namespace.handler(lib_api, user_id, namespace)

    if result is not None:
        print(result)

if __name__ == "__main__":
    main()
