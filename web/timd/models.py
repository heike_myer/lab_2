import enum
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from django.utils.text import slugify
from dateutil import rrule


LAST = None
COUNTER = 0
EMUL_NOW = None

class TestModel(models.Model):
    name = models.CharField(max_length=20)
    age = models.IntegerField(default=100)


class CustomUser(models.Model):
    #slug = models.SlugField(unique=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created_box = models.OneToOneField('Box', related_name='created_owner', null=True, blank=True)
    shared_box = models.OneToOneField('Box', related_name='shared_owner', null=True, blank=True)

    tagged_tasks = models.ManyToManyField('Task', through='Tag')

    def __str__(self):
        return self.user.username


class Box(models.Model):
    #slug = models.SlugField(unique=True, null=True)

    user = models.ForeignKey(CustomUser, related_name='boxes')
    title = models.CharField(max_length=150, null=True)
    tasks = models.ManyToManyField('Task', blank=True)

    def __str__(self):
        return self.title

    @property
    def title_slug(self):
        return slugify(self.title)


class Status(enum.Enum):
    OVERDUE = 0
    PENDING = 1
    COMPLETED = 2


class Priority(enum.Enum):
    HIGH = 0
    MEDIUM = 1
    LOW = 2
    NONE = 3


class AccessLevel(enum.Enum):
    CREATOR = 0
    ADMIN = 1
    EXECUTOR = 2
    WATCHER = 3




class Periodicity(models.Model):
    FREQUENCY = (
        ('MINUTELY', rrule.MINUTELY),
        ('HOURLY', rrule.HOURLY),
        ('DAILY', rrule.DAILY),
        ('WEEKLY', rrule.WEEKLY),
        ('MONTHLY', rrule.MONTHLY),
        ('YEARLY', rrule.YEARLY)
    )

    WEEKDAY = {
        ('MO', 0),
        ('TU', 1),
        ('WE', 2),
        ('TH', 3),
        ('FR', 4),
        ('SA', 5),
        ('SU', 6)
    }

    frequency = models.IntegerField(choices=FREQUENCY)
    interval = models.IntegerField(default=1)
    count = models.IntegerField(null=True, blank=True)
    until = models.DateTimeField(null=True, blank=True)
    byweekday = models.CharField(null=True, blank=True, max_length=12)

    @property
    def weekday_list(self):
        if self.byweekday is None:
            return None

        weekday_list = []
        for weekday in self.byweekday.split():
            weekday_list.append(int(weekday))

        return weekday_list

    @weekday_list.setter
    def weekday_list(self, weekday_list):
        if weekday_list is not None:
            weekday_str = ' '
            self.byweekday = weekday_str.join(weekday_list)
        #self.save()

    def shift(self):
        if self.count is not None and self.count > 0:
            self.count -= 1
            self.save()

    def rule(self, dtstart):
        return rrule.rrule(self.frequency, dtstart=dtstart, interval=self.interval,
                           count=self.count, until=self.until, byweekday=self.weekday_list)

    def next_date(self, current_dtstart):
        rule = self.rule(current_dtstart)
        following = list(rule)
        amount = len(following)
        if amount == 0 or amount == 1 and following[0] == current_dtstart:
            return None
        if following[0] == current_dtstart:
            return following[1]

        return following[0]

    def __str__(self):
        return 'Periodicity: fr={} inter={} count={} until={} byweekday={} weklist={}'.format(self.frequency, self.interval,
                                                                                   self.count, self.until,
                                                                                   self.byweekday, str(self.weekday_list))




class Task(models.Model):
    periodicity = models.ForeignKey(Periodicity, null=True, blank=True, on_delete=models.SET_NULL)

    #slug = models.SlugField(unique=True, null=True)

    creator = models.ForeignKey(CustomUser, related_name='created_tasks')
    users = models.ManyToManyField(CustomUser, through='Permission', related_name='tasks')

    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name='subtasks')

    creation_time = models.DateTimeField(editable=False)
    last_update_time = models.DateTimeField(editable=False)

    title = models.CharField(max_length=150, blank=False)

    deadline = models.DateTimeField(null=True, blank=True)

   # periodical = models.BooleanField(default=False)
   # rule_str = models.CharField(max_length=150, null=True)

    priority = models.IntegerField(
        choices=[(priority.value, priority.name) for priority in Priority],
        default=Priority.NONE.value
    )

    status = models.IntegerField(
        choices=[(status, status.name) for status in Status],
        default=Status.PENDING.value
    )

    archived = models.BooleanField(default=False)

    related_tasks = models.ManyToManyField('self', through='Relation', symmetrical=False,
                                           through_fields=('to_task', 'from_task'),
                                           related_name='relating_tasks')

    def __str__(self):
        return "{} {} {}".format(self.creator, self.title, self.id)

    @property
    def has_parent(self):
        return self.parent is not None



    @property
    def title_slug(self):
        return slugify(self.title)

    @property
    def can_unset_deadline(self):
        parent = self.parent

        if parent is not None and parent.deadline is not None:
            return False

        return True

    @property
    def max_deadline(self):
        parent = self.parent

        if parent is not None:
            return parent.deadline

        return None

    @property
    def can_set_periodicity(self):
        parent = self.parent

        if parent is not None and parent.deadline is not None:
            return False

        if self.deadline is None:
            return False

        return True

    @property
    def can_unset_periodicity(self):
        parent = self.parent

        if parent is not None and parent.periodical:
            return False

        return True

    def save(self):
        now = timezone.now()
        if not self.id:
            self.creation_time = now

        self.last_update_time = now
        super().save()


class Reminder(models.Model):
    task = models.ForeignKey(Task, related_name='reminders', on_delete=models.CASCADE, null=True)
    term = models.DateTimeField()
    timedelta = models.DurationField()
    active = models.BooleanField(default=True)

    def __str__(self):
        return " {} timedelta {} {}".format(self.term, self.timedelta, self.active)


class Permission(models.Model):
    user = models.ForeignKey(CustomUser)
    task = models.ForeignKey(Task, related_name='permissions')

    access_level = models.IntegerField(
        choices=[(access_level.value, access_level.name) for access_level in AccessLevel],
        blank=False
    )

    def __str__(self):
        return "{} {} {}".format(self.user, self.task, self.access_level)


class RelationType(enum.Enum):
    BLOCKING = 0
    SIMULTANEOUS = 1


class Relation(models.Model):
    user = models.ForeignKey(CustomUser, null=True)
    from_task = models.ForeignKey(Task, related_name='where_to')
    to_task = models.ForeignKey(Task, related_name='where_from')

    relation_type = models.IntegerField(
        choices=[(relation_type.value, relation_type.name) for relation_type in RelationType],
        blank=False
    )


class NotificationType(enum.Enum):
    MESSAGE = 0
    DEADLINE = 1
    REMINDER = 2


class Notification(models.Model):
    user = models.ForeignKey(CustomUser, related_name='notifications')
    task = models.ForeignKey(Task)

    creation_time = models.DateTimeField(null=True)
    viewed_time = models.DateTimeField(null=True)

    notification_type = models.IntegerField(
        choices=[(notification_type.value, notification_type.name) for notification_type in NotificationType],
        default=NotificationType.MESSAGE.value
    )

    viewed = models.BooleanField(default=False)

    def __str__(self):
        return "{} {} {} {} {}".format(self.task, self.notification_type, self.creation_time, self.viewed,
                                       self.viewed_time)

    #def save(self):
      #  now = timezone.now()
     #   if not self.id:
        #    self.creation_time = now

       # super().save()




class Tag(models.Model):
    tag_text = models.CharField(max_length=150)
    user = models.ForeignKey(CustomUser)
    task = models.ForeignKey(Task, related_name='tags')
