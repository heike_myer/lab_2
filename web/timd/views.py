import django
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
import django.views.generic.edit as generic_edit
import django.views as django_views
import django.views.generic.base as base

from datetime import datetime

import timd.forms as forms
import timd.models as models

from timd_lib.lib_api import LibraryAPI
from timd.db_api import DatabaseAPI


last_handle = None


def get_lib_api():
    storage_api = DatabaseAPI()
    return LibraryAPI(storage_api)


def get_current_task_id(kwargs):
    current_id = kwargs.get('subtask_id')
    if current_id is None:
        current_id = kwargs['task_id']

    return current_id


def profile_redirect(request):
    return HttpResponseRedirect(reverse('timd:profile', args=[request.user.username]))

import django.utils.timezone as timezone


def handle_overdue_terms(func):
    def wrapper(self, *args, **kwargs):
        self.lib_api.handle_overdue_terms(timezone.now())
        return func(self, *args, **kwargs)

    return wrapper


class ExtraContextMixin:
    extra_context = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.extra_context)
        return context


class LibAPIMixin:
    @property
    def lib_api(self):
        return LibraryAPI(DatabaseAPI())


def get_notifications_url(request):
    return 'http://' + request.META.get('HTTP_HOST') + reverse('timd:notifications', args=[request.user.username])




from datetime import timedelta
import pytz




class Profile(LibAPIMixin, base.TemplateView):


    #counter = 0
   # last_handle = None#datetime(2018, 9, 19, tzinfo=pytz.UTC)

    template_name = 'timd/profile.html'

    def get_context_data(self, **kwargs):
        if models.EMUL_NOW is None:
            models.EMUL_NOW = timezone.now() + timedelta(days=models.COUNTER)
        #ha = 'help'
        if self.request.META.get('HTTP_REFERER') == get_notifications_url(self.request):
            self.lib_api.view_notifications(self.kwargs['username'], models.EMUL_NOW)
        #    ha = 'cool'
        #if self.request.path == ''
       # if self.last_handle is not None and (timezone.now() - self.last_handle).min > 1:
           # self.counter += 1

        #self.lib_api.handle_overdue_terms(timezone.now() + timedelta(days=self.counter))
        if models.LAST is None or (timezone.now() - models.LAST).seconds > 30:
            models.LAST = timezone.now()
            models.COUNTER += 1
            models.EMUL_NOW = timezone.now() + timedelta(days=models.COUNTER)


        self.lib_api.handle_overdue_terms(models.EMUL_NOW)

        context = super().get_context_data(**kwargs)

        username = self.kwargs['username']
        shared_box, created_box, boxes = self.lib_api.get_user_boxes(username)

        context.update({
            'shared_box': shared_box,
            'created_box': created_box,
            'boxes': boxes,
            'created_tasks': self.lib_api.get_tasks_from_box(created_box),
            'path': self.request.META.get('HTTP_REFERER') + " " + get_notifications_url(self.request),
            'lst': models.LAST,
            'fuck': timezone.now(),
            'count': models.COUNTER,
            'delta': (timezone.now() - models.LAST).seconds,
            'emul_now': models.EMUL_NOW
        })

        return context


class Box(Profile):
    template_name = 'timd/box.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        box = self.lib_api.get_box_by_id(self.kwargs['box_id'])
        context.update({
            'box': box,
            'tasks_list': box.tasks.all(),
           # 'path': self.request.META.get('HTTP_REFERER')
        })

        return context


def get_task_context(lib_api, **kwargs):
    current_task_slug = get_current_task_id(kwargs)

    username = kwargs['username']

    current_task = lib_api.get_task_by_id(current_task_slug)

    #blocking, simultaneous, blocked = lib_api.get_all_user_relations(username, current_task)

    context = {
        'access_level': lib_api.get_permission(username, current_task).access_level,
        'current': current_task,
        'relations': lib_api.get_all_user_relations(username, current_task),
        'reminders': lib_api.get_reminders(current_task),
        'reminders_str': len(lib_api.get_reminders(current_task)),
        'permissions': lib_api.get_task_permissions(current_task),
     #   'blocking_tasks': blocking,#lib_api.get_blocking_tasks(username, current_task),
        #'blocked_tasks': blocked,#lib_api.get_blocked_tasks(username, current_task),
       # 'simultaneous_tasks': simultaneous,#lib_api.get_simultaneous_tasks(username, current_task),
        'tags': lib_api.get_user_tags(username, current_task),
        'subtasks': lib_api.get_all_subtasks(current_task),
        'form': forms.SearchForm()
    }

    #if current_task.parent is None:
    #    context.update({'subtasks': lib_api.get_subtasks(username, current_task)})
    return context


class Task(Profile):
    template_name = 'timd/task.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(get_task_context(self.lib_api, **self.kwargs))
        #context['path'] = self.request.META.get('HTTP_REFERER')

        return context


class Notifications(Profile):
    template_name = 'timd/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'page_slug': 'notifications',
            'title': 'Notifications',
            'a': 'hate it',
            'notifications': self.lib_api.get_all_user_notifications(self.kwargs['username']),
            #'path': self.request.META.get('HTTP_REFERER')
        })

        #self.lib_api.view_notifications(context['notifications'])

        return context


class Shared(Profile):
    template_name = 'timd/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'page_slug': 'shared-with-me',
            'title': 'Shared with me',
            'tasks_list': self.lib_api.get_shared_tasks(self.kwargs['username']),
           # 'path': self.request.META.get('HTTP_REFERER')
        })

        return context



class Today(Profile):
    template_name = 'timd/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        now = timezone.now().date()
        context.update({
            'page_slug': 'today',
            'title': 'Today',
            'now' : now,
            'tasks_list': self.lib_api.get_tasks_for_date(self.kwargs['username'], now)
           # 'path': self.request.META.get('HTTP_REFERER')
        })

        return context



class Week(Profile):
    template_name = 'timd/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        now = timezone.now().date()
        context.update({
            'page_slug': 'week',
            'title': 'Week',
            'now' : now,
            'tasks_list': self.lib_api.get_tasks_in_date_range(self.kwargs['username'], now, now+timedelta(days=7))
           # 'path': self.request.META.get('HTTP_REFERER')
        })

        return context


class Completed(Profile):
    template_name = 'timd/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'page_slug': 'completed',
            'title': 'Completed',
            'tasks_list': self.lib_api.get_user_completed_tasks(self.kwargs['username']),
            #'path': self.request.META.get('HTTP_REFERER')
        })

        return context


class RedirectMixin(LibAPIMixin, ExtraContextMixin):
    redirect_url_name = None

    def get_redirect_url_name(self):
        return self.redirect_url_name

    def redirect(self):
        return HttpResponseRedirect(reverse(self.get_redirect_url_name(), kwargs=self.kwargs))

    def redirect_back(self):
        return HttpResponseRedirect(self.request.path)

    def update_kwargs_redirect(self, **kwargs):
        self.kwargs.update(kwargs)
        return self.redirect()

    def pop_kwargs_redirect(self):
        if 'subtask_id' in self.kwargs:
            self.kwargs.pop('subtask_title')
            self.kwargs.pop('subtask_id')

        elif 'task_id' in self.kwargs:
            self.kwargs.pop('task_title')
            self.kwargs.pop('task_id')

        else:
            self.kwargs.pop('box_title')
            self.kwargs.pop('box_id')

        return self.redirect()

    def pop_and_update_redirect(self, *pop_args, **update_kwargs):
        for arg in pop_args:
            self.kwargs.pop(arg)

        self.kwargs.update(update_kwargs)
        #self.kwargs.pop('box_title')
        #self.kwargs.pop('box_id')

        return self.redirect()


class CreateBox(RedirectMixin, generic_edit.CreateView):
    form_class = forms.BoxForm
    template_name = 'timd/form.html'
    redirect_url_name = 'timd:profile'

    def form_valid(self, form):
        self.lib_api.create_box(self.kwargs['username'], form.cleaned_data['title'])
        return self.redirect()


class RenameBox(RedirectMixin, generic_edit.UpdateView):
    form_class = forms.BoxForm
    model = models.Box
    template_name = 'timd/form.html'
    redirect_url_name = 'timd:box'

    def get_object(self, queryset=None):
        return self.lib_api.get_box_by_id(self.kwargs['box_id'])

    def form_valid(self, form):
        box = self.lib_api.rename_box(self.object, form.cleaned_data['title'])
        return self.update_kwargs_redirect(box_title=box.title_slug)


class DeleteBox(RedirectMixin, generic_edit.DeleteView):
    model = models.Box
    template_name = 'timd/confirm_delete.html'
    redirect_url_name = 'timd:profile'

    def get_object(self, queryset=None):
        return self.kwargs['box_id']

    def delete(self, request, *args, **kwargs):
        self.lib_api.delete_box(self.kwargs['box_id'])
        return self.pop_kwargs_redirect()


class CreateTask(RedirectMixin, generic_edit.CreateView):
    form_class = forms.TaskForm
    template_name = 'timd/form.html'
    redirect_url_name = 'timd:task'

    def form_valid(self, form):
        task = self.lib_api.create_task(self.kwargs['username'], self.kwargs['box_id'], **form.cleaned_data)
        return self.pop_and_update_redirect('box_title', 'box_id', task_title=task.title_slug, task_id=task.id)


class CreateSubtask(RedirectMixin, generic_edit.CreateView):
    form_class = forms.TaskForm
    template_name = 'timd/form.html'
    redirect_url_name = 'timd:subtask'

    def form_valid(self, form):
        subtask = self.lib_api.create_subtask(self.kwargs['username'], self.kwargs['task_id'], **form.cleaned_data)
        return self.update_kwargs_redirect(subtask_title=subtask.title_slug, subtask_id=subtask.id)


class DeleteTask(RedirectMixin, generic_edit.DeleteView):
    model = models.Task
    template_name = 'timd/confirm_delete.html'

    def get_redirect_url_name(self):
        if self.kwargs.get('task_id') is not None:
            return 'timd:task'

        return 'timd:box'

    def get_object(self, queryset=None):
        return self.lib_api.get_task_by_id(get_current_task_id(self.kwargs))

    def delete(self, request, *args, **kwargs):
        self.lib_api.delete_task(get_current_task_id(self.kwargs))
        return self.pop_kwargs_redirect()


def task_redirect(request, username, task_id):
    lib_api = get_lib_api()
    task = lib_api.get_task_by_id(task_id)

    parent = task.parent
    if parent is None:
        box = lib_api.get_user_box(username, task)
        return HttpResponseRedirect(reverse('timd:task',
                                            args=[username, box.title_slug, box.id,
                                                  task.title_slug, task.id]))
    else:
        box = lib_api.get_user_box(username, parent)
        return HttpResponseRedirect(reverse('timd:subtask',
                                            args=[username, box.title_slug, box.id, parent.title_slug, parent.id,
                                                  task.title_slug, task.id]))


def redirect_to_current_task(kwargs):
    template_name = 'timd:task'
    if kwargs.get('subtask_id') is not None:
        template_name = 'timd:subtask'

    return HttpResponseRedirect(reverse(template_name, kwargs=kwargs))


class EditAttribute(RedirectMixin, base.View):
    form_class = None
    template_name = 'timd/form.html'

    def get_redirect_url_name(self):
        if self.kwargs.get('subtask_id') is not None:
            return 'timd:subtask'

        return 'timd:task'

    def on_get(self, form):
        pass

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        self.on_get(form)
        return render(request, self.template_name, {'form': form})

    def on_post(self, form, current_task):
        pass

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)


        if form.is_valid():
            #return HttpResponse(str(form.cleaned_data))

            task = self.lib_api.get_task_by_id(get_current_task_id(self.kwargs))
            self.on_post(form, task)

            return self.redirect()

        else:
            return self.redirect_back()



class DeleteAttribute(EditAttribute):
    attribute_name = ''
    form_class = forms.ConfirmDeleteAttributeForm
    template_name = 'timd/confirm_delete_attr.html'

    def on_get(self, form):
        form.attribute_name = self.attribute_name


class UpdateDeadline(EditAttribute):
    form_class = forms.DeadlineForm

    def on_post(self, form, task):
        deadline = form.cleaned_data['deadline']

        old_deadline = task.deadline
        new_deadline=deadline
        #func = choose_update_deadline_func(task.deadline, deadline)
        #if func is not None:
            #func(task, deadline)
        if old_deadline is None and new_deadline is not None:
            return self.lib_api.set_deadline(task, deadline)
        elif old_deadline is not None and new_deadline is not None:
            return self.lib_api.update_deadline(task, deadline)
        elif old_deadline is not None and new_deadline is None:
            return self.lib_api.unset_deadline(task)


def choose_update_deadline_func(old_deadline, new_deadline):
    lib_api = get_lib_api()
    if old_deadline is None and new_deadline is not None:
        return lib_api.set_deadline
    elif old_deadline is not None and new_deadline is not None:
        return lib_api.update_deadline
    elif old_deadline is not None and new_deadline is None:
        return lib_api.unset_deadline


class SetPeriodicity(EditAttribute):
    form_class = forms.PeriodicityForm

    def on_post(self, form, task):

        self.lib_api.set_periodicity(task, **form.cleaned_data)


class UnsetPeriodicity(DeleteAttribute):
    attribute_name = 'periodicity rule'

    def on_post(self, form, task):
        self.lib_api.unset_periodicity(task)


class BlockTask(EditAttribute):
    form_class = forms.CutRelationForm

    def on_get(self, form):
        username = self.request.user.username
        user_tasks = self.lib_api.get_user_tasks(username)
        form.fields['related_task'].queryset = user_tasks

    def on_post(self, form, task):
        self.lib_api.attach_relation(self.kwargs['username'], form.cleaned_data['related_task'], task,
                                     form.cleaned_data['type'])


class DetachRelation(DeleteAttribute):
    attribute_name = 'relation'

    def on_post(self, form, task):
        self.lib_api.detach_relation(self.kwargs.pop('relation_id'))


class AttachTag(EditAttribute):
    form_class = forms.TagForm

    def on_post(self, form, task):
        self.lib_api.attach_tag(self.kwargs['username'], task, **form.cleaned_data)


class SetReminder(EditAttribute):
    form_class = forms.ReminderForm

    def on_post(self, form, task):
        self.lib_api.set_reminder(task, form.cleaned_data['timedelta'])



class UnsetReminder(DeleteAttribute):
    attribute_name = 'reminder'

    def on_post(self, form, task):
        self.lib_api.unset_reminder(self.kwargs.pop('reminder_id'))



class DetachTag(DeleteAttribute):
    attribute_name = 'tag'

    def on_post(self, form, task):
        self.lib_api.detach_tag(self.kwargs['username'], task, self.kwargs.pop('tag_id'))


class GivePermission(EditAttribute):
    form_class = forms.PermissionForm

    def on_post(self, form, task):
        self.lib_api.give_permission(form.cleaned_data['user'], task, form.cleaned_data['access_level'])


class WithdrawPermission(DeleteAttribute):
    attribute_name = 'permission'

    def on_post(self, form, task):
        self.lib_api.withdraw_permission(self.kwargs.pop('user_id'), task)


def change_task_status(request, **kwargs):
    task = get_current_task_id(kwargs)
    lib_api = get_lib_api()
    action = kwargs.pop('action')
    if action == 'complete':
        lib_api.complete_task(task)
    elif action == 'resume':
        lib_api.resume_task(task)

    elif action == 'archive':
        lib_api.archive_task(task)

    elif action == 'restore':
        lib_api.restore_task(task)

    template_name = 'timd:task'
    if kwargs.get('subtask_id') is not None:
        template_name = 'timd:subtask'

    return HttpResponseRedirect(reverse(template_name, kwargs=kwargs))


def today(request, username):
    return HttpResponse("Today!")


def week(request, username):
    return HttpResponse("Week!")


def set_reminder(request, username, box_slug, task_slug, subtask_slug=None):
    lib_api = get_lib_api()
    if request.method == "POST":
        form = forms.ReminderForm(request.POST)
        if form.is_valid():

            return HttpResponse(str(form.cleaned_data))
        else:
            return HttpResponse('wrong data')
    else:
        form = forms.ReminderForm()
        return render(request, 'timd/tag.html', {'form': form})


def unset_reminder(request, username, box_slug, task_slug, subtask_slug=None):
    return HttpResponse('unset reminder')


