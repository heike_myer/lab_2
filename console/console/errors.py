
class ConsoleError(Exception):
    def __init__(self, message=None, *args, **kwargs):
        self.message = message
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.message