from django.conf.urls import url, include

from django.contrib.auth import views as auth_views

import timd.views as views


actions_urlpatterns = [
    url(r'^delete/$', views.DeleteTask.as_view(), name='delete-task'),
    url(r'^update-deadline/$',
        views.UpdateDeadline.as_view(extra_context={'title': 'Update Deadline'}), name='update-deadline'),
    url(r'^unset-periodicity/$', views.UnsetPeriodicity.as_view(), name='unset-periodicity'),
    url(r'^set-reminder/$', views.SetReminder.as_view(extra_context={'title': 'Set Reminder'}), name='set-reminder'),
    url(r'^give-permission/$', views.GivePermission.as_view(), name='give-permission'),
    url(r'^withdraw-permission/(?P<user_id>[a-z0-9_-]+)/$',
        views.WithdrawPermission.as_view(), name='withdraw-permission'),
    url(r'^unset-reminder/(?P<reminder_id>\d+)/$', views.UnsetReminder.as_view(extra_context={'title': 'Unset Reminder'}), name='unset-reminder'),
    url(r'^attach-tag/$', views.AttachTag.as_view(), name='attach-tag'),
    url(r'^block/$', views.BlockTask.as_view(), name='block'),
    url(r'^complete/$', views.change_task_status, {'action': 'complete'}, name='complete'),
    url(r'^resume/$', views.change_task_status, {'action': 'resume'}, name='resume'),
    url(r'^archive/$', views.change_task_status, {'action': 'archive'}, name='archive'),
    url(r'^restore/$', views.change_task_status, {'action': 'restore'}, name='restore'),
    url(r'^detach-tag/(?P<tag_id>\d+)/$', views.DetachTag.as_view(), name='detach-tag'),
    url(r'^detach-relation/(?P<relation_id>\d+)/$', views.DetachRelation.as_view(), name='detach-relation'),
    url(r'^set-periodicity/$', views.SetPeriodicity.as_view(), name='set-periodicity'),
]


task_urlpatterns = [
    url(r'^$', views.Task.as_view(), name='task'),
    url(r'^/', include(actions_urlpatterns)),

    url(r'^create-subtask$',
        views.CreateSubtask.as_view(extra_context={'title': 'Create Subtask'}), name='create-subtask'),
    url(r'^(?P<subtask_title>[a-z0-9-_]+)/(?P<subtask_id>\d+)/$',
        views.Task.as_view(template_name='timd/subtask.html'), name='subtask'),
    #url(r'^(?P<subtask_title>[a-z0-9-_]+)/(?P<subtask_id>\d+)/', include(actions_urlpatterns)),
]


box_urlpatterns = [
    url(r'^rename/$', views.RenameBox.as_view(extra_context={'title': 'Rename Box'}), name='rename-box'),
    url(r'^delete/$', views.DeleteBox.as_view(extra_context={'title': 'Delete Box'}), name='delete-box'),

    #url(r'^(?P<task_title>[a-z0-9-_]+)/(?P<task_id>\d+)/', include(task_urlpatterns)),
    url(r'^$', views.Box.as_view(), name='box'),
]





profile_urlpatterns = [
   # url(r'^boxes/', include(boxes_urlpatterns)),
    url(r'^task-redirect/(?P<task_id>\d+)/', views.task_redirect, name='task-redirect'),
    url(r'^boxes/create/$', views.CreateBox.as_view(extra_context={'title': 'Create Box'}), name='create-box'),
    url(r'^notifications/$', views.Notifications.as_view(), name='notifications'),
    url(r'^shared/$', views.Shared.as_view(), name='shared'),
    url(r'^today/$', views.Today.as_view(), name='today'),
    url(r'^week/$', views.Week.as_view(), name='week'),
    url(r'^completed/$', views.Completed.as_view(), name='completed'),

    url(r'^boxes/(?P<box_title>[a-z0-9-_]+)/(?P<box_id>\d+)/tasks/create/$', views.CreateTask.as_view(extra_context={'title': 'Create Task'}), name='create-task'),
    url(r'^boxes/(?P<box_title>[a-z0-9-_]+)/(?P<box_id>\d+)/', include(box_urlpatterns)),
    url(r'^tasks/(?P<task_title>[a-z0-9-_]+)/(?P<task_id>\d+)/', include(task_urlpatterns)),

    url(r'^', views.Profile.as_view(), name='profile'),
]




app_name = 'timd'
urlpatterns = [
    url(r'^profiles/(?P<username>[a-z0-9_-]+)/', include(profile_urlpatterns)),
    url(r'^profile-redirect/$', views.profile_redirect, name='profile-redirect'),
    url(r'^logout/$', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    url(r'^$', auth_views.LoginView.as_view(), name='login'),
]
