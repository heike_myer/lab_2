import functools

from datetime import datetime
from dateutil.relativedelta import relativedelta

import file_storage.models as models
import file_storage.models.str_constants as str_constants

import console.output_handlers as output
import console.environment_settings as settings


from console.errors import ConsoleError


def has_access(access_level=models.AccessLevel.CREATOR):
    def decorator(func):

        @functools.wraps(func)
        def wrapper(storage_api, username, namespace):

            if username is None and namespace.handler != signup_handler and namespace.handler != login_handler:
                raise ConsoleError('You need to log in first.')

            if username is not None and (namespace.handler == signup_handler or namespace.handler == login_handler):
                raise ConsoleError('You need to log out first.')

            task_id = namespace.__dict__.get('task_id')

            if task_id is None:
                return func(storage_api, username, namespace)

            permission = storage_api.get_permission(username, task_id)

            if permission.access_level.value <= access_level.value:
                return func(storage_api, username, namespace)
            else:
                raise Exception('Access denied.')

        return wrapper

    return decorator


def to_priority(priority_str):
    return models.Priority[priority_str]


def to_access_level(access_level_str):
    return models.AccessLevel[access_level_str]


@has_access()
def me_handler(storage_api, username, namespace):
    if namespace.me:
        user = storage_api.get_user_by_username(username)
        output.print_object(user)


@has_access()
def signup_handler(storage_api, user_id, namespace):
    storage_api.create_user(namespace.username)


@has_access()
def login_handler(storage_api, user_id, namespace):
    user = storage_api.get_user_by_username(namespace.username)

    config_parser = settings.get_session_parser()

    if config_parser.has_section('current_user'):
        raise Exception('Log out first')

    else:
        config_parser.add_section('current_user')
        config_parser['current_user']['user_id'] = user.instance_id
        config_parser['current_user']['username'] = user.username
        with open(settings.SESSION_PATH, 'w') as config_file:
            config_parser.write(config_file)


@has_access()
def logout_handler(storage_api, user_id, namespace):
    config_parser = settings.get_session_parser()
    config_parser.remove_section('current_user')
    with open(settings.SESSION_PATH, 'w') as conf:
        config_parser.write(conf)


@has_access()
def view_handler(storage_api, user_id, namespace):
    hierarchy = storage_api.get_hierarchy(user_id)
    output.print_object(hierarchy, namespace.depth)


@has_access()
def create_box_handler(storage_api, user_id, namespace):
    storage_api.create_box(user_id, namespace.title)


@has_access()
def create_task_handler(storage_api, user_id, namespace):
    kwargs = {
        str_constants.DEADLINE: namespace.deadline,
        str_constants.PRIORITY: namespace.priority_str,
        str_constants.BOX_ID: storage_api.get_user_box_by_title(user_id, namespace.box)
    }

    return storage_api.create_task(user_id, namespace.title, **kwargs)


@has_access()
def create_subtask_handler(storage_api, user_id, namespace):
    kwargs = {
        str_constants.DEADLINE: namespace.deadline,
        str_constants.PRIORITY: to_priority(namespace.priority_str)
    }

    return storage_api.create_subtask(user_id, namespace.task_id, namespace.title, **kwargs)


@has_access()
def move_handler(storage_api, user_id, namespace):
    if namespace.box is not None:
        box = storage_api.get_user_box_by_title(user_id, namespace.box)
        storage_api.move_to_box(namespace.task_id, box)
    else:
        storage_api.move_to_parent(namespace.task_id, namespace.parent)


@has_access()
def set_deadline_handler(storage_api, user_id, namespace):
    storage_api.set_deadline(namespace.task_id, namespace.term)


@has_access()
def set_reminder_handler(storage_api, user_id, namespace):
    storage_api.set_reminder(namespace.task_id, namespace.timedelta)


@has_access()
def set_periodicity_handler(storage_api, user_id, namespace):
    storage_api.set_periodicity(namespace.task_id, **namespace.rule)


@has_access()
def unset_deadline_handler(storage_api, user_id, namespace):
    storage_api.unset_deadline(namespace.task_id)


@has_access()
def unset_reminder_handler(storage_api, user_id, namespace):
    if namespace.all:
        storage_api.unset_all_reminders(namespace.task_id)
    else:
        print(namespace.timedelta)
        storage_api.unset_reminder(namespace.task_id, namespace.timedelta)


@has_access()
def unset_periodicity_handler(storage_api, user_id, namespace):
    storage_api.unset_periodicity(namespace.task_id)


@has_access()
def update_handler(storage_api, user_id, namespace):
    if namespace.deadline is not None:
        storage_api.update_task_attribute(namespace.task_id, deadline=namespace.deadline)
    elif namespace.priority_str is not None:
        storage_api.update_task_attribute(namespace.task_id, priority=to_priority(namespace.priority_str))
    else:
        storage_api.update_task_attribute(namespace.task_id, title=namespace.title)


@has_access(access_level=models.AccessLevel.WATCHER)
def tag_handler(storage_api, username, namespace):
    if namespace.add:
        storage_api.attach_tag(username, namespace.task_id, namespace.tag)
    else:
        storage_api.detach_tag(username, namespace.task_id, namespace.tag)


@has_access(access_level=models.AccessLevel.WATCHER)
def relate_handler(storage_api, user_id, namespace):
    if namespace.add:
        storage_api.attach_relation(user_id, namespace.task_from, namespace.task_to, namespace.relation_type)
    else:
        storage_api.detach_relation(user_id, namespace.task_from, namespace.task_to)


@has_access(access_level=models.AccessLevel.ADMIN)
def permit_handler(storage_api, username, namespace):
    if username == namespace.username:
        raise ConsoleError('User can neither give nor withdraw his own permission.')

    if namespace.add:
        access_level = namespace.__dict__.get('access_level')
        if access_level is None:
            raise ConsoleError('Access level is required to give permission.')
        storage_api.give_permission(namespace.username, namespace.task_id, namespace.access_level)
    else:
        storage_api.withdraw_permission(namespace.username, namespace.task_id)


@has_access()
def get_handler(storage_api, user_id, namespace):
    if namespace.notifications:
        notifications = storage_api.get_fresh_user_notifications(user_id)
        output.print_notifications_list(notifications)
        storage_api.view_notifications(notifications)

    elif namespace.overdue:
        tasks = storage_api.get_user_overdue_tasks(user_id)
        output.print_tasks_list(tasks)

    elif namespace.today:
        tasks = storage_api.get_tasks_until_date(user_id, datetime.now() + relativedelta(days=1))
        output.print_tasks_list(tasks)


@has_access()
def archive_handler(storage_api, user_id, namespace):
    if namespace.box:
        box = storage_api.get_user_box_by_title(user_id, namespace.box)
        storage_api.archive_box(box)
    else:
        storage_api.archive_task(namespace.task_id)


@has_access()
def restore_handler(storage_api, user_id, namespace):
    if namespace.box:
        box = storage_api.get_user_box_by_title(user_id, namespace.box)
        storage_api.restore_box(box)
    else:
        storage_api.restore_task(namespace.task_id)


@has_access()
def delete_handler(storage_api, user_id, namespace):
    if namespace.box:
        box = storage_api.get_user_box_by_title(user_id, namespace.box)
        storage_api.delete_box(box)
    else:
        storage_api.delete_task(namespace.task)


@has_access(access_level=models.AccessLevel.EXECUTOR)
def complete_handler(storage_api, user_id, namespace):
    storage_api.complete_task(namespace.task_id)


@has_access(access_level=models.AccessLevel.EXECUTOR)
def resume_handler(storage_api, user_id, namespace):
    storage_api.resume_task(namespace.task_id)


@has_access(access_level=models.AccessLevel.WATCHER)
def info_handler(storage_api, username, namespace):
    task_id = namespace.task_id
    if task_id:
        task = storage_api.get_task_by_id(task_id)

        reminders = storage_api.get_reminders(task_id)
        permissions = storage_api.get_task_permissions(task_id)
        tags = storage_api.get_user_tags(username, task_id)
        relations = storage_api.get_all_user_related_tasks(username, task_id)

        output.print_object(task, permissions, reminders, tags, relations)
