"""Custom extensions for python json package.

This module contains functions and classes necessary for correct json encoding and decoding of models' objects.

Attributes:
    encodable_classes (set): Set of supported models' classes.
"""
import datetime
import enum
import json

import dateutil.parser
import dateutil.relativedelta

import file_storage.models as models
import file_storage.models.str_constants as str_constants


encodable_classes = {
    models.Box,
    models.TaskBoxRelation,
    models.Task,
    models.Term,
    models.Relation,
    models.Permission,
    models.TaskTagRelation,
    models.Tag,
    models.User,
    models.Notification,
    models.TestObject
}


def parse_date(date_isoformat):
    """Parse isoformat date string and return a corresponding `datetime.datetime object`."""
    date = None
    if date_isoformat is not None:
        date = dateutil.parser.parse(date_isoformat)
    return date


def parse_relative_delta(obj_dict):
    """Transform a dict representing relative time delta to `dateutil.relativedelta.relativedelta`."""
    obj = None
    if obj_dict is not None:
        obj = dateutil.relativedelta.relativedelta(**obj_dict)
    return obj


def box_hook(obj_dict):
    """If dict represents a `Box` object, transform it to `Box` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Box.__name__:
        obj = models.Box(obj_dict[str_constants.INSTANCE_ID],
                         obj_dict[str_constants.USER_ID],
                         obj_dict[str_constants.TITLE],
                         archived=obj_dict[str_constants.ARCHIVED])

        return obj

    return obj_dict


def task_box_relation_hook(obj_dict):
    """If dict represents a `TaskBoxRelation` object, transform it to `TaskBoxRelation` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.TaskBoxRelation.__name__:
        obj = models.TaskBoxRelation(obj_dict[str_constants.INSTANCE_ID],
                                     obj_dict[str_constants.USER_ID],
                                     obj_dict[str_constants.TASK_ID],
                                     obj_dict[str_constants.BOX_ID])

        return obj

    return obj_dict


def task_hook(obj_dict):
    """If dict represents a `Task` object, transform it to `Task` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Task.__name__:
        obj = models.Task(obj_dict[str_constants.INSTANCE_ID],
                          parse_date(obj_dict[str_constants.CREATION_TIME]),
                          parse_date(obj_dict[str_constants.LAST_UPDATE_TIME]),
                          obj_dict[str_constants.USER_ID],
                          obj_dict[str_constants.TITLE],
                          status=models.Status[obj_dict[str_constants.STATUS]],
                          deadline=parse_date(obj_dict[str_constants.DEADLINE]),
                          priority=models.Priority[obj_dict[str_constants.PRIORITY]],
                          parent_id=obj_dict[str_constants.PARENT_ID],
                          periodical=obj_dict[str_constants.PERIODICAL],
                          rule_str=obj_dict[str_constants.RULE_STR],
                          archived=obj_dict[str_constants.ARCHIVED])

        return obj

    return obj_dict


def user_hook(obj_dict):
    """If dict represents a `User` object, transform it to `User` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.User.__name__:
        obj = models.User(obj_dict[str_constants.INSTANCE_ID],
                          obj_dict[str_constants.USERNAME],
                          obj_dict[str_constants.CREATOR_BOX],
                          obj_dict[str_constants.SHARED_BOX])

        return obj

    return obj_dict


def permission_hook(obj_dict):
    """If dict represents a `Permission` object, transform it to `Permission` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Permission.__name__:
        obj = models.Permission(obj_dict[str_constants.INSTANCE_ID],
                                obj_dict[str_constants.USER_ID],
                                obj_dict[str_constants.TASK_ID],
                                access_level=models.AccessLevel[obj_dict[str_constants.ACCESS_LEVEL]])

        return obj

    return obj_dict


def relation_hook(obj_dict):
    """If dict represents a `Relation` object, transform it to `Relation` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Relation.__name__:
        obj = models.Relation(obj_dict[str_constants.INSTANCE_ID],
                              obj_dict[str_constants.TASK_FROM],
                              obj_dict[str_constants.TASK_TO],
                              models.RelationType[obj_dict[str_constants.RELATION_TYPE]],
                              obj_dict[str_constants.USER_ID],
                              archived=obj_dict[str_constants.ARCHIVED])

        return obj

    return obj_dict


def term_hook(obj_dict):
    """If dict represents a `Term` object, transform it to `Term` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Term.__name__:
        obj = models.Term(obj_dict[str_constants.INSTANCE_ID],
                          obj_dict[str_constants.TASK_ID],
                          parse_date(obj_dict[str_constants.TERM], ),
                          models.TermType[obj_dict[str_constants.TERM_TYPE]],
                          obj_dict[str_constants.ACTIVE],
                          parse_relative_delta(obj_dict[str_constants.RELATIVE_DELTA]),
                          archived=obj_dict[str_constants.ARCHIVED])

        return obj

    return obj_dict


def tag_hook(obj_dict):
    """If dict represents a `Tag` object, transform it to `Tag` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Tag.__name__:
        obj = models.Tag(obj_dict[str_constants.INSTANCE_ID],
                         obj_dict[str_constants.TAG])

        return obj

    return obj_dict


def task_tag_relation_hook(obj_dict):
    """If dict represents a `TaskTagRelation object`, transform it to `TaskTagRelation` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.TaskTagRelation.__name__:
        obj = models.TaskTagRelation(obj_dict[str_constants.INSTANCE_ID],
                                     obj_dict[str_constants.TASK_ID],
                                     obj_dict[str_constants.TAG],
                                     obj_dict[str_constants.USER_ID])

        return obj

    return obj_dict


def notification_hook(obj_dict):
    """If dict represents a `Notification` object, transform it to `Notification` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.Notification.__name__:
        obj = models.Notification(obj_dict[str_constants.INSTANCE_ID],
                                  obj_dict[str_constants.USER_ID],
                                  obj_dict[str_constants.TASK_ID],
                                  parse_date(obj_dict[str_constants.CREATION_TIME]),
                                  models.NotificationType[obj_dict[str_constants.NOTIFICATION_TYPE]],
                                  obj_dict[str_constants.VIEWED])

        return obj

    return obj_dict


def test_object_hook(obj_dict):
    """If dict represents a `TestObject` object, transform it to `TestObject` and return."""
    obj_class = obj_dict.get(str_constants.CLASS)
    if obj_class is not None and obj_class == models.TestObject.__name__:
        return models.TestObject(obj_dict[str_constants.INSTANCE_ID], obj_dict[str_constants.NAME])

    return obj_dict


class CustomJSONDecoder(json.JSONDecoder):
    """Class performs decoding of supported models objects encoded by `CustomJSONEncoder`.

     To use it, specify `CustomJSONDecoder` as `cls` when calling `json.load` and add `object_type` keyword argument.
     Before passing arguments to `json.JSONDecoder` it will choose which object hook to use based on `object_type`.
    """
    _object_hooks = {
        models.Box: box_hook,
        models.Task: task_hook,
        models.User: user_hook,
        models.Term: term_hook,
        models.Permission: permission_hook,
        models.Tag: tag_hook,
        models.TaskTagRelation: task_tag_relation_hook,
        models.Relation: relation_hook,
        models.TaskBoxRelation: task_box_relation_hook,
        models.Notification: notification_hook,
        models.TestObject: test_object_hook
    }

    def __init__(self, parse_float=None, parse_int=None,
                 parse_constant=None, strict=True, object_pairs_hook=None, **kwargs):
        object_type = kwargs[str_constants.OBJECT_TYPE]

        object_hook = self.__class__._object_hooks.get(object_type)

        super().__init__(object_hook=object_hook, parse_int=parse_int,
                         parse_float=parse_float, parse_constant=parse_constant,
                         strict=strict, object_pairs_hook=object_pairs_hook)


def encode_relative_delta(relative_delta):
    """Transform `dateutil.relativedelta.relativedelta` object to a `dict`."""
    return {
        str_constants.YEARS: relative_delta.years,
        str_constants.MONTHS: relative_delta.months,
        str_constants.DAYS: relative_delta.days,
        str_constants.HOURS: relative_delta.hours,
        str_constants.MINUTES: relative_delta.minutes,
    }


class CustomJSONEncoder(json.JSONEncoder):
    """Class performs encoding of supported models' objects.

    Specify it as `cls` when calling `json.dump` to convert models' objects to their valid json representation.
    """
    def default(self, obj):
        if type(obj) in encodable_classes:
            obj_dict = obj.__dict__
            obj_dict[str_constants.CLASS] = obj.__class__.__name__
            return obj_dict

        if isinstance(obj, datetime.datetime):
            return obj.isoformat()

        if isinstance(obj, dateutil.relativedelta.relativedelta):
            return encode_relative_delta(obj)

        if isinstance(obj, enum.Enum):
            return obj.name

        return super().default(obj)
