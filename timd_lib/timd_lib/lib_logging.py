import functools
import logging
import logging.config


LOGGER_NAME = 'timd_lib'


def get_lib_logger():
    return logging.getLogger(LOGGER_NAME)


def lib_api_log(level=logging.INFO):
    def log_decorator(func):

        @functools.wraps(func)
        def logging_wrapper(*args, **kwargs):
            logger = get_lib_logger()
            extra = {'func': func.__name__, 'action': 'ENTER'}

            logger.log(level, 'Args: {}\tKwargs: {}'.format(args, kwargs), extra=extra)

            try:
                result = func(*args, **kwargs)
                extra['action'] = 'EXIT'
                logger.log(level, 'Result: {}'.format(result), extra=extra)
                return result

            except Exception as e:
                extra['action'] = 'EXCEPTION RAISED'
                logger.log(logging.WARNING, 'Exception: {}'.format(type(e)), extra=extra)
                raise

        return logging_wrapper

    return log_decorator
